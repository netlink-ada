--
--  Copyright (c) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This file is part of Netlink/Ada.
--
--  Netlink/Ada is free software; you can redistribute it and/or modify
--  it under the terms of the GNU Lesser General Public License as published
--  by the Free Software Foundation; either version 2.1 of the License, or
--  (at your option) any later version.
--
--  Netlink/Ada is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public License
--  along with Netlink/Ada.  If not, see <https://www.gnu.org/licenses/>.
--

with Ada.Text_IO;
with Ada.Command_Line;

with Interfaces.C.Strings;

with netlink_object_h;
with netlink_route_link_h;

with NL.Cache_Manager;

with Handlers;

procedure SLAAC
is
   Sock : NL.Socket_Type;
   Mngr : NL.Cache_Manager.Cache_Mngr_Type;
begin
   if Ada.Command_Line.Argument_Count /= 1 then
      Ada.Text_IO.Put_Line ("Usage: " & Ada.Command_Line.Command_Name
                            & " <iface>");
      return;
   end if;

   NL.Cache_Manager.Initialize
     (Cache_Mngr => Mngr,
      Socket     => Sock);

   declare
      use type NL.Link_Operstatus_Type;

      Cache : constant access netlink_object_h.nl_cache
        := Mngr.Get_Link_Cache;
      C_Name : Interfaces.C.Strings.chars_ptr
        := Interfaces.C.Strings.New_String
          (Str => Ada.Command_Line.Argument (1));
      Link : constant access netlink_route_link_h.rtnl_link
        := netlink_route_link_h.rtnl_link_get_by_name
          (arg1 => Cache,
           arg2 => C_Name);
      Operstate : constant NL.Link_Operstatus_Type
        := NL.Link_Operstatus_Type'Val
          (netlink_route_link_h.rtnl_link_get_operstate (arg1 => Link));
   begin
      Interfaces.C.Strings.Free (Item => C_Name);

      Handlers.Instance.Iface_Idx := netlink_route_link_h.rtnl_link_get_ifindex
        (arg1 => Link);
      netlink_route_link_h.rtnl_link_put (arg1 => Link);

      if Operstate /= NL.IF_Oper_Down then
         Ada.Text_IO.Put_Line ("Start with interface in DOWN state");
         return;
      end if;
   end;

   Ada.Text_IO.Put_Line ("Observing SLAAC of iface "
                         & Ada.Command_Line.Argument (1) & ", idx"
                         & Handlers.Instance.Iface_Idx'Img);

   Mngr.Register_Link_Handler  (Handler => Handlers.Instance'Access);
   Mngr.Register_Addr_Handler  (Handler => Handlers.Instance'Access);
   Mngr.Register_Route_Handler (Handler => Handlers.Instance'Access);

   loop
      Mngr.Peek;
   end loop;
end SLAAC;
