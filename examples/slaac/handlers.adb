--
--  Copyright (c) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This file is part of Netlink/Ada.
--
--  Netlink/Ada is free software; you can redistribute it and/or modify
--  it under the terms of the GNU Lesser General Public License as published
--  by the Free Software Foundation; either version 2.1 of the License, or
--  (at your option) any later version.
--
--  Netlink/Ada is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public License
--  along with Netlink/Ada.  If not, see <https://www.gnu.org/licenses/>.
--

pragma Warnings (Off);
with System.OS_Constants;
pragma Warnings (On);

with Ada.Text_IO;
with Ada.Command_Line;

with Interfaces.C.Strings;

with netlink_route_link_inet6_h;
with netlink_route_nexthop_h;
with netlink_addr_h;
with linux_rtnetlink_h;
with x86_64_linux_gnu_bits_stdint_uintn_h;

with NL.Utils;

package body Handlers
is

   use type Interfaces.C.int;
   use type NL.Cache_Actions_Type;

   type RA_State_Type is record
      Received  : Boolean := False;
      Managed   : Boolean := False;
      Otherconf : Boolean := False;
   end record;

   RA_State : RA_State_Type;

   -------------------------------------------------------------------------

   procedure Address_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_addr_h.rtnl_addr)
   is
      Scope : constant NL.Scope_Type
        := NL.Scope_Type'Enum_Val
          (netlink_route_addr_h.rtnl_addr_get_scope
             (arg1 => Msg));
   begin
      if Action = NL.NL_Act_New
        and then H.Iface_Idx = netlink_route_addr_h.rtnl_addr_get_ifindex
          (arg1 => Msg)
        and then netlink_route_addr_h.rtnl_addr_get_family
          (arg1 => Msg) = System.OS_Constants.AF_INET6
        and then Scope in NL.RT_Scope_Link | NL.RT_Scope_Site
          | NL.RT_Scope_Universe
      then
         Ada.Text_IO.Put_Line ("** New non-local IPv6 address appeared");
         Ada.Text_IO.Put_Line
           (" Address: " & NL.Utils.Nladdr_To_String
              (Addr => netlink_route_addr_h.rtnl_addr_get_local
                   (arg1 => Msg)));
         Ada.Text_IO.Put_Line
           (" Netmask:" & netlink_route_addr_h.rtnl_addr_get_prefixlen
              (arg1 => Msg)'Img);
         Ada.Text_IO.Put_Line (" Scope  : " & Scope'Img);
      end if;
   end Address_Update;

   -------------------------------------------------------------------------

   procedure Link_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_link_h.rtnl_link)
   is
      pragma Unreferenced (H);

      Iface_Name : constant String
        := Interfaces.C.Strings.Value
          (netlink_route_link_h.rtnl_link_get_name
             (arg1 => Msg));
   begin
      if Action = NL.NL_Act_Change
        and then Iface_Name = Ada.Command_Line.Argument (1)
      then
         declare
            use type Interfaces.C.unsigned;

            Inet6_Flags : aliased
              x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;
            Inet6_Flags_Status : constant Interfaces.C.int
              := netlink_route_link_inet6_h.rtnl_link_inet6_get_flags
                (arg1 => Msg,
                 arg2 => Inet6_Flags'Access);
            New_RA_State : RA_State_Type;
         begin
            if Inet6_Flags_Status = 0 then
               New_RA_State :=
                 (Received  => True,
                  Managed   => (Inet6_Flags and NL.IF_RA_MANAGED) /= 0,
                  Otherconf => (Inet6_Flags and NL.IF_RA_OTHERCONF) /= 0);
               if New_RA_State /= RA_State then
                  RA_State := New_RA_State;

                  Ada.Text_IO.Put_Line
                    ("** RA: Managed " & RA_State.Managed'Img
                     & " otherconf " & RA_State.Otherconf'Img);
               end if;
            end if;
         end;
      end if;
   end Link_Update;

   -------------------------------------------------------------------------

   procedure Route_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_route_h.rtnl_route)
   is
      use type Interfaces.C.unsigned;
      use type Interfaces.C.unsigned_char;

      Hop_Count : constant Interfaces.C.int
        := netlink_route_route_h.rtnl_route_get_nnexthops
          (arg1 => Msg);
   begin
      if Action = NL.NL_Act_New
        and then netlink_route_route_h.rtnl_route_get_table
          (arg1 => Msg) = linux_rtnetlink_h.RT_TABLE_MAIN
        and then netlink_route_route_h.rtnl_route_get_family
          (arg1 => Msg) = System.OS_Constants.AF_INET6
        and then Hop_Count > 0
      then
         for I in 0 .. Hop_Count - 1 loop
            declare
               Next_Hop : constant access
                 netlink_route_nexthop_h.rtnl_nexthop
                   := netlink_route_route_h.rtnl_route_nexthop_n
                     (arg1 => Msg,
                      arg2 => I);
               Gw : constant access netlink_addr_h.nl_addr
                 := netlink_route_nexthop_h.rtnl_route_nh_get_gateway
                   (arg1 => Next_Hop);
               If_Idx : constant Interfaces.C.int
                 := netlink_route_nexthop_h.rtnl_route_nh_get_ifindex
                   (arg1 => Next_Hop);
            begin
               if If_Idx = H.Iface_Idx
                 and then Gw /= null
               then
                  Ada.Text_IO.Put_Line
                    ("** New route with gateway "
                     & NL.Utils.Nladdr_To_String (Addr => Gw) & " (" & I'Img
                     & ")");
               end if;
            end;
         end loop;
      end if;
   end Route_Update;

end Handlers;
