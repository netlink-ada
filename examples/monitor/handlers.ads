--
--  Copyright (c) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This file is part of Netlink/Ada.
--
--  Netlink/Ada is free software; you can redistribute it and/or modify
--  it under the terms of the GNU Lesser General Public License as published
--  by the Free Software Foundation; either version 2.1 of the License, or
--  (at your option) any later version.
--
--  Netlink/Ada is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public License
--  along with Netlink/Ada.  If not, see <https://www.gnu.org/licenses/>.
--

with netlink_route_link_h;
with netlink_route_addr_h;
with netlink_route_route_h;

with NL.Cache_Manager;

package Handlers
is

   type My_Handler_Type is new NL.Cache_Manager.Link_Handler_Type
     and NL.Cache_Manager.Addr_Handler_Type
     and NL.Cache_Manager.Route_Handler_Type with null record;

   Conversion_Error : exception;

   overriding
   procedure Link_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_link_h.rtnl_link);

   overriding
   procedure Address_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_addr_h.rtnl_addr);

   overriding
   procedure Route_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_route_h.rtnl_route);

   Instance : aliased My_Handler_Type;

end Handlers;
