--
--  Copyright (c) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This file is part of Netlink/Ada.
--
--  Netlink/Ada is free software; you can redistribute it and/or modify
--  it under the terms of the GNU Lesser General Public License as published
--  by the Free Software Foundation; either version 2.1 of the License, or
--  (at your option) any later version.
--
--  Netlink/Ada is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public License
--  along with Netlink/Ada.  If not, see <https://www.gnu.org/licenses/>.
--

with Ada.Text_IO;

with Interfaces.C.Strings;

with netlink_route_link_inet6_h;
with netlink_route_nexthop_h;
with netlink_addr_h;
with linux_rtnetlink_h;
with x86_64_linux_gnu_bits_stdint_uintn_h;

with NL.Utils;

package body Handlers
is

   -------------------------------------------------------------------------

   procedure Address_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_addr_h.rtnl_addr)
   is
      pragma Unreferenced (H);
   begin
      Ada.Text_IO.Put_Line ("addr: update: " & Action'Img);
      Ada.Text_IO.Put_Line
        ("addr: iface idx:" & netlink_route_addr_h.rtnl_addr_get_ifindex
           (arg1 => Msg)'Img);
      Ada.Text_IO.Put_Line
        ("addr: family:" & netlink_route_addr_h.rtnl_addr_get_family
           (arg1 => Msg)'Img);

      Ada.Text_IO.Put_Line ("addr: addr: " & NL.Utils.Nladdr_To_String
                            (Addr => netlink_route_addr_h.rtnl_addr_get_local
                             (arg1 => Msg)));
   end Address_Update;

   -------------------------------------------------------------------------

   procedure Link_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_link_h.rtnl_link)
   is
      pragma Unreferenced (H);
   begin
      Ada.Text_IO.Put_Line
        ("link: update: " & Action'Img);
      Ada.Text_IO.Put_Line
        ("link: name: " & Interfaces.C.Strings.Value
           (netlink_route_link_h.rtnl_link_get_name
                (arg1 => Msg)));
      Ada.Text_IO.Put_Line
        ("link: iface idx:" & netlink_route_link_h.rtnl_link_get_ifindex
           (arg1 => Msg)'Img);
      Ada.Text_IO.Put_Line
        ("link: mtu:" & netlink_route_link_h.rtnl_link_get_mtu
           (arg1 => Msg)'Img);
      declare
         use type Interfaces.C.int;
         use type Interfaces.C.unsigned;

         Inet6_Flags : aliased x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;
         Inet6_Flags_Status : constant Interfaces.C.int
           := netlink_route_link_inet6_h.rtnl_link_inet6_get_flags
             (arg1 => Msg,
              arg2 => Inet6_Flags'Access);
         C_Buf : aliased Interfaces.C.char_array
           := (1 .. 64 => Interfaces.C.nul);
      begin
         if Inet6_Flags_Status = 0 then
            if (Inet6_Flags and NL.IF_RA_OTHERCONF) /= 0 then
               Ada.Text_IO.Put_Line ("link: inet6: IF_RA_OTHERCONF");
            end if;
            if (Inet6_Flags and NL.IF_RA_MANAGED) /= 0 then
               Ada.Text_IO.Put_Line ("link: inet6: IF_RA_MANAGED");
            end if;
            Ada.Text_IO.Put_Line
              ("link: inet6_flags_str: " & Interfaces.C.Strings.Value
                 (netlink_route_link_inet6_h.rtnl_link_inet6_flags2str
                      (arg1 => Interfaces.C.int
                           (Inet6_Flags and 16#7fff_ffff#),
                       arg2 => Interfaces.C.Strings.To_Chars_Ptr
                         (C_Buf'Unchecked_Access),
                       arg3 => C_Buf'Length)));
         end if;
      end;
   end Link_Update;

   -------------------------------------------------------------------------

   procedure Route_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_route_h.rtnl_route)
   is
      pragma Unreferenced (H);

      use type Interfaces.C.unsigned;
   begin
      Ada.Text_IO.Put_Line ("route: update: " & Action'Img);

      Ada.Text_IO.Put_Line
        ("route: table:" & netlink_route_route_h.rtnl_route_get_table
           (arg1 => Msg)'Img);
      if netlink_route_route_h.rtnl_route_get_table
        (arg1 => Msg) = linux_rtnetlink_h.RT_TABLE_MAIN
      then
         Ada.Text_IO.Put_Line ("route: main table");
      end if;
      Ada.Text_IO.Put_Line
        ("route: family:" & netlink_route_route_h.rtnl_route_get_family
           (arg1 => Msg)'Img);
      Ada.Text_IO.Put_Line
        ("route: proto:" & netlink_route_route_h.rtnl_route_get_protocol
           (arg1 => Msg)'Img);
      Ada.Text_IO.Put_Line
        ("route: iif:" & netlink_route_route_h.rtnl_route_get_iif
           (arg1 => Msg)'Img);
      Ada.Text_IO.Put_Line
        ("route: src: " & NL.Utils.Nladdr_To_String
           (Addr => netlink_route_route_h.rtnl_route_get_src
                (arg1 => Msg)));

      declare
         use type Interfaces.C.int;

         Hop_Count : constant Interfaces.C.int
           := netlink_route_route_h.rtnl_route_get_nnexthops
             (arg1 => Msg);
      begin
         Ada.Text_IO.Put_Line ("route: next hops:" & Hop_Count'Img);

         for I in 0 .. Hop_Count - 1 loop
            declare
               Next_Hop : constant access netlink_route_nexthop_h.rtnl_nexthop
                 := netlink_route_route_h.rtnl_route_nexthop_n
                   (arg1 => Msg,
                    arg2 => I);
               Gw : constant access netlink_addr_h.nl_addr
                 := netlink_route_nexthop_h.rtnl_route_nh_get_gateway
                   (arg1 => Next_Hop);
               Ifidx : constant Interfaces.C.int
                 := netlink_route_nexthop_h.rtnl_route_nh_get_ifindex
                   (arg1 => Next_Hop);
            begin
               Ada.Text_IO.Put_Line
                 ("route: nexthop" & I'Img & ": ifindex:" & Ifidx'Img);
               Ada.Text_IO.Put_Line
                 ("route: nexthop" & I'Img & ": gw: "
                  & NL.Utils.Nladdr_To_String (Addr => Gw));
            end;
         end loop;
      end;
   end Route_Update;

end Handlers;
