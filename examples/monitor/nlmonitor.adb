--
--  Copyright (c) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This file is part of Netlink/Ada.
--
--  Netlink/Ada is free software; you can redistribute it and/or modify
--  it under the terms of the GNU Lesser General Public License as published
--  by the Free Software Foundation; either version 2.1 of the License, or
--  (at your option) any later version.
--
--  Netlink/Ada is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public License
--  along with Netlink/Ada.  If not, see <https://www.gnu.org/licenses/>.
--

with NL.Cache_Manager;

with Handlers;

procedure NLMonitor
is
   Sock : NL.Socket_Type;
   Mngr : NL.Cache_Manager.Cache_Mngr_Type;
begin
   NL.Cache_Manager.Initialize
     (Cache_Mngr => Mngr,
      Socket     => Sock);

   Mngr.Register_Link_Handler  (Handler => Handlers.Instance'Access);
   Mngr.Register_Addr_Handler  (Handler => Handlers.Instance'Access);
   Mngr.Register_Route_Handler (Handler => Handlers.Instance'Access);

   loop
      Mngr.Peek;
   end loop;
end NLMonitor;
