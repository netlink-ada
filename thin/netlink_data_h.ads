pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with System;
with nlada_stddef_h;

package netlink_data_h is

   package stddef_h renames nlada_stddef_h;

   type nlattr is null record;   -- incomplete struct

   type nl_data is null record;   -- incomplete struct

   function nl_data_alloc (arg1 : System.Address; arg2 : stddef_h.size_t) return access nl_data;  -- ../include/netlink/data.h:20
   pragma Import (C, nl_data_alloc, "nl_data_alloc");

   function nl_data_alloc_attr (arg1 : access constant nlattr) return access nl_data;  -- ../include/netlink/data.h:21
   pragma Import (C, nl_data_alloc_attr, "nl_data_alloc_attr");

   function nl_data_clone (arg1 : access constant nl_data) return access nl_data;  -- ../include/netlink/data.h:22
   pragma Import (C, nl_data_clone, "nl_data_clone");

   function nl_data_append
     (arg1 : access nl_data;
      arg2 : System.Address;
      arg3 : stddef_h.size_t) return int;  -- ../include/netlink/data.h:23
   pragma Import (C, nl_data_append, "nl_data_append");

   procedure nl_data_free (arg1 : access nl_data);  -- ../include/netlink/data.h:24
   pragma Import (C, nl_data_free, "nl_data_free");

   function nl_data_get (arg1 : access constant nl_data) return System.Address;  -- ../include/netlink/data.h:27
   pragma Import (C, nl_data_get, "nl_data_get");

   function nl_data_get_size (arg1 : access constant nl_data) return stddef_h.size_t;  -- ../include/netlink/data.h:28
   pragma Import (C, nl_data_get_size, "nl_data_get_size");

   function nl_data_cmp (arg1 : access constant nl_data; arg2 : access constant nl_data) return int;  -- ../include/netlink/data.h:31
   pragma Import (C, nl_data_cmp, "nl_data_cmp");

end netlink_data_h;
