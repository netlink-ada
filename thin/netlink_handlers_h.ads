pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with System;
limited with linux_netlink_h;

package netlink_handlers_h is

   --  unsupported macro: NL_CB_KIND_MAX (__NL_CB_KIND_MAX - 1)
   --  unsupported macro: NL_CB_TYPE_MAX (__NL_CB_TYPE_MAX - 1)
   type ucred is null record;   -- incomplete struct

   type nl_cb is null record;   -- incomplete struct

   type nl_sock is null record;   -- incomplete struct

   type nl_msg is null record;   -- incomplete struct

   type nl_recvmsg_msg_cb_t is access function (arg1 : access nl_msg; arg2 : System.Address) return int;
   pragma Convention (C, nl_recvmsg_msg_cb_t);  -- ../include/netlink/handlers.h:38

   type nl_recvmsg_err_cb_t is access function
        (arg1 : access linux_netlink_h.sockaddr_nl;
         arg2 : access linux_netlink_h.nlmsgerr;
         arg3 : System.Address) return int;
   pragma Convention (C, nl_recvmsg_err_cb_t);  -- ../include/netlink/handlers.h:47

   type nl_cb_action is 
     (NL_OK,
      NL_SKIP,
      NL_STOP);
   pragma Convention (C, nl_cb_action);  -- ../include/netlink/handlers.h:56

   type nl_cb_kind is 
     (NL_CB_DEFAULT,
      NL_CB_VERBOSE,
      NL_CB_DEBUG,
      NL_CB_CUSTOM,
      uu_NL_CB_KIND_MAX);
   pragma Convention (C, nl_cb_kind);  -- ../include/netlink/handlers.h:69

   type nl_cb_type is 
     (NL_CB_VALID,
      NL_CB_FINISH,
      NL_CB_OVERRUN,
      NL_CB_SKIPPED,
      NL_CB_ACK,
      NL_CB_MSG_IN,
      NL_CB_MSG_OUT,
      NL_CB_INVALID,
      NL_CB_SEQ_CHECK,
      NL_CB_SEND_ACK,
      NL_CB_DUMP_INTR,
      uu_NL_CB_TYPE_MAX);
   pragma Convention (C, nl_cb_type);  -- ../include/netlink/handlers.h:87

   function nl_cb_alloc (arg1 : nl_cb_kind) return access nl_cb;  -- ../include/netlink/handlers.h:115
   pragma Import (C, nl_cb_alloc, "nl_cb_alloc");

   function nl_cb_clone (arg1 : access nl_cb) return access nl_cb;  -- ../include/netlink/handlers.h:116
   pragma Import (C, nl_cb_clone, "nl_cb_clone");

   function nl_cb_get (arg1 : access nl_cb) return access nl_cb;  -- ../include/netlink/handlers.h:117
   pragma Import (C, nl_cb_get, "nl_cb_get");

   procedure nl_cb_put (arg1 : access nl_cb);  -- ../include/netlink/handlers.h:118
   pragma Import (C, nl_cb_put, "nl_cb_put");

   function nl_cb_set
     (arg1 : access nl_cb;
      arg2 : nl_cb_type;
      arg3 : nl_cb_kind;
      arg4 : nl_recvmsg_msg_cb_t;
      arg5 : System.Address) return int;  -- ../include/netlink/handlers.h:120
   pragma Import (C, nl_cb_set, "nl_cb_set");

   function nl_cb_set_all
     (arg1 : access nl_cb;
      arg2 : nl_cb_kind;
      arg3 : nl_recvmsg_msg_cb_t;
      arg4 : System.Address) return int;  -- ../include/netlink/handlers.h:122
   pragma Import (C, nl_cb_set_all, "nl_cb_set_all");

   function nl_cb_err
     (arg1 : access nl_cb;
      arg2 : nl_cb_kind;
      arg3 : nl_recvmsg_err_cb_t;
      arg4 : System.Address) return int;  -- ../include/netlink/handlers.h:124
   pragma Import (C, nl_cb_err, "nl_cb_err");

   procedure nl_cb_overwrite_recvmsgs (arg1 : access nl_cb; func : access function (arg1 : access nl_sock; arg2 : access nl_cb) return int);  -- ../include/netlink/handlers.h:127
   pragma Import (C, nl_cb_overwrite_recvmsgs, "nl_cb_overwrite_recvmsgs");

   procedure nl_cb_overwrite_recv (arg1 : access nl_cb; func : access function
        (arg1 : access nl_sock;
         arg2 : access linux_netlink_h.sockaddr_nl;
         arg3 : System.Address;
         arg4 : System.Address) return int);  -- ../include/netlink/handlers.h:130
   pragma Import (C, nl_cb_overwrite_recv, "nl_cb_overwrite_recv");

   procedure nl_cb_overwrite_send (arg1 : access nl_cb; func : access function (arg1 : access nl_sock; arg2 : access nl_msg) return int);  -- ../include/netlink/handlers.h:135
   pragma Import (C, nl_cb_overwrite_send, "nl_cb_overwrite_send");

   function nl_cb_active_type (cb : access nl_cb) return nl_cb_type;  -- ../include/netlink/handlers.h:139
   pragma Import (C, nl_cb_active_type, "nl_cb_active_type");

end netlink_handlers_h;
