pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with System;
limited with netlink_types_h;
with nlada_stddef_h;
with x86_64_linux_gnu_bits_stdint_uintn_h;

package netlink_object_h is

   package stddef_h renames nlada_stddef_h;

   --  arg-macro: function OBJ_CAST (ptr)
   --    return (struct nl_object *) (ptr);
   type nl_cache is null record;   -- incomplete struct

   type nl_object is null record;   -- incomplete struct

   type nl_object_ops is null record;   -- incomplete struct

   function nl_object_alloc (arg1 : access nl_object_ops) return access nl_object;  -- ../include/netlink/object.h:23
   pragma Import (C, nl_object_alloc, "nl_object_alloc");

   function nl_object_alloc_name (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : System.Address) return int;  -- ../include/netlink/object.h:24
   pragma Import (C, nl_object_alloc_name, "nl_object_alloc_name");

   procedure nl_object_free (arg1 : access nl_object);  -- ../include/netlink/object.h:26
   pragma Import (C, nl_object_free, "nl_object_free");

   function nl_object_clone (arg1 : access nl_object) return access nl_object;  -- ../include/netlink/object.h:27
   pragma Import (C, nl_object_clone, "nl_object_clone");

   function nl_object_update (dst : access nl_object; src : access nl_object) return int;  -- ../include/netlink/object.h:28
   pragma Import (C, nl_object_update, "nl_object_update");

   procedure nl_object_get (arg1 : access nl_object);  -- ../include/netlink/object.h:30
   pragma Import (C, nl_object_get, "nl_object_get");

   procedure nl_object_put (arg1 : access nl_object);  -- ../include/netlink/object.h:31
   pragma Import (C, nl_object_put, "nl_object_put");

   function nl_object_shared (arg1 : access nl_object) return int;  -- ../include/netlink/object.h:32
   pragma Import (C, nl_object_shared, "nl_object_shared");

   procedure nl_object_dump (arg1 : access nl_object; arg2 : access netlink_types_h.nl_dump_params);  -- ../include/netlink/object.h:33
   pragma Import (C, nl_object_dump, "nl_object_dump");

   procedure nl_object_dump_buf
     (arg1 : access nl_object;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t);  -- ../include/netlink/object.h:35
   pragma Import (C, nl_object_dump_buf, "nl_object_dump_buf");

   function nl_object_identical (arg1 : access nl_object; arg2 : access nl_object) return int;  -- ../include/netlink/object.h:36
   pragma Import (C, nl_object_identical, "nl_object_identical");

   function nl_object_diff (arg1 : access nl_object; arg2 : access nl_object) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/object.h:38
   pragma Import (C, nl_object_diff, "nl_object_diff");

   function nl_object_diff64 (arg1 : access nl_object; arg2 : access nl_object) return x86_64_linux_gnu_bits_stdint_uintn_h.uint64_t;  -- ../include/netlink/object.h:40
   pragma Import (C, nl_object_diff64, "nl_object_diff64");

   function nl_object_match_filter (arg1 : access nl_object; arg2 : access nl_object) return int;  -- ../include/netlink/object.h:42
   pragma Import (C, nl_object_match_filter, "nl_object_match_filter");

   function nl_object_attrs2str
     (arg1 : access nl_object;
      arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;
      arg3 : Interfaces.C.Strings.chars_ptr;
      arg4 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/object.h:44
   pragma Import (C, nl_object_attrs2str, "nl_object_attrs2str");

   function nl_object_attr_list
     (arg1 : access nl_object;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/object.h:47
   pragma Import (C, nl_object_attr_list, "nl_object_attr_list");

   procedure nl_object_keygen
     (arg1 : access nl_object;
      arg2 : access x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;
      arg3 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/object.h:49
   pragma Import (C, nl_object_keygen, "nl_object_keygen");

   procedure nl_object_mark (arg1 : access nl_object);  -- ../include/netlink/object.h:53
   pragma Import (C, nl_object_mark, "nl_object_mark");

   procedure nl_object_unmark (arg1 : access nl_object);  -- ../include/netlink/object.h:54
   pragma Import (C, nl_object_unmark, "nl_object_unmark");

   function nl_object_is_marked (arg1 : access nl_object) return int;  -- ../include/netlink/object.h:55
   pragma Import (C, nl_object_is_marked, "nl_object_is_marked");

   function nl_object_get_refcnt (arg1 : access nl_object) return int;  -- ../include/netlink/object.h:58
   pragma Import (C, nl_object_get_refcnt, "nl_object_get_refcnt");

   function nl_object_get_cache (arg1 : access nl_object) return access nl_cache;  -- ../include/netlink/object.h:59
   pragma Import (C, nl_object_get_cache, "nl_object_get_cache");

   function nl_object_get_type (arg1 : access constant nl_object) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/object.h:60
   pragma Import (C, nl_object_get_type, "nl_object_get_type");

   function nl_object_get_msgtype (arg1 : access constant nl_object) return int;  -- ../include/netlink/object.h:61
   pragma Import (C, nl_object_get_msgtype, "nl_object_get_msgtype");

   function nl_object_get_ops (arg1 : access constant nl_object) return access nl_object_ops;  -- ../include/netlink/object.h:62
   pragma Import (C, nl_object_get_ops, "nl_object_get_ops");

   function nl_object_get_id_attrs (obj : access nl_object) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/object.h:63
   pragma Import (C, nl_object_get_id_attrs, "nl_object_get_id_attrs");

   function nl_object_priv (obj : access nl_object) return System.Address;  -- ../include/netlink/object.h:66
   pragma Import (C, nl_object_priv, "nl_object_priv");

end netlink_object_h;
