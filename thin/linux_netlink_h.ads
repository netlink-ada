pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with linux_socket_h;
with asm_generic_int_ll64_h;

package linux_netlink_h is

   NETLINK_ROUTE : constant := 0;  --  /usr/include/linux/netlink.h:9
   NETLINK_UNUSED : constant := 1;  --  /usr/include/linux/netlink.h:10
   NETLINK_USERSOCK : constant := 2;  --  /usr/include/linux/netlink.h:11
   NETLINK_FIREWALL : constant := 3;  --  /usr/include/linux/netlink.h:12
   NETLINK_SOCK_DIAG : constant := 4;  --  /usr/include/linux/netlink.h:13
   NETLINK_NFLOG : constant := 5;  --  /usr/include/linux/netlink.h:14
   NETLINK_XFRM : constant := 6;  --  /usr/include/linux/netlink.h:15
   NETLINK_SELINUX : constant := 7;  --  /usr/include/linux/netlink.h:16
   NETLINK_ISCSI : constant := 8;  --  /usr/include/linux/netlink.h:17
   NETLINK_AUDIT : constant := 9;  --  /usr/include/linux/netlink.h:18
   NETLINK_FIB_LOOKUP : constant := 10;  --  /usr/include/linux/netlink.h:19
   NETLINK_CONNECTOR : constant := 11;  --  /usr/include/linux/netlink.h:20
   NETLINK_NETFILTER : constant := 12;  --  /usr/include/linux/netlink.h:21
   NETLINK_IP6_FW : constant := 13;  --  /usr/include/linux/netlink.h:22
   NETLINK_DNRTMSG : constant := 14;  --  /usr/include/linux/netlink.h:23
   NETLINK_KOBJECT_UEVENT : constant := 15;  --  /usr/include/linux/netlink.h:24
   NETLINK_GENERIC : constant := 16;  --  /usr/include/linux/netlink.h:25

   NETLINK_SCSITRANSPORT : constant := 18;  --  /usr/include/linux/netlink.h:27
   NETLINK_ECRYPTFS : constant := 19;  --  /usr/include/linux/netlink.h:28
   NETLINK_RDMA : constant := 20;  --  /usr/include/linux/netlink.h:29
   NETLINK_CRYPTO : constant := 21;  --  /usr/include/linux/netlink.h:30
   NETLINK_SMC : constant := 22;  --  /usr/include/linux/netlink.h:31
   --  unsupported macro: NETLINK_INET_DIAG NETLINK_SOCK_DIAG

   MAX_LINKS : constant := 32;  --  /usr/include/linux/netlink.h:35

   NLM_F_REQUEST : constant := 16#01#;  --  /usr/include/linux/netlink.h:54
   NLM_F_MULTI : constant := 16#02#;  --  /usr/include/linux/netlink.h:55
   NLM_F_ACK : constant := 16#04#;  --  /usr/include/linux/netlink.h:56
   NLM_F_ECHO : constant := 16#08#;  --  /usr/include/linux/netlink.h:57
   NLM_F_DUMP_INTR : constant := 16#10#;  --  /usr/include/linux/netlink.h:58
   NLM_F_DUMP_FILTERED : constant := 16#20#;  --  /usr/include/linux/netlink.h:59

   NLM_F_ROOT : constant := 16#100#;  --  /usr/include/linux/netlink.h:62
   NLM_F_MATCH : constant := 16#200#;  --  /usr/include/linux/netlink.h:63
   NLM_F_ATOMIC : constant := 16#400#;  --  /usr/include/linux/netlink.h:64
   --  unsupported macro: NLM_F_DUMP (NLM_F_ROOT|NLM_F_MATCH)

   NLM_F_REPLACE : constant := 16#100#;  --  /usr/include/linux/netlink.h:68
   NLM_F_EXCL : constant := 16#200#;  --  /usr/include/linux/netlink.h:69
   NLM_F_CREATE : constant := 16#400#;  --  /usr/include/linux/netlink.h:70
   NLM_F_APPEND : constant := 16#800#;  --  /usr/include/linux/netlink.h:71

   NLM_F_NONREC : constant := 16#100#;  --  /usr/include/linux/netlink.h:74

   NLM_F_CAPPED : constant := 16#100#;  --  /usr/include/linux/netlink.h:77
   NLM_F_ACK_TLVS : constant := 16#200#;  --  /usr/include/linux/netlink.h:78

   NLMSG_ALIGNTO : constant := 4;  --  /usr/include/linux/netlink.h:89
   --  arg-macro: function NLMSG_ALIGN (len)
   --    return  ((len)+NLMSG_ALIGNTO-1) and ~(NLMSG_ALIGNTO-1) ;
   --  unsupported macro: NLMSG_HDRLEN ((int) NLMSG_ALIGN(sizeof(struct nlmsghdr)))
   --  arg-macro: function NLMSG_LENGTH (len)
   --    return (len) + NLMSG_HDRLEN;
   --  arg-macro: procedure NLMSG_SPACE (len)
   --    NLMSG_ALIGN(NLMSG_LENGTH(len))
   --  arg-macro: function NLMSG_DATA (nlh)
   --    return (void*)(((char*)nlh) + NLMSG_LENGTH(0));
   --  arg-macro: function NLMSG_NEXT (nlh, len)
   --    return (len) -= NLMSG_ALIGN((nlh).nlmsg_len), (struct nlmsghdr*)(((char*)(nlh)) + NLMSG_ALIGN((nlh).nlmsg_len));
   --  arg-macro: function NLMSG_OK (nlh, len)
   --    return (len) >= (int)sizeof(struct nlmsghdr)  and then  (nlh).nlmsg_len >= sizeof(struct nlmsghdr)  and then  (nlh).nlmsg_len <= (len);
   --  arg-macro: function NLMSG_PAYLOAD (nlh, len)
   --    return (nlh).nlmsg_len - NLMSG_SPACE((len));

   NLMSG_NOOP : constant := 16#1#;  --  /usr/include/linux/netlink.h:102
   NLMSG_ERROR : constant := 16#2#;  --  /usr/include/linux/netlink.h:103
   NLMSG_DONE : constant := 16#3#;  --  /usr/include/linux/netlink.h:104
   NLMSG_OVERRUN : constant := 16#4#;  --  /usr/include/linux/netlink.h:105

   NLMSG_MIN_TYPE : constant := 16#10#;  --  /usr/include/linux/netlink.h:107

   NETLINK_ADD_MEMBERSHIP : constant := 1;  --  /usr/include/linux/netlink.h:145
   NETLINK_DROP_MEMBERSHIP : constant := 2;  --  /usr/include/linux/netlink.h:146
   NETLINK_PKTINFO : constant := 3;  --  /usr/include/linux/netlink.h:147
   NETLINK_BROADCAST_ERROR : constant := 4;  --  /usr/include/linux/netlink.h:148
   NETLINK_NO_ENOBUFS : constant := 5;  --  /usr/include/linux/netlink.h:149
   NETLINK_RX_RING : constant := 6;  --  /usr/include/linux/netlink.h:150
   NETLINK_TX_RING : constant := 7;  --  /usr/include/linux/netlink.h:151
   NETLINK_LISTEN_ALL_NSID : constant := 8;  --  /usr/include/linux/netlink.h:152
   NETLINK_LIST_MEMBERSHIPS : constant := 9;  --  /usr/include/linux/netlink.h:153
   NETLINK_CAP_ACK : constant := 10;  --  /usr/include/linux/netlink.h:154
   NETLINK_EXT_ACK : constant := 11;  --  /usr/include/linux/netlink.h:155
   --  unsupported macro: NL_MMAP_MSG_ALIGNMENT NLMSG_ALIGNTO
   --  arg-macro: procedure NL_MMAP_MSG_ALIGN (sz)
   --    __ALIGN_KERNEL(sz, NL_MMAP_MSG_ALIGNMENT)
   --  unsupported macro: NL_MMAP_HDRLEN NL_MMAP_MSG_ALIGN(sizeof(struct nl_mmap_hdr))

   NET_MAJOR : constant := 36;  --  /usr/include/linux/netlink.h:190

   NLA_F_NESTED : constant := (2 ** 15);  --  /usr/include/linux/netlink.h:221
   NLA_F_NET_BYTEORDER : constant := (2 ** 14);  --  /usr/include/linux/netlink.h:222
   --  unsupported macro: NLA_TYPE_MASK ~(NLA_F_NESTED | NLA_F_NET_BYTEORDER)

   NLA_ALIGNTO : constant := 4;  --  /usr/include/linux/netlink.h:225
   --  arg-macro: function NLA_ALIGN (len)
   --    return ((len) + NLA_ALIGNTO - 1) and ~(NLA_ALIGNTO - 1);
   --  unsupported macro: NLA_HDRLEN ((int) NLA_ALIGN(sizeof(struct nlattr)))

   type sockaddr_nl is record
      nl_family : aliased linux_socket_h.uu_kernel_sa_family_t;  -- /usr/include/linux/netlink.h:38
      nl_pad : aliased unsigned_short;  -- /usr/include/linux/netlink.h:39
      nl_pid : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/netlink.h:40
      nl_groups : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/netlink.h:41
   end record;
   pragma Convention (C_Pass_By_Copy, sockaddr_nl);  -- /usr/include/linux/netlink.h:37

   type nlmsghdr is record
      nlmsg_len : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/netlink.h:45
      nlmsg_type : aliased asm_generic_int_ll64_h.uu_u16;  -- /usr/include/linux/netlink.h:46
      nlmsg_flags : aliased asm_generic_int_ll64_h.uu_u16;  -- /usr/include/linux/netlink.h:47
      nlmsg_seq : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/netlink.h:48
      nlmsg_pid : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/netlink.h:49
   end record;
   pragma Convention (C_Pass_By_Copy, nlmsghdr);  -- /usr/include/linux/netlink.h:44

   type nlmsgerr is record
      error : aliased int;  -- /usr/include/linux/netlink.h:110
      msg : aliased nlmsghdr;  -- /usr/include/linux/netlink.h:111
   end record;
   pragma Convention (C_Pass_By_Copy, nlmsgerr);  -- /usr/include/linux/netlink.h:109

   subtype nlmsgerr_attrs is unsigned;
   NLMSGERR_ATTR_UNUSED : constant unsigned := 0;
   NLMSGERR_ATTR_MSG : constant unsigned := 1;
   NLMSGERR_ATTR_OFFS : constant unsigned := 2;
   NLMSGERR_ATTR_COOKIE : constant unsigned := 3;
   uu_NLMSGERR_ATTR_MAX : constant unsigned := 4;
   NLMSGERR_ATTR_MAX : constant unsigned := 3;  -- /usr/include/linux/netlink.h:135

   type nl_pktinfo is record
      group : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/netlink.h:158
   end record;
   pragma Convention (C_Pass_By_Copy, nl_pktinfo);  -- /usr/include/linux/netlink.h:157

   type nl_mmap_req is record
      nm_block_size : aliased unsigned;  -- /usr/include/linux/netlink.h:162
      nm_block_nr : aliased unsigned;  -- /usr/include/linux/netlink.h:163
      nm_frame_size : aliased unsigned;  -- /usr/include/linux/netlink.h:164
      nm_frame_nr : aliased unsigned;  -- /usr/include/linux/netlink.h:165
   end record;
   pragma Convention (C_Pass_By_Copy, nl_mmap_req);  -- /usr/include/linux/netlink.h:161

   type nl_mmap_hdr is record
      nm_status : aliased unsigned;  -- /usr/include/linux/netlink.h:169
      nm_len : aliased unsigned;  -- /usr/include/linux/netlink.h:170
      nm_group : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/netlink.h:171
      nm_pid : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/netlink.h:173
      nm_uid : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/netlink.h:174
      nm_gid : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/netlink.h:175
   end record;
   pragma Convention (C_Pass_By_Copy, nl_mmap_hdr);  -- /usr/include/linux/netlink.h:168

   type nl_mmap_status is 
     (NL_MMAP_STATUS_UNUSED,
      NL_MMAP_STATUS_RESERVED,
      NL_MMAP_STATUS_VALID,
      NL_MMAP_STATUS_COPY,
      NL_MMAP_STATUS_SKIP);
   pragma Convention (C, nl_mmap_status);  -- /usr/include/linux/netlink.h:178

   type nlattr is record
      nla_len : aliased asm_generic_int_ll64_h.uu_u16;  -- /usr/include/linux/netlink.h:207
      nla_type : aliased asm_generic_int_ll64_h.uu_u16;  -- /usr/include/linux/netlink.h:208
   end record;
   pragma Convention (C_Pass_By_Copy, nlattr);  -- /usr/include/linux/netlink.h:206

   type nla_bitfield32 is record
      value : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/netlink.h:243
      selector : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/netlink.h:244
   end record;
   pragma Convention (C_Pass_By_Copy, nla_bitfield32);  -- /usr/include/linux/netlink.h:242

end linux_netlink_h;
