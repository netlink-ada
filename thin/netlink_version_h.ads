pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package netlink_version_h is

   LIBNL_STRING : aliased constant String := "libnl 3.5.0" & ASCII.NUL;  --  ../include/netlink/version.h:11
   LIBNL_VERSION : aliased constant String := "3.5.0" & ASCII.NUL;  --  ../include/netlink/version.h:12

   LIBNL_VER_MAJ : constant := 3;  --  ../include/netlink/version.h:14
   LIBNL_VER_MIN : constant := 5;  --  ../include/netlink/version.h:15
   LIBNL_VER_MIC : constant := 0;  --  ../include/netlink/version.h:16
   --  arg-macro: function LIBNL_VER (maj, min)
   --    return (maj) << 8 or (min);
   --  unsupported macro: LIBNL_VER_NUM LIBNL_VER(LIBNL_VER_MAJ, LIBNL_VER_MIN)

   LIBNL_CURRENT : constant := 226;  --  ../include/netlink/version.h:20
   LIBNL_REVISION : constant := 0;  --  ../include/netlink/version.h:21
   LIBNL_AGE : constant := 26;  --  ../include/netlink/version.h:22

   nl_ver_num : aliased constant int;  -- ../include/netlink/version.h:26
   pragma Import (C, nl_ver_num, "nl_ver_num");

   nl_ver_maj : aliased constant int;  -- ../include/netlink/version.h:27
   pragma Import (C, nl_ver_maj, "nl_ver_maj");

   nl_ver_min : aliased constant int;  -- ../include/netlink/version.h:28
   pragma Import (C, nl_ver_min, "nl_ver_min");

   nl_ver_mic : aliased constant int;  -- ../include/netlink/version.h:29
   pragma Import (C, nl_ver_mic, "nl_ver_mic");

end netlink_version_h;
