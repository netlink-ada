pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with x86_64_linux_gnu_bits_stdint_uintn_h;
with Interfaces.C.Strings;
with nlada_stddef_h;
with System;
limited with netlink_route_link_h;

package netlink_route_link_inet6_h is

   package stddef_h renames nlada_stddef_h;

   type nl_addr is null record;   -- incomplete struct

   function rtnl_link_inet6_addrgenmode2str
     (arg1 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link/inet6.h:15
   pragma Import (C, rtnl_link_inet6_addrgenmode2str, "rtnl_link_inet6_addrgenmode2str");

   function rtnl_link_inet6_str2addrgenmode (mode : Interfaces.C.Strings.chars_ptr) return x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;  -- ../include/netlink/route/link/inet6.h:19
   pragma Import (C, rtnl_link_inet6_str2addrgenmode, "rtnl_link_inet6_str2addrgenmode");

   function rtnl_link_inet6_get_token (arg1 : access netlink_route_link_h.rtnl_link; arg2 : System.Address) return int;  -- ../include/netlink/route/link/inet6.h:21
   pragma Import (C, rtnl_link_inet6_get_token, "rtnl_link_inet6_get_token");

   function rtnl_link_inet6_set_token (arg1 : access netlink_route_link_h.rtnl_link; arg2 : access nl_addr) return int;  -- ../include/netlink/route/link/inet6.h:24
   pragma Import (C, rtnl_link_inet6_set_token, "rtnl_link_inet6_set_token");

   function rtnl_link_inet6_get_addr_gen_mode (arg1 : access netlink_route_link_h.rtnl_link; arg2 : access x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t) return int;  -- ../include/netlink/route/link/inet6.h:27
   pragma Import (C, rtnl_link_inet6_get_addr_gen_mode, "rtnl_link_inet6_get_addr_gen_mode");

   function rtnl_link_inet6_set_addr_gen_mode (arg1 : access netlink_route_link_h.rtnl_link; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t) return int;  -- ../include/netlink/route/link/inet6.h:30
   pragma Import (C, rtnl_link_inet6_set_addr_gen_mode, "rtnl_link_inet6_set_addr_gen_mode");

   function rtnl_link_inet6_get_flags (arg1 : access netlink_route_link_h.rtnl_link; arg2 : access x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t) return int;  -- ../include/netlink/route/link/inet6.h:33
   pragma Import (C, rtnl_link_inet6_get_flags, "rtnl_link_inet6_get_flags");

   function rtnl_link_inet6_set_flags (arg1 : access netlink_route_link_h.rtnl_link; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t) return int;  -- ../include/netlink/route/link/inet6.h:36
   pragma Import (C, rtnl_link_inet6_set_flags, "rtnl_link_inet6_set_flags");

   function rtnl_link_inet6_flags2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link/inet6.h:40
   pragma Import (C, rtnl_link_inet6_flags2str, "rtnl_link_inet6_flags2str");

   function rtnl_link_inet6_str2flags (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/link/inet6.h:41
   pragma Import (C, rtnl_link_inet6_str2flags, "rtnl_link_inet6_str2flags");

end netlink_route_link_inet6_h;
