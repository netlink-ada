pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with asm_generic_int_ll64_h;
with linux_socket_h;

package linux_rtnetlink_h is

   RTNL_FAMILY_IPMR : constant := 128;  --  /usr/include/linux/rtnetlink.h:14
   RTNL_FAMILY_IP6MR : constant := 129;  --  /usr/include/linux/rtnetlink.h:15
   RTNL_FAMILY_MAX : constant := 129;  --  /usr/include/linux/rtnetlink.h:16
   --  unsupported macro: RTM_BASE RTM_BASE
   --  unsupported macro: RTM_NEWLINK RTM_NEWLINK
   --  unsupported macro: RTM_DELLINK RTM_DELLINK
   --  unsupported macro: RTM_GETLINK RTM_GETLINK
   --  unsupported macro: RTM_SETLINK RTM_SETLINK
   --  unsupported macro: RTM_NEWADDR RTM_NEWADDR
   --  unsupported macro: RTM_DELADDR RTM_DELADDR
   --  unsupported macro: RTM_GETADDR RTM_GETADDR
   --  unsupported macro: RTM_NEWROUTE RTM_NEWROUTE
   --  unsupported macro: RTM_DELROUTE RTM_DELROUTE
   --  unsupported macro: RTM_GETROUTE RTM_GETROUTE
   --  unsupported macro: RTM_NEWNEIGH RTM_NEWNEIGH
   --  unsupported macro: RTM_DELNEIGH RTM_DELNEIGH
   --  unsupported macro: RTM_GETNEIGH RTM_GETNEIGH
   --  unsupported macro: RTM_NEWRULE RTM_NEWRULE
   --  unsupported macro: RTM_DELRULE RTM_DELRULE
   --  unsupported macro: RTM_GETRULE RTM_GETRULE
   --  unsupported macro: RTM_NEWQDISC RTM_NEWQDISC
   --  unsupported macro: RTM_DELQDISC RTM_DELQDISC
   --  unsupported macro: RTM_GETQDISC RTM_GETQDISC
   --  unsupported macro: RTM_NEWTCLASS RTM_NEWTCLASS
   --  unsupported macro: RTM_DELTCLASS RTM_DELTCLASS
   --  unsupported macro: RTM_GETTCLASS RTM_GETTCLASS
   --  unsupported macro: RTM_NEWTFILTER RTM_NEWTFILTER
   --  unsupported macro: RTM_DELTFILTER RTM_DELTFILTER
   --  unsupported macro: RTM_GETTFILTER RTM_GETTFILTER
   --  unsupported macro: RTM_NEWACTION RTM_NEWACTION
   --  unsupported macro: RTM_DELACTION RTM_DELACTION
   --  unsupported macro: RTM_GETACTION RTM_GETACTION
   --  unsupported macro: RTM_NEWPREFIX RTM_NEWPREFIX
   --  unsupported macro: RTM_GETMULTICAST RTM_GETMULTICAST
   --  unsupported macro: RTM_GETANYCAST RTM_GETANYCAST
   --  unsupported macro: RTM_NEWNEIGHTBL RTM_NEWNEIGHTBL
   --  unsupported macro: RTM_GETNEIGHTBL RTM_GETNEIGHTBL
   --  unsupported macro: RTM_SETNEIGHTBL RTM_SETNEIGHTBL
   --  unsupported macro: RTM_NEWNDUSEROPT RTM_NEWNDUSEROPT
   --  unsupported macro: RTM_NEWADDRLABEL RTM_NEWADDRLABEL
   --  unsupported macro: RTM_DELADDRLABEL RTM_DELADDRLABEL
   --  unsupported macro: RTM_GETADDRLABEL RTM_GETADDRLABEL
   --  unsupported macro: RTM_GETDCB RTM_GETDCB
   --  unsupported macro: RTM_SETDCB RTM_SETDCB
   --  unsupported macro: RTM_NEWNETCONF RTM_NEWNETCONF
   --  unsupported macro: RTM_DELNETCONF RTM_DELNETCONF
   --  unsupported macro: RTM_GETNETCONF RTM_GETNETCONF
   --  unsupported macro: RTM_NEWMDB RTM_NEWMDB
   --  unsupported macro: RTM_DELMDB RTM_DELMDB
   --  unsupported macro: RTM_GETMDB RTM_GETMDB
   --  unsupported macro: RTM_NEWNSID RTM_NEWNSID
   --  unsupported macro: RTM_DELNSID RTM_DELNSID
   --  unsupported macro: RTM_GETNSID RTM_GETNSID
   --  unsupported macro: RTM_NEWSTATS RTM_NEWSTATS
   --  unsupported macro: RTM_GETSTATS RTM_GETSTATS
   --  unsupported macro: RTM_NEWCACHEREPORT RTM_NEWCACHEREPORT
   --  unsupported macro: RTM_NEWCHAIN RTM_NEWCHAIN
   --  unsupported macro: RTM_DELCHAIN RTM_DELCHAIN
   --  unsupported macro: RTM_GETCHAIN RTM_GETCHAIN
   --  unsupported macro: RTM_MAX (((__RTM_MAX + 3) & ~3) - 1)
   --  unsupported macro: RTM_NR_MSGTYPES (RTM_MAX + 1 - RTM_BASE)
   --  unsupported macro: RTM_NR_FAMILIES (RTM_NR_MSGTYPES >> 2)
   --  arg-macro: function RTM_FAM (cmd)
   --    return ((cmd) - RTM_BASE) >> 2;

   RTA_ALIGNTO : constant := 4;  --  /usr/include/linux/rtnetlink.h:181
   --  arg-macro: function RTA_ALIGN (len)
   --    return  ((len)+RTA_ALIGNTO-1) and ~(RTA_ALIGNTO-1) ;
   --  arg-macro: function RTA_OK (rta, len)
   --    return (len) >= (int)sizeof(struct rtattr)  and then  (rta).rta_len >= sizeof(struct rtattr)  and then  (rta).rta_len <= (len);
   --  arg-macro: function RTA_NEXT (rta, attrlen)
   --    return (attrlen) -= RTA_ALIGN((rta).rta_len), (struct rtattr*)(((char*)(rta)) + RTA_ALIGN((rta).rta_len));
   --  arg-macro: function RTA_LENGTH (len)
   --    return RTA_ALIGN(sizeof(struct rtattr)) + (len);
   --  arg-macro: procedure RTA_SPACE (len)
   --    RTA_ALIGN(RTA_LENGTH(len))
   --  arg-macro: function RTA_DATA (rta)
   --    return (void*)(((char*)(rta)) + RTA_LENGTH(0));
   --  arg-macro: function RTA_PAYLOAD (rta)
   --    return (int)((rta).rta_len) - RTA_LENGTH(0);
   --  unsupported macro: RTN_MAX (__RTN_MAX - 1)

   RTPROT_UNSPEC : constant := 0;  --  /usr/include/linux/rtnetlink.h:239
   RTPROT_REDIRECT : constant := 1;  --  /usr/include/linux/rtnetlink.h:240

   RTPROT_KERNEL : constant := 2;  --  /usr/include/linux/rtnetlink.h:242
   RTPROT_BOOT : constant := 3;  --  /usr/include/linux/rtnetlink.h:243
   RTPROT_STATIC : constant := 4;  --  /usr/include/linux/rtnetlink.h:244

   RTPROT_GATED : constant := 8;  --  /usr/include/linux/rtnetlink.h:253
   RTPROT_RA : constant := 9;  --  /usr/include/linux/rtnetlink.h:254
   RTPROT_MRT : constant := 10;  --  /usr/include/linux/rtnetlink.h:255
   RTPROT_ZEBRA : constant := 11;  --  /usr/include/linux/rtnetlink.h:256
   RTPROT_BIRD : constant := 12;  --  /usr/include/linux/rtnetlink.h:257
   RTPROT_DNROUTED : constant := 13;  --  /usr/include/linux/rtnetlink.h:258
   RTPROT_XORP : constant := 14;  --  /usr/include/linux/rtnetlink.h:259
   RTPROT_NTK : constant := 15;  --  /usr/include/linux/rtnetlink.h:260
   RTPROT_DHCP : constant := 16;  --  /usr/include/linux/rtnetlink.h:261
   RTPROT_MROUTED : constant := 17;  --  /usr/include/linux/rtnetlink.h:262
   RTPROT_BABEL : constant := 42;  --  /usr/include/linux/rtnetlink.h:263
   RTPROT_BGP : constant := 186;  --  /usr/include/linux/rtnetlink.h:264
   RTPROT_ISIS : constant := 187;  --  /usr/include/linux/rtnetlink.h:265
   RTPROT_OSPF : constant := 188;  --  /usr/include/linux/rtnetlink.h:266
   RTPROT_RIP : constant := 189;  --  /usr/include/linux/rtnetlink.h:267
   RTPROT_EIGRP : constant := 192;  --  /usr/include/linux/rtnetlink.h:268

   RTM_F_NOTIFY : constant := 16#100#;  --  /usr/include/linux/rtnetlink.h:292
   RTM_F_CLONED : constant := 16#200#;  --  /usr/include/linux/rtnetlink.h:293
   RTM_F_EQUALIZE : constant := 16#400#;  --  /usr/include/linux/rtnetlink.h:294
   RTM_F_PREFIX : constant := 16#800#;  --  /usr/include/linux/rtnetlink.h:295
   RTM_F_LOOKUP_TABLE : constant := 16#1000#;  --  /usr/include/linux/rtnetlink.h:296
   RTM_F_FIB_MATCH : constant := 16#2000#;  --  /usr/include/linux/rtnetlink.h:297
   --  unsupported macro: RTA_MAX (__RTA_MAX - 1)
   --  arg-macro: function RTM_RTA (r)
   --    return (struct rtattr*)(((char*)(r)) + NLMSG_ALIGN(sizeof(struct rtmsg)));
   --  arg-macro: procedure RTM_PAYLOAD (n)
   --    NLMSG_PAYLOAD(n,sizeof(struct rtmsg))

   RTNH_F_DEAD : constant := 1;  --  /usr/include/linux/rtnetlink.h:371
   RTNH_F_PERVASIVE : constant := 2;  --  /usr/include/linux/rtnetlink.h:372
   RTNH_F_ONLINK : constant := 4;  --  /usr/include/linux/rtnetlink.h:373
   RTNH_F_OFFLOAD : constant := 8;  --  /usr/include/linux/rtnetlink.h:374
   RTNH_F_LINKDOWN : constant := 16;  --  /usr/include/linux/rtnetlink.h:375
   RTNH_F_UNRESOLVED : constant := 32;  --  /usr/include/linux/rtnetlink.h:376
   --  unsupported macro: RTNH_COMPARE_MASK (RTNH_F_DEAD | RTNH_F_LINKDOWN | RTNH_F_OFFLOAD)

   RTNH_ALIGNTO : constant := 4;  --  /usr/include/linux/rtnetlink.h:382
   --  arg-macro: function RTNH_ALIGN (len)
   --    return  ((len)+RTNH_ALIGNTO-1) and ~(RTNH_ALIGNTO-1) ;
   --  arg-macro: function RTNH_OK (rtnh, len)
   --    return (rtnh).rtnh_len >= sizeof(struct rtnexthop)  and then  ((int)(rtnh).rtnh_len) <= (len);
   --  arg-macro: function RTNH_NEXT (rtnh)
   --    return (struct rtnexthop*)(((char*)(rtnh)) + RTNH_ALIGN((rtnh).rtnh_len));
   --  arg-macro: function RTNH_LENGTH (len)
   --    return RTNH_ALIGN(sizeof(struct rtnexthop)) + (len);
   --  arg-macro: procedure RTNH_SPACE (len)
   --    RTNH_ALIGN(RTNH_LENGTH(len))
   --  arg-macro: function RTNH_DATA (rtnh)
   --    return (struct rtattr*)(((char*)(rtnh)) + RTNH_LENGTH(0));

   RTNETLINK_HAVE_PEERINFO : constant := 1;  --  /usr/include/linux/rtnetlink.h:406
   --  unsupported macro: RTAX_UNSPEC RTAX_UNSPEC
   --  unsupported macro: RTAX_LOCK RTAX_LOCK
   --  unsupported macro: RTAX_MTU RTAX_MTU
   --  unsupported macro: RTAX_WINDOW RTAX_WINDOW
   --  unsupported macro: RTAX_RTT RTAX_RTT
   --  unsupported macro: RTAX_RTTVAR RTAX_RTTVAR
   --  unsupported macro: RTAX_SSTHRESH RTAX_SSTHRESH
   --  unsupported macro: RTAX_CWND RTAX_CWND
   --  unsupported macro: RTAX_ADVMSS RTAX_ADVMSS
   --  unsupported macro: RTAX_REORDERING RTAX_REORDERING
   --  unsupported macro: RTAX_HOPLIMIT RTAX_HOPLIMIT
   --  unsupported macro: RTAX_INITCWND RTAX_INITCWND
   --  unsupported macro: RTAX_FEATURES RTAX_FEATURES
   --  unsupported macro: RTAX_RTO_MIN RTAX_RTO_MIN
   --  unsupported macro: RTAX_INITRWND RTAX_INITRWND
   --  unsupported macro: RTAX_QUICKACK RTAX_QUICKACK
   --  unsupported macro: RTAX_CC_ALGO RTAX_CC_ALGO
   --  unsupported macro: RTAX_FASTOPEN_NO_COOKIE RTAX_FASTOPEN_NO_COOKIE
   --  unsupported macro: RTAX_MAX (__RTAX_MAX - 1)

   RTAX_FEATURE_ECN : constant := (2 ** 0);  --  /usr/include/linux/rtnetlink.h:456
   RTAX_FEATURE_SACK : constant := (2 ** 1);  --  /usr/include/linux/rtnetlink.h:457
   RTAX_FEATURE_TIMESTAMP : constant := (2 ** 2);  --  /usr/include/linux/rtnetlink.h:458
   RTAX_FEATURE_ALLFRAG : constant := (2 ** 3);  --  /usr/include/linux/rtnetlink.h:459
   --  unsupported macro: RTAX_FEATURE_MASK (RTAX_FEATURE_ECN | RTAX_FEATURE_SACK | RTAX_FEATURE_TIMESTAMP | RTAX_FEATURE_ALLFRAG)
   --  unsupported macro: PREFIX_MAX (__PREFIX_MAX - 1)
   --  unsupported macro: tcm_block_index tcm_parent

   TCM_IFINDEX_MAGIC_BLOCK : constant := (16#FFFFFFFF#);  --  /usr/include/linux/rtnetlink.h:570
   --  unsupported macro: TCA_MAX (__TCA_MAX - 1)
   --  arg-macro: function TCA_RTA (r)
   --    return (struct rtattr*)(((char*)(r)) + NLMSG_ALIGN(sizeof(struct tcmsg)));
   --  arg-macro: procedure TCA_PAYLOAD (n)
   --    NLMSG_PAYLOAD(n,sizeof(struct tcmsg))
   --  unsupported macro: NDUSEROPT_MAX (__NDUSEROPT_MAX - 1)

   RTMGRP_LINK : constant := 1;  --  /usr/include/linux/rtnetlink.h:621
   RTMGRP_NOTIFY : constant := 2;  --  /usr/include/linux/rtnetlink.h:622
   RTMGRP_NEIGH : constant := 4;  --  /usr/include/linux/rtnetlink.h:623
   RTMGRP_TC : constant := 8;  --  /usr/include/linux/rtnetlink.h:624

   RTMGRP_IPV4_IFADDR : constant := 16#10#;  --  /usr/include/linux/rtnetlink.h:626
   RTMGRP_IPV4_MROUTE : constant := 16#20#;  --  /usr/include/linux/rtnetlink.h:627
   RTMGRP_IPV4_ROUTE : constant := 16#40#;  --  /usr/include/linux/rtnetlink.h:628
   RTMGRP_IPV4_RULE : constant := 16#80#;  --  /usr/include/linux/rtnetlink.h:629

   RTMGRP_IPV6_IFADDR : constant := 16#100#;  --  /usr/include/linux/rtnetlink.h:631
   RTMGRP_IPV6_MROUTE : constant := 16#200#;  --  /usr/include/linux/rtnetlink.h:632
   RTMGRP_IPV6_ROUTE : constant := 16#400#;  --  /usr/include/linux/rtnetlink.h:633
   RTMGRP_IPV6_IFINFO : constant := 16#800#;  --  /usr/include/linux/rtnetlink.h:634

   RTMGRP_DECnet_IFADDR : constant := 16#1000#;  --  /usr/include/linux/rtnetlink.h:636
   RTMGRP_DECnet_ROUTE : constant := 16#4000#;  --  /usr/include/linux/rtnetlink.h:637

   RTMGRP_IPV6_PREFIX : constant := 16#20000#;  --  /usr/include/linux/rtnetlink.h:639
   --  unsupported macro: RTNLGRP_NONE RTNLGRP_NONE
   --  unsupported macro: RTNLGRP_LINK RTNLGRP_LINK
   --  unsupported macro: RTNLGRP_NOTIFY RTNLGRP_NOTIFY
   --  unsupported macro: RTNLGRP_NEIGH RTNLGRP_NEIGH
   --  unsupported macro: RTNLGRP_TC RTNLGRP_TC
   --  unsupported macro: RTNLGRP_IPV4_IFADDR RTNLGRP_IPV4_IFADDR
   --  unsupported macro: RTNLGRP_IPV4_MROUTE RTNLGRP_IPV4_MROUTE
   --  unsupported macro: RTNLGRP_IPV4_ROUTE RTNLGRP_IPV4_ROUTE
   --  unsupported macro: RTNLGRP_IPV4_RULE RTNLGRP_IPV4_RULE
   --  unsupported macro: RTNLGRP_IPV6_IFADDR RTNLGRP_IPV6_IFADDR
   --  unsupported macro: RTNLGRP_IPV6_MROUTE RTNLGRP_IPV6_MROUTE
   --  unsupported macro: RTNLGRP_IPV6_ROUTE RTNLGRP_IPV6_ROUTE
   --  unsupported macro: RTNLGRP_IPV6_IFINFO RTNLGRP_IPV6_IFINFO
   --  unsupported macro: RTNLGRP_DECnet_IFADDR RTNLGRP_DECnet_IFADDR
   --  unsupported macro: RTNLGRP_DECnet_ROUTE RTNLGRP_DECnet_ROUTE
   --  unsupported macro: RTNLGRP_DECnet_RULE RTNLGRP_DECnet_RULE
   --  unsupported macro: RTNLGRP_IPV6_PREFIX RTNLGRP_IPV6_PREFIX
   --  unsupported macro: RTNLGRP_IPV6_RULE RTNLGRP_IPV6_RULE
   --  unsupported macro: RTNLGRP_ND_USEROPT RTNLGRP_ND_USEROPT
   --  unsupported macro: RTNLGRP_PHONET_IFADDR RTNLGRP_PHONET_IFADDR
   --  unsupported macro: RTNLGRP_PHONET_ROUTE RTNLGRP_PHONET_ROUTE
   --  unsupported macro: RTNLGRP_DCB RTNLGRP_DCB
   --  unsupported macro: RTNLGRP_IPV4_NETCONF RTNLGRP_IPV4_NETCONF
   --  unsupported macro: RTNLGRP_IPV6_NETCONF RTNLGRP_IPV6_NETCONF
   --  unsupported macro: RTNLGRP_MDB RTNLGRP_MDB
   --  unsupported macro: RTNLGRP_MPLS_ROUTE RTNLGRP_MPLS_ROUTE
   --  unsupported macro: RTNLGRP_NSID RTNLGRP_NSID
   --  unsupported macro: RTNLGRP_MPLS_NETCONF RTNLGRP_MPLS_NETCONF
   --  unsupported macro: RTNLGRP_IPV4_MROUTE_R RTNLGRP_IPV4_MROUTE_R
   --  unsupported macro: RTNLGRP_IPV6_MROUTE_R RTNLGRP_IPV6_MROUTE_R
   --  unsupported macro: RTNLGRP_MAX (__RTNLGRP_MAX - 1)
   --  unsupported macro: TCA_ACT_TAB TCA_ROOT_TAB
   --  unsupported macro: TCAA_MAX TCA_ROOT_TAB
   --  unsupported macro: TCA_ROOT_MAX (__TCA_ROOT_MAX - 1)
   --  arg-macro: function TA_RTA (r)
   --    return (struct rtattr*)(((char*)(r)) + NLMSG_ALIGN(sizeof(struct tcamsg)));
   --  arg-macro: procedure TA_PAYLOAD (n)
   --    NLMSG_PAYLOAD(n,sizeof(struct tcamsg))

   TCA_FLAG_LARGE_DUMP_ON : constant := (2 ** 0);  --  /usr/include/linux/rtnetlink.h:737

   RTEXT_FILTER_VF : constant := (2 ** 0);  --  /usr/include/linux/rtnetlink.h:740
   RTEXT_FILTER_BRVLAN : constant := (2 ** 1);  --  /usr/include/linux/rtnetlink.h:741
   RTEXT_FILTER_BRVLAN_COMPRESSED : constant := (2 ** 2);  --  /usr/include/linux/rtnetlink.h:742
   RTEXT_FILTER_SKIP_STATS : constant := (2 ** 3);  --  /usr/include/linux/rtnetlink.h:743

   type rtattr is record
      rta_len : aliased unsigned_short;  -- /usr/include/linux/rtnetlink.h:175
      rta_type : aliased unsigned_short;  -- /usr/include/linux/rtnetlink.h:176
   end record;
   pragma Convention (C_Pass_By_Copy, rtattr);  -- /usr/include/linux/rtnetlink.h:174

   type rtmsg is record
      rtm_family : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:201
      rtm_dst_len : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:202
      rtm_src_len : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:203
      rtm_tos : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:204
      rtm_table : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:206
      rtm_protocol : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:207
      rtm_scope : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:208
      rtm_type : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:209
      rtm_flags : aliased unsigned;  -- /usr/include/linux/rtnetlink.h:211
   end record;
   pragma Convention (C_Pass_By_Copy, rtmsg);  -- /usr/include/linux/rtnetlink.h:200

   subtype rt_scope_t is unsigned;
   RT_SCOPE_UNIVERSE : constant unsigned := 0;
   RT_SCOPE_SITE : constant unsigned := 200;
   RT_SCOPE_LINK : constant unsigned := 253;
   RT_SCOPE_HOST : constant unsigned := 254;
   RT_SCOPE_NOWHERE : constant unsigned := 255;  -- /usr/include/linux/rtnetlink.h:281

   subtype rt_class_t is unsigned;
   RT_TABLE_UNSPEC : constant unsigned := 0;
   RT_TABLE_COMPAT : constant unsigned := 252;
   RT_TABLE_DEFAULT : constant unsigned := 253;
   RT_TABLE_MAIN : constant unsigned := 254;
   RT_TABLE_LOCAL : constant unsigned := 255;
   RT_TABLE_MAX : constant unsigned := 4294967295;  -- /usr/include/linux/rtnetlink.h:301

   type rtattr_type_t is 
     (RTA_UNSPEC,
      RTA_DST,
      RTA_SRC,
      RTA_IIF,
      RTA_OIF,
      RTA_GATEWAY,
      RTA_PRIORITY,
      RTA_PREFSRC,
      RTA_METRICS,
      RTA_MULTIPATH,
      RTA_PROTOINFO,
      RTA_FLOW,
      RTA_CACHEINFO,
      RTA_SESSION,
      RTA_MP_ALGO,
      RTA_TABLE,
      RTA_MARK,
      RTA_MFC_STATS,
      RTA_VIA,
      RTA_NEWDST,
      RTA_PREF,
      RTA_ENCAP_TYPE,
      RTA_ENCAP,
      RTA_EXPIRES,
      RTA_PAD,
      RTA_UID,
      RTA_TTL_PROPAGATE,
      RTA_IP_PROTO,
      RTA_SPORT,
      RTA_DPORT,
      uu_RTA_MAX);
   pragma Convention (C, rtattr_type_t);  -- /usr/include/linux/rtnetlink.h:314

   type rtnexthop is record
      rtnh_len : aliased unsigned_short;  -- /usr/include/linux/rtnetlink.h:363
      rtnh_flags : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:364
      rtnh_hops : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:365
      rtnh_ifindex : aliased int;  -- /usr/include/linux/rtnetlink.h:366
   end record;
   pragma Convention (C_Pass_By_Copy, rtnexthop);  -- /usr/include/linux/rtnetlink.h:362

   type anon1787_rtvia_addr_array is array (0 .. 0) of aliased asm_generic_int_ll64_h.uu_u8;
   type rtvia is record
      rtvia_family : aliased linux_socket_h.uu_kernel_sa_family_t;  -- /usr/include/linux/rtnetlink.h:393
      rtvia_addr : aliased anon1787_rtvia_addr_array;  -- /usr/include/linux/rtnetlink.h:394
   end record;
   pragma Convention (C_Pass_By_Copy, rtvia);  -- /usr/include/linux/rtnetlink.h:392

   type rta_cacheinfo_struct is record
      rta_clntref : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:400
      rta_lastuse : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:401
      rta_expires : aliased asm_generic_int_ll64_h.uu_s32;  -- /usr/include/linux/rtnetlink.h:402
      rta_error : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:403
      rta_used : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:404
      rta_id : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:407
      rta_ts : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:408
      rta_tsage : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:409
   end record;
   pragma Convention (C_Pass_By_Copy, rta_cacheinfo_struct);  -- /usr/include/linux/rtnetlink.h:399

   type anon1792_ports_struct is record
      sport : aliased asm_generic_int_ll64_h.uu_u16;  -- /usr/include/linux/rtnetlink.h:471
      dport : aliased asm_generic_int_ll64_h.uu_u16;  -- /usr/include/linux/rtnetlink.h:472
   end record;
   pragma Convention (C_Pass_By_Copy, anon1792_ports_struct);
   type anon1792_icmpt_struct is record
      c_type : aliased asm_generic_int_ll64_h.uu_u8;  -- /usr/include/linux/rtnetlink.h:476
      code : aliased asm_generic_int_ll64_h.uu_u8;  -- /usr/include/linux/rtnetlink.h:477
      ident : aliased asm_generic_int_ll64_h.uu_u16;  -- /usr/include/linux/rtnetlink.h:478
   end record;
   pragma Convention (C_Pass_By_Copy, anon1792_icmpt_struct);
   type anon1792_u_union (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            ports : aliased anon1792_ports_struct;  -- /usr/include/linux/rtnetlink.h:473
         when 1 =>
            icmpt : aliased anon1792_icmpt_struct;  -- /usr/include/linux/rtnetlink.h:479
         when others =>
            spi : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:481
      end case;
   end record;
   pragma Convention (C_Pass_By_Copy, anon1792_u_union);
   pragma Unchecked_Union (anon1792_u_union);

   type rta_session_struct is record
      proto : aliased asm_generic_int_ll64_h.uu_u8;  -- /usr/include/linux/rtnetlink.h:465
      pad1 : aliased asm_generic_int_ll64_h.uu_u8;  -- /usr/include/linux/rtnetlink.h:466
      pad2 : aliased asm_generic_int_ll64_h.uu_u16;  -- /usr/include/linux/rtnetlink.h:467
      u : aliased anon1792_u_union;  -- /usr/include/linux/rtnetlink.h:482
   end record;
   pragma Convention (C_Pass_By_Copy, rta_session_struct);  -- /usr/include/linux/rtnetlink.h:464

   type rta_mfc_stats_struct is record
      mfcs_packets : aliased asm_generic_int_ll64_h.uu_u64;  -- /usr/include/linux/rtnetlink.h:486
      mfcs_bytes : aliased asm_generic_int_ll64_h.uu_u64;  -- /usr/include/linux/rtnetlink.h:487
      mfcs_wrong_if : aliased asm_generic_int_ll64_h.uu_u64;  -- /usr/include/linux/rtnetlink.h:488
   end record;
   pragma Convention (C_Pass_By_Copy, rta_mfc_stats_struct);  -- /usr/include/linux/rtnetlink.h:485

   type rtgenmsg is record
      rtgen_family : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:496
   end record;
   pragma Convention (C_Pass_By_Copy, rtgenmsg);  -- /usr/include/linux/rtnetlink.h:495

   type ifinfomsg is record
      ifi_family : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:509
      uu_ifi_pad : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:510
      ifi_type : aliased unsigned_short;  -- /usr/include/linux/rtnetlink.h:511
      ifi_index : aliased int;  -- /usr/include/linux/rtnetlink.h:512
      ifi_flags : aliased unsigned;  -- /usr/include/linux/rtnetlink.h:513
      ifi_change : aliased unsigned;  -- /usr/include/linux/rtnetlink.h:514
   end record;
   pragma Convention (C_Pass_By_Copy, ifinfomsg);  -- /usr/include/linux/rtnetlink.h:508

   type prefixmsg is record
      prefix_family : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:522
      prefix_pad1 : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:523
      prefix_pad2 : aliased unsigned_short;  -- /usr/include/linux/rtnetlink.h:524
      prefix_ifindex : aliased int;  -- /usr/include/linux/rtnetlink.h:525
      prefix_type : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:526
      prefix_len : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:527
      prefix_flags : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:528
      prefix_pad3 : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:529
   end record;
   pragma Convention (C_Pass_By_Copy, prefixmsg);  -- /usr/include/linux/rtnetlink.h:521

   type prefix_cacheinfo is record
      preferred_time : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:543
      valid_time : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:544
   end record;
   pragma Convention (C_Pass_By_Copy, prefix_cacheinfo);  -- /usr/include/linux/rtnetlink.h:542

   type tcmsg is record
      tcm_family : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:553
      tcm_u_pad1 : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:554
      tcm_u_pad2 : aliased unsigned_short;  -- /usr/include/linux/rtnetlink.h:555
      tcm_ifindex : aliased int;  -- /usr/include/linux/rtnetlink.h:556
      tcm_handle : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:557
      tcm_parent : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:558
      tcm_info : aliased asm_generic_int_ll64_h.uu_u32;  -- /usr/include/linux/rtnetlink.h:563
   end record;
   pragma Convention (C_Pass_By_Copy, tcmsg);  -- /usr/include/linux/rtnetlink.h:552

   type nduseroptmsg is record
      nduseropt_family : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:601
      nduseropt_pad1 : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:602
      nduseropt_opts_len : aliased unsigned_short;  -- /usr/include/linux/rtnetlink.h:603
      nduseropt_ifindex : aliased int;  -- /usr/include/linux/rtnetlink.h:604
      nduseropt_icmp_type : aliased asm_generic_int_ll64_h.uu_u8;  -- /usr/include/linux/rtnetlink.h:605
      nduseropt_icmp_code : aliased asm_generic_int_ll64_h.uu_u8;  -- /usr/include/linux/rtnetlink.h:606
      nduseropt_pad2 : aliased unsigned_short;  -- /usr/include/linux/rtnetlink.h:607
      nduseropt_pad3 : aliased unsigned;  -- /usr/include/linux/rtnetlink.h:608
   end record;
   pragma Convention (C_Pass_By_Copy, nduseroptmsg);  -- /usr/include/linux/rtnetlink.h:600

   type rtnetlink_groups is 
     (RTNLGRP_NONE,
      RTNLGRP_LINK,
      RTNLGRP_NOTIFY,
      RTNLGRP_NEIGH,
      RTNLGRP_TC,
      RTNLGRP_IPV4_IFADDR,
      RTNLGRP_IPV4_MROUTE,
      RTNLGRP_IPV4_ROUTE,
      RTNLGRP_IPV4_RULE,
      RTNLGRP_IPV6_IFADDR,
      RTNLGRP_IPV6_MROUTE,
      RTNLGRP_IPV6_ROUTE,
      RTNLGRP_IPV6_IFINFO,
      RTNLGRP_DECnet_IFADDR,
      RTNLGRP_NOP2,
      RTNLGRP_DECnet_ROUTE,
      RTNLGRP_DECnet_RULE,
      RTNLGRP_NOP4,
      RTNLGRP_IPV6_PREFIX,
      RTNLGRP_IPV6_RULE,
      RTNLGRP_ND_USEROPT,
      RTNLGRP_PHONET_IFADDR,
      RTNLGRP_PHONET_ROUTE,
      RTNLGRP_DCB,
      RTNLGRP_IPV4_NETCONF,
      RTNLGRP_IPV6_NETCONF,
      RTNLGRP_MDB,
      RTNLGRP_MPLS_ROUTE,
      RTNLGRP_NSID,
      RTNLGRP_MPLS_NETCONF,
      RTNLGRP_IPV4_MROUTE_R,
      RTNLGRP_IPV6_MROUTE_R,
      uu_RTNLGRP_MAX);
   pragma Convention (C, rtnetlink_groups);  -- /usr/include/linux/rtnetlink.h:642

   type tcamsg is record
      tca_family : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:711
      tca_u_pad1 : aliased unsigned_char;  -- /usr/include/linux/rtnetlink.h:712
      tca_u_pad2 : aliased unsigned_short;  -- /usr/include/linux/rtnetlink.h:713
   end record;
   pragma Convention (C_Pass_By_Copy, tcamsg);  -- /usr/include/linux/rtnetlink.h:710

end linux_rtnetlink_h;
