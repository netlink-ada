pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with System;
with x86_64_linux_gnu_bits_types_FILE_h;
with nlada_stddef_h;

package netlink_types_h is

   package stddef_h renames nlada_stddef_h;

   --  unsupported macro: NL_DUMP_MAX (__NL_DUMP_MAX - 1)
   type nl_dump_type is 
     (NL_DUMP_LINE,
      NL_DUMP_DETAILS,
      NL_DUMP_STATS,
      uu_NL_DUMP_MAX);
   pragma Convention (C, nl_dump_type);  -- ../include/netlink/types.h:15

   type nl_dump_params;
   type nl_dump_params is record
      dp_type : aliased nl_dump_type;  -- ../include/netlink/types.h:32
      dp_prefix : aliased int;  -- ../include/netlink/types.h:38
      dp_print_index : aliased int;  -- ../include/netlink/types.h:43
      dp_dump_msgtype : aliased int;  -- ../include/netlink/types.h:48
      dp_cb : access procedure (arg1 : access nl_dump_params; arg2 : Interfaces.C.Strings.chars_ptr);  -- ../include/netlink/types.h:57
      dp_nl_cb : access procedure (arg1 : access nl_dump_params; arg2 : int);  -- ../include/netlink/types.h:67
      dp_data : System.Address;  -- ../include/netlink/types.h:72
      dp_fd : access x86_64_linux_gnu_bits_types_FILE_h.FILE;  -- ../include/netlink/types.h:77
      dp_buf : Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/types.h:82
      dp_buflen : aliased stddef_h.size_t;  -- ../include/netlink/types.h:87
      dp_pre_dump : aliased int;  -- ../include/netlink/types.h:93
      dp_ivar : aliased int;  -- ../include/netlink/types.h:99
      dp_line : aliased unsigned;  -- ../include/netlink/types.h:101
   end record;
   pragma Convention (C_Pass_By_Copy, nl_dump_params);  -- ../include/netlink/types.h:27

end netlink_types_h;
