pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package netlink_list_h is

   --  arg-macro: function nl_container_of (ptr, type, member)
   --    return { const __typeof__( ((type *)0).member ) *__mptr := (ptr); (type *)( (char *)__mptr - (offsetof(type, member)));};
   --  arg-macro: procedure nl_list_entry (ptr, type, member)
   --    nl_container_of(ptr, type, member)
   --  arg-macro: function nl_list_at_tail (pos, head, member)
   --    return (pos).member.next = (head);
   --  arg-macro: function nl_list_at_head (pos, head, member)
   --    return (pos).member.prev = (head);
   --  arg-macro: procedure NL_LIST_HEAD (name)
   --    struct nl_list_head name := { and(name), and(name) }
   --  arg-macro: procedure nl_list_first_entry (head, type, member)
   --    nl_list_entry((head).next, type, member)
   --  arg-macro: procedure nl_list_for_each_entry (pos, head, member)
   --    for (pos := nl_list_entry((head).next, __typeof__(*pos), member); and(pos).member /= (head); (pos) := nl_list_entry((pos).member.next, __typeof__(*(pos)), member))
   --  arg-macro: procedure nl_list_for_each_entry_safe (pos, n, head, member)
   --    for (pos := nl_list_entry((head).next, __typeof__(*pos), member), n := nl_list_entry(pos.member.next, __typeof__(*pos), member); and(pos).member /= (head); pos := n, n := nl_list_entry(n.member.next, __typeof__(*n), member))
   --  arg-macro: procedure nl_init_list_head (head)
   --    do { (head).next := (head); (head).prev := (head); } while (0)
   type nl_list_head;
   type nl_list_head is record
      next : access nl_list_head;  -- ../include/netlink/list.h:13
      prev : access nl_list_head;  -- ../include/netlink/list.h:14
   end record;
   pragma Convention (C_Pass_By_Copy, nl_list_head);  -- ../include/netlink/list.h:11

   procedure NL_INIT_LIST_HEAD (list : access nl_list_head);  -- ../include/netlink/list.h:17
   pragma Import (C, NL_INIT_LIST_HEAD, "NL_INIT_LIST_HEAD");

   --  skipped func __nl_list_add

   procedure nl_list_add_tail (obj : access nl_list_head; head : access nl_list_head);  -- ../include/netlink/list.h:33
   pragma Import (C, nl_list_add_tail, "nl_list_add_tail");

   procedure nl_list_add_head (obj : access nl_list_head; head : access nl_list_head);  -- ../include/netlink/list.h:39
   pragma Import (C, nl_list_add_head, "nl_list_add_head");

   procedure nl_list_del (obj : access nl_list_head);  -- ../include/netlink/list.h:45
   pragma Import (C, nl_list_del, "nl_list_del");

   function nl_list_empty (head : access nl_list_head) return int;  -- ../include/netlink/list.h:51
   pragma Import (C, nl_list_empty, "nl_list_empty");

end netlink_list_h;
