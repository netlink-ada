pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;

package netlink_errno_h is

   NLE_SUCCESS : constant := 0;  --  ../include/netlink/errno.h:13
   NLE_FAILURE : constant := 1;  --  ../include/netlink/errno.h:14
   NLE_INTR : constant := 2;  --  ../include/netlink/errno.h:15
   NLE_BAD_SOCK : constant := 3;  --  ../include/netlink/errno.h:16
   NLE_AGAIN : constant := 4;  --  ../include/netlink/errno.h:17
   NLE_NOMEM : constant := 5;  --  ../include/netlink/errno.h:18
   NLE_EXIST : constant := 6;  --  ../include/netlink/errno.h:19
   NLE_INVAL : constant := 7;  --  ../include/netlink/errno.h:20
   NLE_RANGE : constant := 8;  --  ../include/netlink/errno.h:21
   NLE_MSGSIZE : constant := 9;  --  ../include/netlink/errno.h:22
   NLE_OPNOTSUPP : constant := 10;  --  ../include/netlink/errno.h:23
   NLE_AF_NOSUPPORT : constant := 11;  --  ../include/netlink/errno.h:24
   NLE_OBJ_NOTFOUND : constant := 12;  --  ../include/netlink/errno.h:25
   NLE_NOATTR : constant := 13;  --  ../include/netlink/errno.h:26
   NLE_MISSING_ATTR : constant := 14;  --  ../include/netlink/errno.h:27
   NLE_AF_MISMATCH : constant := 15;  --  ../include/netlink/errno.h:28
   NLE_SEQ_MISMATCH : constant := 16;  --  ../include/netlink/errno.h:29
   NLE_MSG_OVERFLOW : constant := 17;  --  ../include/netlink/errno.h:30
   NLE_MSG_TRUNC : constant := 18;  --  ../include/netlink/errno.h:31
   NLE_NOADDR : constant := 19;  --  ../include/netlink/errno.h:32
   NLE_SRCRT_NOSUPPORT : constant := 20;  --  ../include/netlink/errno.h:33
   NLE_MSG_TOOSHORT : constant := 21;  --  ../include/netlink/errno.h:34
   NLE_MSGTYPE_NOSUPPORT : constant := 22;  --  ../include/netlink/errno.h:35
   NLE_OBJ_MISMATCH : constant := 23;  --  ../include/netlink/errno.h:36
   NLE_NOCACHE : constant := 24;  --  ../include/netlink/errno.h:37
   NLE_BUSY : constant := 25;  --  ../include/netlink/errno.h:38
   NLE_PROTO_MISMATCH : constant := 26;  --  ../include/netlink/errno.h:39
   NLE_NOACCESS : constant := 27;  --  ../include/netlink/errno.h:40
   NLE_PERM : constant := 28;  --  ../include/netlink/errno.h:41
   NLE_PKTLOC_FILE : constant := 29;  --  ../include/netlink/errno.h:42
   NLE_PARSE_ERR : constant := 30;  --  ../include/netlink/errno.h:43
   NLE_NODEV : constant := 31;  --  ../include/netlink/errno.h:44
   NLE_IMMUTABLE : constant := 32;  --  ../include/netlink/errno.h:45
   NLE_DUMP_INTR : constant := 33;  --  ../include/netlink/errno.h:46
   NLE_ATTRSIZE : constant := 34;  --  ../include/netlink/errno.h:47
   --  unsupported macro: NLE_MAX NLE_ATTRSIZE

   function nl_geterror (arg1 : int) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/errno.h:51
   pragma Import (C, nl_geterror, "nl_geterror");

   procedure nl_perror (arg1 : int; arg2 : Interfaces.C.Strings.chars_ptr);  -- ../include/netlink/errno.h:52
   pragma Import (C, nl_perror, "nl_perror");

   function nl_syserr2nlerr (arg1 : int) return int;  -- ../include/netlink/errno.h:53
   pragma Import (C, nl_syserr2nlerr, "nl_syserr2nlerr");

end netlink_errno_h;
