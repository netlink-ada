pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with nlada_stddef_h;
with System;
with Interfaces.C.Strings;
with x86_64_linux_gnu_bits_socket_h;

package netlink_addr_h is

   package stddef_h renames nlada_stddef_h;

   type nlattr is null record;   -- incomplete struct

   type nl_addr is null record;   -- incomplete struct

   function nl_addr_alloc (arg1 : stddef_h.size_t) return access nl_addr;  -- ../include/netlink/addr.h:20
   pragma Import (C, nl_addr_alloc, "nl_addr_alloc");

   function nl_addr_alloc_attr (arg1 : access constant nlattr; arg2 : int) return access nl_addr;  -- ../include/netlink/addr.h:21
   pragma Import (C, nl_addr_alloc_attr, "nl_addr_alloc_attr");

   function nl_addr_build
     (arg1 : int;
      arg2 : System.Address;
      arg3 : stddef_h.size_t) return access nl_addr;  -- ../include/netlink/addr.h:22
   pragma Import (C, nl_addr_build, "nl_addr_build");

   function nl_addr_parse
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : int;
      arg3 : System.Address) return int;  -- ../include/netlink/addr.h:23
   pragma Import (C, nl_addr_parse, "nl_addr_parse");

   function nl_addr_clone (arg1 : access constant nl_addr) return access nl_addr;  -- ../include/netlink/addr.h:24
   pragma Import (C, nl_addr_clone, "nl_addr_clone");

   function nl_addr_get (arg1 : access nl_addr) return access nl_addr;  -- ../include/netlink/addr.h:27
   pragma Import (C, nl_addr_get, "nl_addr_get");

   procedure nl_addr_put (arg1 : access nl_addr);  -- ../include/netlink/addr.h:28
   pragma Import (C, nl_addr_put, "nl_addr_put");

   function nl_addr_shared (arg1 : access constant nl_addr) return int;  -- ../include/netlink/addr.h:29
   pragma Import (C, nl_addr_shared, "nl_addr_shared");

   function nl_addr_cmp (arg1 : access constant nl_addr; arg2 : access constant nl_addr) return int;  -- ../include/netlink/addr.h:31
   pragma Import (C, nl_addr_cmp, "nl_addr_cmp");

   function nl_addr_cmp_prefix (arg1 : access constant nl_addr; arg2 : access constant nl_addr) return int;  -- ../include/netlink/addr.h:33
   pragma Import (C, nl_addr_cmp_prefix, "nl_addr_cmp_prefix");

   function nl_addr_iszero (arg1 : access constant nl_addr) return int;  -- ../include/netlink/addr.h:35
   pragma Import (C, nl_addr_iszero, "nl_addr_iszero");

   function nl_addr_valid (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : int) return int;  -- ../include/netlink/addr.h:36
   pragma Import (C, nl_addr_valid, "nl_addr_valid");

   function nl_addr_guess_family (arg1 : access constant nl_addr) return int;  -- ../include/netlink/addr.h:37
   pragma Import (C, nl_addr_guess_family, "nl_addr_guess_family");

   function nl_addr_fill_sockaddr
     (arg1 : access constant nl_addr;
      arg2 : access x86_64_linux_gnu_bits_socket_h.sockaddr;
      arg3 : access x86_64_linux_gnu_bits_socket_h.socklen_t) return int;  -- ../include/netlink/addr.h:38
   pragma Import (C, nl_addr_fill_sockaddr, "nl_addr_fill_sockaddr");

   function nl_addr_info (arg1 : access constant nl_addr; arg2 : System.Address) return int;  -- ../include/netlink/addr.h:40
   pragma Import (C, nl_addr_info, "nl_addr_info");

   function nl_addr_resolve
     (arg1 : access constant nl_addr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return int;  -- ../include/netlink/addr.h:42
   pragma Import (C, nl_addr_resolve, "nl_addr_resolve");

   procedure nl_addr_set_family (arg1 : access nl_addr; arg2 : int);  -- ../include/netlink/addr.h:45
   pragma Import (C, nl_addr_set_family, "nl_addr_set_family");

   function nl_addr_get_family (arg1 : access constant nl_addr) return int;  -- ../include/netlink/addr.h:46
   pragma Import (C, nl_addr_get_family, "nl_addr_get_family");

   function nl_addr_set_binary_addr
     (arg1 : access nl_addr;
      arg2 : System.Address;
      arg3 : stddef_h.size_t) return int;  -- ../include/netlink/addr.h:47
   pragma Import (C, nl_addr_set_binary_addr, "nl_addr_set_binary_addr");

   function nl_addr_get_binary_addr (arg1 : access constant nl_addr) return System.Address;  -- ../include/netlink/addr.h:49
   pragma Import (C, nl_addr_get_binary_addr, "nl_addr_get_binary_addr");

   function nl_addr_get_len (arg1 : access constant nl_addr) return unsigned;  -- ../include/netlink/addr.h:50
   pragma Import (C, nl_addr_get_len, "nl_addr_get_len");

   procedure nl_addr_set_prefixlen (arg1 : access nl_addr; arg2 : int);  -- ../include/netlink/addr.h:51
   pragma Import (C, nl_addr_set_prefixlen, "nl_addr_set_prefixlen");

   function nl_addr_get_prefixlen (arg1 : access constant nl_addr) return unsigned;  -- ../include/netlink/addr.h:52
   pragma Import (C, nl_addr_get_prefixlen, "nl_addr_get_prefixlen");

   function nl_af2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/addr.h:55
   pragma Import (C, nl_af2str, "nl_af2str");

   function nl_str2af (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/addr.h:56
   pragma Import (C, nl_str2af, "nl_str2af");

   function nl_addr2str
     (arg1 : access constant nl_addr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/addr.h:59
   pragma Import (C, nl_addr2str, "nl_addr2str");

end netlink_addr_h;
