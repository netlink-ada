pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package nlada_stddef_h is

   --  unsupported macro: NULL ((void *)0)
   --  arg-macro: procedure offsetof (TYPE, MEMBER)
   --    __builtin_offsetof (TYPE, MEMBER)
   subtype ptrdiff_t is long;  -- /usr/lib/gcc/x86_64-linux-gnu/8/include/stddef.h:149

   subtype size_t is unsigned_long;  -- /usr/lib/gcc/x86_64-linux-gnu/8/include/stddef.h:216

   subtype wchar_t is int;  -- /usr/lib/gcc/x86_64-linux-gnu/8/include/stddef.h:328

   type max_align_t is record
      uu_max_align_ll : aliased Long_Long_Integer;  -- /usr/lib/gcc/x86_64-linux-gnu/8/include/stddef.h:427
      uu_max_align_ld : aliased long_double;  -- /usr/lib/gcc/x86_64-linux-gnu/8/include/stddef.h:428
   end record;
   pragma Convention (C_Pass_By_Copy, max_align_t);  -- /usr/lib/gcc/x86_64-linux-gnu/8/include/stddef.h:437

end nlada_stddef_h;
