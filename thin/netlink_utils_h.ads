pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;
with System;
with Interfaces.C.Strings;
with nlada_stddef_h;
with x86_64_linux_gnu_bits_stdint_uintn_h;
limited with netlink_types_h;

package netlink_utils_h is

   package stddef_h renames nlada_stddef_h;

   NL_PROB_MIN : constant := 16#0#;  --  ../include/netlink/utils.h:25

   NL_PROB_MAX : constant := 16#ffffffff#;  --  ../include/netlink/utils.h:31
   --  unsupported macro: NL_CAPABILITY_ROUTE_BUILD_MSG_SET_SCOPE NL_CAPABILITY_ROUTE_BUILD_MSG_SET_SCOPE
   --  unsupported macro: NL_CAPABILITY_ROUTE_LINK_VETH_GET_PEER_OWN_REFERENCE NL_CAPABILITY_ROUTE_LINK_VETH_GET_PEER_OWN_REFERENCE
   --  unsupported macro: NL_CAPABILITY_ROUTE_LINK_CLS_ADD_ACT_OWN_REFERENCE NL_CAPABILITY_ROUTE_LINK_CLS_ADD_ACT_OWN_REFERENCE
   --  unsupported macro: NL_CAPABILITY_NL_CONNECT_RETRY_GENERATE_PORT_ON_ADDRINUSE NL_CAPABILITY_NL_CONNECT_RETRY_GENERATE_PORT_ON_ADDRINUSE
   --  unsupported macro: NL_CAPABILITY_ROUTE_LINK_GET_KERNEL_FAIL_OPNOTSUPP NL_CAPABILITY_ROUTE_LINK_GET_KERNEL_FAIL_OPNOTSUPP
   --  unsupported macro: NL_CAPABILITY_ROUTE_ADDR_COMPARE_CACHEINFO NL_CAPABILITY_ROUTE_ADDR_COMPARE_CACHEINFO
   --  unsupported macro: NL_CAPABILITY_VERSION_3_2_26 NL_CAPABILITY_VERSION_3_2_26
   --  unsupported macro: NL_CAPABILITY_NL_RECV_FAIL_TRUNC_NO_PEEK NL_CAPABILITY_NL_RECV_FAIL_TRUNC_NO_PEEK
   --  unsupported macro: NL_CAPABILITY_LINK_BUILD_CHANGE_REQUEST_SET_CHANGE NL_CAPABILITY_LINK_BUILD_CHANGE_REQUEST_SET_CHANGE
   --  unsupported macro: NL_CAPABILITY_RTNL_NEIGH_GET_FILTER_AF_UNSPEC_FIX NL_CAPABILITY_RTNL_NEIGH_GET_FILTER_AF_UNSPEC_FIX
   --  unsupported macro: NL_CAPABILITY_VERSION_3_2_27 NL_CAPABILITY_VERSION_3_2_27
   --  unsupported macro: NL_CAPABILITY_RTNL_LINK_VLAN_PROTOCOL_SERIALZE NL_CAPABILITY_RTNL_LINK_VLAN_PROTOCOL_SERIALZE
   --  unsupported macro: NL_CAPABILITY_RTNL_LINK_PARSE_GRE_REMOTE NL_CAPABILITY_RTNL_LINK_PARSE_GRE_REMOTE
   --  unsupported macro: NL_CAPABILITY_RTNL_LINK_VLAN_INGRESS_MAP_CLEAR NL_CAPABILITY_RTNL_LINK_VLAN_INGRESS_MAP_CLEAR
   --  unsupported macro: NL_CAPABILITY_RTNL_LINK_VXLAN_IO_COMPARE NL_CAPABILITY_RTNL_LINK_VXLAN_IO_COMPARE
   --  unsupported macro: NL_CAPABILITY_NL_OBJECT_DIFF64 NL_CAPABILITY_NL_OBJECT_DIFF64
   --  unsupported macro: NL_CAPABILITY_XFRM_SA_KEY_SIZE NL_CAPABILITY_XFRM_SA_KEY_SIZE
   --  unsupported macro: NL_CAPABILITY_RTNL_ADDR_PEER_FIX NL_CAPABILITY_RTNL_ADDR_PEER_FIX
   --  unsupported macro: NL_CAPABILITY_VERSION_3_2_28 NL_CAPABILITY_VERSION_3_2_28
   --  unsupported macro: NL_CAPABILITY_RTNL_ADDR_PEER_ID_FIX NL_CAPABILITY_RTNL_ADDR_PEER_ID_FIX
   --  unsupported macro: NL_CAPABILITY_NL_ADDR_FILL_SOCKADDR NL_CAPABILITY_NL_ADDR_FILL_SOCKADDR
   --  unsupported macro: NL_CAPABILITY_XFRM_SEC_CTX_LEN NL_CAPABILITY_XFRM_SEC_CTX_LEN
   --  unsupported macro: NL_CAPABILITY_LINK_BUILD_ADD_REQUEST_SET_CHANGE NL_CAPABILITY_LINK_BUILD_ADD_REQUEST_SET_CHANGE
   --  unsupported macro: NL_CAPABILITY_NL_RECVMSGS_PEEK_BY_DEFAULT NL_CAPABILITY_NL_RECVMSGS_PEEK_BY_DEFAULT
   --  unsupported macro: NL_CAPABILITY_VERSION_3_2_29 NL_CAPABILITY_VERSION_3_2_29
   --  unsupported macro: NL_CAPABILITY_XFRM_SP_SEC_CTX_LEN NL_CAPABILITY_XFRM_SP_SEC_CTX_LEN
   --  unsupported macro: NL_CAPABILITY_VERSION_3_3_0 NL_CAPABILITY_VERSION_3_3_0
   --  unsupported macro: NL_CAPABILITY_VERSION_3_4_0 NL_CAPABILITY_VERSION_3_4_0
   --  unsupported macro: NL_CAPABILITY_ROUTE_FIX_VLAN_SET_EGRESS_MAP NL_CAPABILITY_ROUTE_FIX_VLAN_SET_EGRESS_MAP
   --  unsupported macro: NL_CAPABILITY_VERSION_3_5_0 NL_CAPABILITY_VERSION_3_5_0
   --  unsupported macro: NL_CAPABILITY_NL_OBJECT_IDENTICAL_PARTIAL NL_CAPABILITY_NL_OBJECT_IDENTICAL_PARTIAL
   --  unsupported macro: NL_CAPABILITY_MAX NL_CAPABILITY_MAX
   --  arg-macro: function NL_CAPABILITY_IS_USER_RESERVED (cap)
   --    return  ((cap) and ~16#0FFF#) = 16#7000# ;

   function nl_cancel_down_bytes (arg1 : Extensions.unsigned_long_long; arg2 : System.Address) return double;  -- ../include/netlink/utils.h:41
   pragma Import (C, nl_cancel_down_bytes, "nl_cancel_down_bytes");

   function nl_cancel_down_bits (arg1 : Extensions.unsigned_long_long; arg2 : System.Address) return double;  -- ../include/netlink/utils.h:42
   pragma Import (C, nl_cancel_down_bits, "nl_cancel_down_bits");

   function nl_rate2str
     (arg1 : Extensions.unsigned_long_long;
      arg2 : int;
      arg3 : Interfaces.C.Strings.chars_ptr;
      arg4 : stddef_h.size_t) return int;  -- ../include/netlink/utils.h:43
   pragma Import (C, nl_rate2str, "nl_rate2str");

   function nl_cancel_down_us (arg1 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t; arg2 : System.Address) return double;  -- ../include/netlink/utils.h:44
   pragma Import (C, nl_cancel_down_us, "nl_cancel_down_us");

   function nl_size2int (arg1 : Interfaces.C.Strings.chars_ptr) return long;  -- ../include/netlink/utils.h:47
   pragma Import (C, nl_size2int, "nl_size2int");

   function nl_size2str
     (arg1 : stddef_h.size_t;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/utils.h:48
   pragma Import (C, nl_size2str, "nl_size2str");

   function nl_prob2int (arg1 : Interfaces.C.Strings.chars_ptr) return long;  -- ../include/netlink/utils.h:49
   pragma Import (C, nl_prob2int, "nl_prob2int");

   function nl_get_user_hz return int;  -- ../include/netlink/utils.h:52
   pragma Import (C, nl_get_user_hz, "nl_get_user_hz");

   function nl_get_psched_hz return int;  -- ../include/netlink/utils.h:53
   pragma Import (C, nl_get_psched_hz, "nl_get_psched_hz");

   function nl_us2ticks (arg1 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/utils.h:54
   pragma Import (C, nl_us2ticks, "nl_us2ticks");

   function nl_ticks2us (arg1 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/utils.h:55
   pragma Import (C, nl_ticks2us, "nl_ticks2us");

   function nl_str2msec (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : access x86_64_linux_gnu_bits_stdint_uintn_h.uint64_t) return int;  -- ../include/netlink/utils.h:56
   pragma Import (C, nl_str2msec, "nl_str2msec");

   function nl_msec2str
     (arg1 : x86_64_linux_gnu_bits_stdint_uintn_h.uint64_t;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/utils.h:57
   pragma Import (C, nl_msec2str, "nl_msec2str");

   function nl_llproto2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/utils.h:60
   pragma Import (C, nl_llproto2str, "nl_llproto2str");

   function nl_str2llproto (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/utils.h:61
   pragma Import (C, nl_str2llproto, "nl_str2llproto");

   function nl_ether_proto2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/utils.h:64
   pragma Import (C, nl_ether_proto2str, "nl_ether_proto2str");

   function nl_str2ether_proto (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/utils.h:65
   pragma Import (C, nl_str2ether_proto, "nl_str2ether_proto");

   function nl_ip_proto2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/utils.h:68
   pragma Import (C, nl_ip_proto2str, "nl_ip_proto2str");

   function nl_str2ip_proto (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/utils.h:69
   pragma Import (C, nl_str2ip_proto, "nl_str2ip_proto");

   procedure nl_new_line (arg1 : access netlink_types_h.nl_dump_params);  -- ../include/netlink/utils.h:72
   pragma Import (C, nl_new_line, "nl_new_line");

   procedure nl_dump (arg1 : access netlink_types_h.nl_dump_params; arg2 : Interfaces.C.Strings.chars_ptr  -- , ...
      );  -- ../include/netlink/utils.h:73
   pragma Import (C, nl_dump, "nl_dump");

   procedure nl_dump_line (arg1 : access netlink_types_h.nl_dump_params; arg2 : Interfaces.C.Strings.chars_ptr  -- , ...
      );  -- ../include/netlink/utils.h:74
   pragma Import (C, nl_dump_line, "nl_dump_line");

   function nl_has_capability (capability : int) return int;  -- ../include/netlink/utils.h:314
   pragma Import (C, nl_has_capability, "nl_has_capability");

end netlink_utils_h;
