pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
limited with netlink_handlers_h;
with System;
limited with netlink_object_h;
limited with netlink_addr_h;
with Interfaces.C.Strings;
with nlada_stddef_h;
limited with netlink_route_link_h;
with x86_64_linux_gnu_bits_stdint_uintn_h;

package netlink_route_addr_h is

   package stddef_h renames nlada_stddef_h;

   type rtnl_addr is null record;   -- incomplete struct

   function rtnl_addr_alloc return access rtnl_addr;  -- ../include/netlink/route/addr.h:23
   pragma Import (C, rtnl_addr_alloc, "rtnl_addr_alloc");

   procedure rtnl_addr_put (arg1 : access rtnl_addr);  -- ../include/netlink/route/addr.h:24
   pragma Import (C, rtnl_addr_put, "rtnl_addr_put");

   function rtnl_addr_alloc_cache (arg1 : access netlink_handlers_h.nl_sock; arg2 : System.Address) return int;  -- ../include/netlink/route/addr.h:26
   pragma Import (C, rtnl_addr_alloc_cache, "rtnl_addr_alloc_cache");

   function rtnl_addr_get
     (arg1 : access netlink_object_h.nl_cache;
      arg2 : int;
      arg3 : access netlink_addr_h.nl_addr) return access rtnl_addr;  -- ../include/netlink/route/addr.h:28
   pragma Import (C, rtnl_addr_get, "rtnl_addr_get");

   function rtnl_addr_build_add_request
     (arg1 : access rtnl_addr;
      arg2 : int;
      arg3 : System.Address) return int;  -- ../include/netlink/route/addr.h:30
   pragma Import (C, rtnl_addr_build_add_request, "rtnl_addr_build_add_request");

   function rtnl_addr_add
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : access rtnl_addr;
      arg3 : int) return int;  -- ../include/netlink/route/addr.h:32
   pragma Import (C, rtnl_addr_add, "rtnl_addr_add");

   function rtnl_addr_build_delete_request
     (arg1 : access rtnl_addr;
      arg2 : int;
      arg3 : System.Address) return int;  -- ../include/netlink/route/addr.h:34
   pragma Import (C, rtnl_addr_build_delete_request, "rtnl_addr_build_delete_request");

   function rtnl_addr_delete
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : access rtnl_addr;
      arg3 : int) return int;  -- ../include/netlink/route/addr.h:36
   pragma Import (C, rtnl_addr_delete, "rtnl_addr_delete");

   function rtnl_addr_flags2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/addr.h:39
   pragma Import (C, rtnl_addr_flags2str, "rtnl_addr_flags2str");

   function rtnl_addr_str2flags (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/addr.h:40
   pragma Import (C, rtnl_addr_str2flags, "rtnl_addr_str2flags");

   function rtnl_addr_set_label (arg1 : access rtnl_addr; arg2 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/addr.h:42
   pragma Import (C, rtnl_addr_set_label, "rtnl_addr_set_label");

   function rtnl_addr_get_label (arg1 : access rtnl_addr) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/addr.h:43
   pragma Import (C, rtnl_addr_get_label, "rtnl_addr_get_label");

   procedure rtnl_addr_set_ifindex (arg1 : access rtnl_addr; arg2 : int);  -- ../include/netlink/route/addr.h:45
   pragma Import (C, rtnl_addr_set_ifindex, "rtnl_addr_set_ifindex");

   function rtnl_addr_get_ifindex (arg1 : access rtnl_addr) return int;  -- ../include/netlink/route/addr.h:46
   pragma Import (C, rtnl_addr_get_ifindex, "rtnl_addr_get_ifindex");

   procedure rtnl_addr_set_link (arg1 : access rtnl_addr; arg2 : access netlink_route_link_h.rtnl_link);  -- ../include/netlink/route/addr.h:48
   pragma Import (C, rtnl_addr_set_link, "rtnl_addr_set_link");

   function rtnl_addr_get_link (arg1 : access rtnl_addr) return access netlink_route_link_h.rtnl_link;  -- ../include/netlink/route/addr.h:50
   pragma Import (C, rtnl_addr_get_link, "rtnl_addr_get_link");

   procedure rtnl_addr_set_family (arg1 : access rtnl_addr; arg2 : int);  -- ../include/netlink/route/addr.h:52
   pragma Import (C, rtnl_addr_set_family, "rtnl_addr_set_family");

   function rtnl_addr_get_family (arg1 : access rtnl_addr) return int;  -- ../include/netlink/route/addr.h:53
   pragma Import (C, rtnl_addr_get_family, "rtnl_addr_get_family");

   procedure rtnl_addr_set_prefixlen (arg1 : access rtnl_addr; arg2 : int);  -- ../include/netlink/route/addr.h:55
   pragma Import (C, rtnl_addr_set_prefixlen, "rtnl_addr_set_prefixlen");

   function rtnl_addr_get_prefixlen (arg1 : access rtnl_addr) return int;  -- ../include/netlink/route/addr.h:56
   pragma Import (C, rtnl_addr_get_prefixlen, "rtnl_addr_get_prefixlen");

   procedure rtnl_addr_set_scope (arg1 : access rtnl_addr; arg2 : int);  -- ../include/netlink/route/addr.h:58
   pragma Import (C, rtnl_addr_set_scope, "rtnl_addr_set_scope");

   function rtnl_addr_get_scope (arg1 : access rtnl_addr) return int;  -- ../include/netlink/route/addr.h:59
   pragma Import (C, rtnl_addr_get_scope, "rtnl_addr_get_scope");

   procedure rtnl_addr_set_flags (arg1 : access rtnl_addr; arg2 : unsigned);  -- ../include/netlink/route/addr.h:61
   pragma Import (C, rtnl_addr_set_flags, "rtnl_addr_set_flags");

   procedure rtnl_addr_unset_flags (arg1 : access rtnl_addr; arg2 : unsigned);  -- ../include/netlink/route/addr.h:62
   pragma Import (C, rtnl_addr_unset_flags, "rtnl_addr_unset_flags");

   function rtnl_addr_get_flags (arg1 : access rtnl_addr) return unsigned;  -- ../include/netlink/route/addr.h:63
   pragma Import (C, rtnl_addr_get_flags, "rtnl_addr_get_flags");

   function rtnl_addr_set_local (arg1 : access rtnl_addr; arg2 : access netlink_addr_h.nl_addr) return int;  -- ../include/netlink/route/addr.h:65
   pragma Import (C, rtnl_addr_set_local, "rtnl_addr_set_local");

   function rtnl_addr_get_local (arg1 : access rtnl_addr) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/addr.h:67
   pragma Import (C, rtnl_addr_get_local, "rtnl_addr_get_local");

   function rtnl_addr_set_peer (arg1 : access rtnl_addr; arg2 : access netlink_addr_h.nl_addr) return int;  -- ../include/netlink/route/addr.h:69
   pragma Import (C, rtnl_addr_set_peer, "rtnl_addr_set_peer");

   function rtnl_addr_get_peer (arg1 : access rtnl_addr) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/addr.h:70
   pragma Import (C, rtnl_addr_get_peer, "rtnl_addr_get_peer");

   function rtnl_addr_set_broadcast (arg1 : access rtnl_addr; arg2 : access netlink_addr_h.nl_addr) return int;  -- ../include/netlink/route/addr.h:72
   pragma Import (C, rtnl_addr_set_broadcast, "rtnl_addr_set_broadcast");

   function rtnl_addr_get_broadcast (arg1 : access rtnl_addr) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/addr.h:73
   pragma Import (C, rtnl_addr_get_broadcast, "rtnl_addr_get_broadcast");

   function rtnl_addr_set_multicast (arg1 : access rtnl_addr; arg2 : access netlink_addr_h.nl_addr) return int;  -- ../include/netlink/route/addr.h:75
   pragma Import (C, rtnl_addr_set_multicast, "rtnl_addr_set_multicast");

   function rtnl_addr_get_multicast (arg1 : access rtnl_addr) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/addr.h:76
   pragma Import (C, rtnl_addr_get_multicast, "rtnl_addr_get_multicast");

   function rtnl_addr_set_anycast (arg1 : access rtnl_addr; arg2 : access netlink_addr_h.nl_addr) return int;  -- ../include/netlink/route/addr.h:78
   pragma Import (C, rtnl_addr_set_anycast, "rtnl_addr_set_anycast");

   function rtnl_addr_get_anycast (arg1 : access rtnl_addr) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/addr.h:79
   pragma Import (C, rtnl_addr_get_anycast, "rtnl_addr_get_anycast");

   function rtnl_addr_get_valid_lifetime (arg1 : access rtnl_addr) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/addr.h:81
   pragma Import (C, rtnl_addr_get_valid_lifetime, "rtnl_addr_get_valid_lifetime");

   procedure rtnl_addr_set_valid_lifetime (arg1 : access rtnl_addr; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/route/addr.h:82
   pragma Import (C, rtnl_addr_set_valid_lifetime, "rtnl_addr_set_valid_lifetime");

   function rtnl_addr_get_preferred_lifetime (arg1 : access rtnl_addr) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/addr.h:83
   pragma Import (C, rtnl_addr_get_preferred_lifetime, "rtnl_addr_get_preferred_lifetime");

   procedure rtnl_addr_set_preferred_lifetime (arg1 : access rtnl_addr; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/route/addr.h:84
   pragma Import (C, rtnl_addr_set_preferred_lifetime, "rtnl_addr_set_preferred_lifetime");

   function rtnl_addr_get_create_time (arg1 : access rtnl_addr) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/addr.h:85
   pragma Import (C, rtnl_addr_get_create_time, "rtnl_addr_get_create_time");

   function rtnl_addr_get_last_update_time (arg1 : access rtnl_addr) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/addr.h:86
   pragma Import (C, rtnl_addr_get_last_update_time, "rtnl_addr_get_last_update_time");

end netlink_route_addr_h;
