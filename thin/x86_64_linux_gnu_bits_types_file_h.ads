pragma Style_Checks (Off);

with x86_64_linux_gnu_bits_types_struct_FILE_h;

package x86_64_linux_gnu_bits_types_FILE_h is

   subtype FILE is x86_64_linux_gnu_bits_types_struct_FILE_h.u_IO_FILE;  -- /usr/include/x86_64-linux-gnu/bits/types/FILE.h:7

end x86_64_linux_gnu_bits_types_FILE_h;
