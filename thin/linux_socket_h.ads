pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package linux_socket_h is

   subtype uu_kernel_sa_family_t is unsigned_short;  -- /usr/include/linux/socket.h:12

   subtype anon1676_uu_data_array is Interfaces.C.char_array (0 .. 125);
   type uu_kernel_sockaddr_storage is record
      ss_family : aliased uu_kernel_sa_family_t;  -- /usr/include/linux/socket.h:15
      uu_data : aliased anon1676_uu_data_array;  -- /usr/include/linux/socket.h:17
   end record;
   pragma Convention (C_Pass_By_Copy, uu_kernel_sockaddr_storage);  -- /usr/include/linux/socket.h:14

end linux_socket_h;
