pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with x86_64_linux_gnu_bits_stdint_uintn_h;
with x86_64_linux_gnu_bits_stdint_intn_h;
with netlink_object_h;
limited with netlink_handlers_h;
with System;
limited with linux_netlink_h;
limited with netlink_addr_h;
limited with netlink_route_nexthop_h;
limited with netlink_list_h;
with Interfaces.C.Strings;
with nlada_stddef_h;

package netlink_route_route_h is

   package stddef_h renames nlada_stddef_h;

   ROUTE_CACHE_CONTENT : constant := 1;  --  ../include/netlink/route/route.h:27

   type rtnl_route is null record;   -- incomplete struct

   type rtnl_rtcacheinfo is record
      rtci_clntref : aliased x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/route.h:33
      rtci_last_use : aliased x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/route.h:34
      rtci_expires : aliased x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/route.h:35
      rtci_error : aliased x86_64_linux_gnu_bits_stdint_intn_h.int32_t;  -- ../include/netlink/route/route.h:36
      rtci_used : aliased x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/route.h:37
      rtci_id : aliased x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/route.h:38
      rtci_ts : aliased x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/route.h:39
      rtci_tsage : aliased x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/route.h:40
   end record;
   pragma Convention (C_Pass_By_Copy, rtnl_rtcacheinfo);  -- ../include/netlink/route/route.h:31

   route_obj_ops : aliased netlink_object_h.nl_object_ops;  -- ../include/netlink/route/route.h:43
   pragma Import (C, route_obj_ops, "route_obj_ops");

   function rtnl_route_alloc return access rtnl_route;  -- ../include/netlink/route/route.h:45
   pragma Import (C, rtnl_route_alloc, "rtnl_route_alloc");

   procedure rtnl_route_put (arg1 : access rtnl_route);  -- ../include/netlink/route/route.h:46
   pragma Import (C, rtnl_route_put, "rtnl_route_put");

   function rtnl_route_alloc_cache
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : int;
      arg3 : int;
      arg4 : System.Address) return int;  -- ../include/netlink/route/route.h:47
   pragma Import (C, rtnl_route_alloc_cache, "rtnl_route_alloc_cache");

   procedure rtnl_route_get (arg1 : access rtnl_route);  -- ../include/netlink/route/route.h:50
   pragma Import (C, rtnl_route_get, "rtnl_route_get");

   function rtnl_route_parse (arg1 : access linux_netlink_h.nlmsghdr; arg2 : System.Address) return int;  -- ../include/netlink/route/route.h:52
   pragma Import (C, rtnl_route_parse, "rtnl_route_parse");

   function rtnl_route_build_msg (arg1 : access netlink_handlers_h.nl_msg; arg2 : access rtnl_route) return int;  -- ../include/netlink/route/route.h:53
   pragma Import (C, rtnl_route_build_msg, "rtnl_route_build_msg");

   function rtnl_route_build_add_request
     (arg1 : access rtnl_route;
      arg2 : int;
      arg3 : System.Address) return int;  -- ../include/netlink/route/route.h:55
   pragma Import (C, rtnl_route_build_add_request, "rtnl_route_build_add_request");

   function rtnl_route_add
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : access rtnl_route;
      arg3 : int) return int;  -- ../include/netlink/route/route.h:57
   pragma Import (C, rtnl_route_add, "rtnl_route_add");

   function rtnl_route_build_del_request
     (arg1 : access rtnl_route;
      arg2 : int;
      arg3 : System.Address) return int;  -- ../include/netlink/route/route.h:58
   pragma Import (C, rtnl_route_build_del_request, "rtnl_route_build_del_request");

   function rtnl_route_delete
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : access rtnl_route;
      arg3 : int) return int;  -- ../include/netlink/route/route.h:60
   pragma Import (C, rtnl_route_delete, "rtnl_route_delete");

   procedure rtnl_route_set_table (arg1 : access rtnl_route; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/route/route.h:62
   pragma Import (C, rtnl_route_set_table, "rtnl_route_set_table");

   function rtnl_route_get_table (arg1 : access rtnl_route) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/route.h:63
   pragma Import (C, rtnl_route_get_table, "rtnl_route_get_table");

   procedure rtnl_route_set_scope (arg1 : access rtnl_route; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t);  -- ../include/netlink/route/route.h:64
   pragma Import (C, rtnl_route_set_scope, "rtnl_route_set_scope");

   function rtnl_route_get_scope (arg1 : access rtnl_route) return x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;  -- ../include/netlink/route/route.h:65
   pragma Import (C, rtnl_route_get_scope, "rtnl_route_get_scope");

   procedure rtnl_route_set_tos (arg1 : access rtnl_route; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t);  -- ../include/netlink/route/route.h:66
   pragma Import (C, rtnl_route_set_tos, "rtnl_route_set_tos");

   function rtnl_route_get_tos (arg1 : access rtnl_route) return x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;  -- ../include/netlink/route/route.h:67
   pragma Import (C, rtnl_route_get_tos, "rtnl_route_get_tos");

   procedure rtnl_route_set_protocol (arg1 : access rtnl_route; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t);  -- ../include/netlink/route/route.h:68
   pragma Import (C, rtnl_route_set_protocol, "rtnl_route_set_protocol");

   function rtnl_route_get_protocol (arg1 : access rtnl_route) return x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;  -- ../include/netlink/route/route.h:69
   pragma Import (C, rtnl_route_get_protocol, "rtnl_route_get_protocol");

   procedure rtnl_route_set_priority (arg1 : access rtnl_route; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/route/route.h:70
   pragma Import (C, rtnl_route_set_priority, "rtnl_route_set_priority");

   function rtnl_route_get_priority (arg1 : access rtnl_route) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/route.h:71
   pragma Import (C, rtnl_route_get_priority, "rtnl_route_get_priority");

   function rtnl_route_set_family (arg1 : access rtnl_route; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t) return int;  -- ../include/netlink/route/route.h:72
   pragma Import (C, rtnl_route_set_family, "rtnl_route_set_family");

   function rtnl_route_get_family (arg1 : access rtnl_route) return x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;  -- ../include/netlink/route/route.h:73
   pragma Import (C, rtnl_route_get_family, "rtnl_route_get_family");

   function rtnl_route_set_type (arg1 : access rtnl_route; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t) return int;  -- ../include/netlink/route/route.h:74
   pragma Import (C, rtnl_route_set_type, "rtnl_route_set_type");

   function rtnl_route_get_type (arg1 : access rtnl_route) return x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;  -- ../include/netlink/route/route.h:75
   pragma Import (C, rtnl_route_get_type, "rtnl_route_get_type");

   procedure rtnl_route_set_flags (arg1 : access rtnl_route; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/route/route.h:76
   pragma Import (C, rtnl_route_set_flags, "rtnl_route_set_flags");

   procedure rtnl_route_unset_flags (arg1 : access rtnl_route; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/route/route.h:77
   pragma Import (C, rtnl_route_unset_flags, "rtnl_route_unset_flags");

   function rtnl_route_get_flags (arg1 : access rtnl_route) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/route.h:78
   pragma Import (C, rtnl_route_get_flags, "rtnl_route_get_flags");

   function rtnl_route_set_metric
     (arg1 : access rtnl_route;
      arg2 : int;
      arg3 : unsigned) return int;  -- ../include/netlink/route/route.h:79
   pragma Import (C, rtnl_route_set_metric, "rtnl_route_set_metric");

   function rtnl_route_unset_metric (arg1 : access rtnl_route; arg2 : int) return int;  -- ../include/netlink/route/route.h:80
   pragma Import (C, rtnl_route_unset_metric, "rtnl_route_unset_metric");

   function rtnl_route_get_metric
     (arg1 : access rtnl_route;
      arg2 : int;
      arg3 : access x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t) return int;  -- ../include/netlink/route/route.h:81
   pragma Import (C, rtnl_route_get_metric, "rtnl_route_get_metric");

   function rtnl_route_set_dst (arg1 : access rtnl_route; arg2 : access netlink_addr_h.nl_addr) return int;  -- ../include/netlink/route/route.h:82
   pragma Import (C, rtnl_route_set_dst, "rtnl_route_set_dst");

   function rtnl_route_get_dst (arg1 : access rtnl_route) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/route.h:83
   pragma Import (C, rtnl_route_get_dst, "rtnl_route_get_dst");

   function rtnl_route_set_src (arg1 : access rtnl_route; arg2 : access netlink_addr_h.nl_addr) return int;  -- ../include/netlink/route/route.h:84
   pragma Import (C, rtnl_route_set_src, "rtnl_route_set_src");

   function rtnl_route_get_src (arg1 : access rtnl_route) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/route.h:85
   pragma Import (C, rtnl_route_get_src, "rtnl_route_get_src");

   function rtnl_route_set_pref_src (arg1 : access rtnl_route; arg2 : access netlink_addr_h.nl_addr) return int;  -- ../include/netlink/route/route.h:86
   pragma Import (C, rtnl_route_set_pref_src, "rtnl_route_set_pref_src");

   function rtnl_route_get_pref_src (arg1 : access rtnl_route) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/route.h:87
   pragma Import (C, rtnl_route_get_pref_src, "rtnl_route_get_pref_src");

   procedure rtnl_route_set_iif (arg1 : access rtnl_route; arg2 : int);  -- ../include/netlink/route/route.h:88
   pragma Import (C, rtnl_route_set_iif, "rtnl_route_set_iif");

   function rtnl_route_get_iif (arg1 : access rtnl_route) return int;  -- ../include/netlink/route/route.h:89
   pragma Import (C, rtnl_route_get_iif, "rtnl_route_get_iif");

   function rtnl_route_get_src_len (arg1 : access rtnl_route) return int;  -- ../include/netlink/route/route.h:90
   pragma Import (C, rtnl_route_get_src_len, "rtnl_route_get_src_len");

   procedure rtnl_route_set_ttl_propagate (route : access rtnl_route; ttl_prop : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t);  -- ../include/netlink/route/route.h:91
   pragma Import (C, rtnl_route_set_ttl_propagate, "rtnl_route_set_ttl_propagate");

   function rtnl_route_get_ttl_propagate (route : access rtnl_route) return int;  -- ../include/netlink/route/route.h:93
   pragma Import (C, rtnl_route_get_ttl_propagate, "rtnl_route_get_ttl_propagate");

   procedure rtnl_route_add_nexthop (arg1 : access rtnl_route; arg2 : access netlink_route_nexthop_h.rtnl_nexthop);  -- ../include/netlink/route/route.h:95
   pragma Import (C, rtnl_route_add_nexthop, "rtnl_route_add_nexthop");

   procedure rtnl_route_remove_nexthop (arg1 : access rtnl_route; arg2 : access netlink_route_nexthop_h.rtnl_nexthop);  -- ../include/netlink/route/route.h:97
   pragma Import (C, rtnl_route_remove_nexthop, "rtnl_route_remove_nexthop");

   function rtnl_route_get_nexthops (arg1 : access rtnl_route) return access netlink_list_h.nl_list_head;  -- ../include/netlink/route/route.h:99
   pragma Import (C, rtnl_route_get_nexthops, "rtnl_route_get_nexthops");

   function rtnl_route_get_nnexthops (arg1 : access rtnl_route) return int;  -- ../include/netlink/route/route.h:100
   pragma Import (C, rtnl_route_get_nnexthops, "rtnl_route_get_nnexthops");

   procedure rtnl_route_foreach_nexthop
     (r : access rtnl_route;
      cb : access procedure (arg1 : access netlink_route_nexthop_h.rtnl_nexthop; arg2 : System.Address);
      arg : System.Address);  -- ../include/netlink/route/route.h:102
   pragma Import (C, rtnl_route_foreach_nexthop, "rtnl_route_foreach_nexthop");

   function rtnl_route_nexthop_n (arg1 : access rtnl_route; arg2 : int) return access netlink_route_nexthop_h.rtnl_nexthop;  -- ../include/netlink/route/route.h:106
   pragma Import (C, rtnl_route_nexthop_n, "rtnl_route_nexthop_n");

   function rtnl_route_guess_scope (arg1 : access rtnl_route) return int;  -- ../include/netlink/route/route.h:108
   pragma Import (C, rtnl_route_guess_scope, "rtnl_route_guess_scope");

   function rtnl_route_table2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/route.h:110
   pragma Import (C, rtnl_route_table2str, "rtnl_route_table2str");

   function rtnl_route_str2table (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/route.h:111
   pragma Import (C, rtnl_route_str2table, "rtnl_route_str2table");

   function rtnl_route_read_table_names (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/route.h:112
   pragma Import (C, rtnl_route_read_table_names, "rtnl_route_read_table_names");

   function rtnl_route_proto2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/route.h:114
   pragma Import (C, rtnl_route_proto2str, "rtnl_route_proto2str");

   function rtnl_route_str2proto (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/route.h:115
   pragma Import (C, rtnl_route_str2proto, "rtnl_route_str2proto");

   function rtnl_route_read_protocol_names (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/route.h:116
   pragma Import (C, rtnl_route_read_protocol_names, "rtnl_route_read_protocol_names");

   function rtnl_route_metric2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/route.h:118
   pragma Import (C, rtnl_route_metric2str, "rtnl_route_metric2str");

   function rtnl_route_str2metric (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/route.h:119
   pragma Import (C, rtnl_route_str2metric, "rtnl_route_str2metric");

end netlink_route_route_h;
