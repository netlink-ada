pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with x86_64_linux_gnu_bits_stdint_uintn_h;
with System;
limited with linux_netlink_h;
with Interfaces.C.Strings;
with nlada_stddef_h;
limited with netlink_handlers_h;
limited with netlink_addr_h;
with x86_64_linux_gnu_bits_stdint_intn_h;
limited with netlink_data_h;

package netlink_attr_h is

   package stddef_h renames nlada_stddef_h;

   type nlattr is null record;   -- incomplete struct

   type nl_msg is null record;   -- incomplete struct

   --  unsupported macro: NLA_TYPE_MAX (__NLA_TYPE_MAX - 1)
   --  arg-macro: procedure NLA_PUT (msg, attrtype, attrlen, data)
   --    do { if (nla_put(msg, attrtype, attrlen, data) < 0) goto nla_put_failure; } while(0)
   --  arg-macro: procedure NLA_PUT_TYPE (msg, type, attrtype, value)
   --    do { type __tmp := value; NLA_PUT(msg, attrtype, sizeof(type), and__tmp); } while(0)
   --  arg-macro: procedure NLA_PUT_S8 (msg, attrtype, value)
   --    NLA_PUT_TYPE(msg, int8_t, attrtype, value)
   --  arg-macro: procedure NLA_PUT_U8 (msg, attrtype, value)
   --    NLA_PUT_TYPE(msg, uint8_t, attrtype, value)
   --  arg-macro: procedure NLA_PUT_S16 (msg, attrtype, value)
   --    NLA_PUT_TYPE(msg, int16_t, attrtype, value)
   --  arg-macro: procedure NLA_PUT_U16 (msg, attrtype, value)
   --    NLA_PUT_TYPE(msg, uint16_t, attrtype, value)
   --  arg-macro: procedure NLA_PUT_S32 (msg, attrtype, value)
   --    NLA_PUT_TYPE(msg, int32_t, attrtype, value)
   --  arg-macro: procedure NLA_PUT_U32 (msg, attrtype, value)
   --    NLA_PUT_TYPE(msg, uint32_t, attrtype, value)
   --  arg-macro: procedure NLA_PUT_S64 (msg, attrtype, value)
   --    NLA_PUT_TYPE(msg, int64_t, attrtype, value)
   --  arg-macro: procedure NLA_PUT_U64 (msg, attrtype, value)
   --    NLA_PUT_TYPE(msg, uint64_t, attrtype, value)
   --  arg-macro: procedure NLA_PUT_STRING (msg, attrtype, value)
   --    NLA_PUT(msg, attrtype, (int) strlen(value) + 1, value)
   --  arg-macro: procedure NLA_PUT_FLAG (msg, attrtype)
   --    NLA_PUT(msg, attrtype, 0, NULL)
   --  arg-macro: procedure NLA_PUT_MSECS (msg, attrtype, msecs)
   --    NLA_PUT_U64(msg, attrtype, msecs)
   --  arg-macro: procedure NLA_PUT_ADDR (msg, attrtype, addr)
   --    NLA_PUT(msg, attrtype, nl_addr_get_len(addr), nl_addr_get_binary_addr(addr))
   --  arg-macro: procedure NLA_PUT_DATA (msg, attrtype, data)
   --    NLA_PUT(msg, attrtype, nl_data_get_size(data), nl_data_get(data))
   --  arg-macro: procedure nla_for_each_attr (pos, head, len, rem)
   --    for (pos := head, rem := len; nla_ok(pos, rem); pos := nla_next(pos, and(rem)))
   --  arg-macro: procedure nla_for_each_nested (pos, nla, rem)
   --    for (pos := (struct nlattr *) nla_data(nla), rem := nla_len(nla); nla_ok(pos, rem); pos := nla_next(pos, and(rem)))
   type nla_policy is record
      c_type : aliased x86_64_linux_gnu_bits_stdint_uintn_h.uint16_t;  -- ../include/netlink/attr.h:65
      minlen : aliased x86_64_linux_gnu_bits_stdint_uintn_h.uint16_t;  -- ../include/netlink/attr.h:68
      maxlen : aliased x86_64_linux_gnu_bits_stdint_uintn_h.uint16_t;  -- ../include/netlink/attr.h:71
   end record;
   pragma Convention (C_Pass_By_Copy, nla_policy);  -- ../include/netlink/attr.h:63

   function nla_attr_size (payload : int) return int;  -- ../include/netlink/attr.h:75
   pragma Import (C, nla_attr_size, "nla_attr_size");

   function nla_total_size (payload : int) return int;  -- ../include/netlink/attr.h:76
   pragma Import (C, nla_total_size, "nla_total_size");

   function nla_padlen (payload : int) return int;  -- ../include/netlink/attr.h:77
   pragma Import (C, nla_padlen, "nla_padlen");

   function nla_type (arg1 : access constant nlattr) return int;  -- ../include/netlink/attr.h:80
   pragma Import (C, nla_type, "nla_type");

   function nla_data (arg1 : access constant nlattr) return System.Address;  -- ../include/netlink/attr.h:81
   pragma Import (C, nla_data, "nla_data");

   function nla_len (arg1 : access constant nlattr) return int;  -- ../include/netlink/attr.h:82
   pragma Import (C, nla_len, "nla_len");

   function nla_ok (arg1 : access constant nlattr; arg2 : int) return int;  -- ../include/netlink/attr.h:83
   pragma Import (C, nla_ok, "nla_ok");

   function nla_next (arg1 : access constant nlattr; arg2 : access int) return access linux_netlink_h.nlattr;  -- ../include/netlink/attr.h:84
   pragma Import (C, nla_next, "nla_next");

   function nla_parse
     (arg1 : System.Address;
      arg2 : int;
      arg3 : access linux_netlink_h.nlattr;
      arg4 : int;
      arg5 : access constant nla_policy) return int;  -- ../include/netlink/attr.h:85
   pragma Import (C, nla_parse, "nla_parse");

   function nla_validate
     (arg1 : access constant nlattr;
      arg2 : int;
      arg3 : int;
      arg4 : access constant nla_policy) return int;  -- ../include/netlink/attr.h:87
   pragma Import (C, nla_validate, "nla_validate");

   function nla_find
     (arg1 : access constant nlattr;
      arg2 : int;
      arg3 : int) return access linux_netlink_h.nlattr;  -- ../include/netlink/attr.h:89
   pragma Import (C, nla_find, "nla_find");

   function nla_memcpy
     (arg1 : System.Address;
      arg2 : access constant nlattr;
      arg3 : int) return int;  -- ../include/netlink/attr.h:92
   pragma Import (C, nla_memcpy, "nla_memcpy");

   function nla_strlcpy
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : access constant nlattr;
      arg3 : stddef_h.size_t) return stddef_h.size_t;  -- ../include/netlink/attr.h:93
   pragma Import (C, nla_strlcpy, "nla_strlcpy");

   function nla_memcmp
     (arg1 : access constant nlattr;
      arg2 : System.Address;
      arg3 : stddef_h.size_t) return int;  -- ../include/netlink/attr.h:94
   pragma Import (C, nla_memcmp, "nla_memcmp");

   function nla_strcmp (arg1 : access constant nlattr; arg2 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/attr.h:95
   pragma Import (C, nla_strcmp, "nla_strcmp");

   function nla_reserve
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : int) return access linux_netlink_h.nlattr;  -- ../include/netlink/attr.h:98
   pragma Import (C, nla_reserve, "nla_reserve");

   function nla_put
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : int;
      arg4 : System.Address) return int;  -- ../include/netlink/attr.h:99
   pragma Import (C, nla_put, "nla_put");

   function nla_put_data
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : access constant netlink_data_h.nl_data) return int;  -- ../include/netlink/attr.h:100
   pragma Import (C, nla_put_data, "nla_put_data");

   function nla_put_addr
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : access netlink_addr_h.nl_addr) return int;  -- ../include/netlink/attr.h:102
   pragma Import (C, nla_put_addr, "nla_put_addr");

   function nla_get_s8 (arg1 : access constant nlattr) return x86_64_linux_gnu_bits_stdint_intn_h.int8_t;  -- ../include/netlink/attr.h:105
   pragma Import (C, nla_get_s8, "nla_get_s8");

   function nla_put_s8
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : x86_64_linux_gnu_bits_stdint_intn_h.int8_t) return int;  -- ../include/netlink/attr.h:106
   pragma Import (C, nla_put_s8, "nla_put_s8");

   function nla_get_u8 (arg1 : access constant nlattr) return x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;  -- ../include/netlink/attr.h:107
   pragma Import (C, nla_get_u8, "nla_get_u8");

   function nla_put_u8
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t) return int;  -- ../include/netlink/attr.h:108
   pragma Import (C, nla_put_u8, "nla_put_u8");

   function nla_get_s16 (arg1 : access constant nlattr) return x86_64_linux_gnu_bits_stdint_intn_h.int16_t;  -- ../include/netlink/attr.h:109
   pragma Import (C, nla_get_s16, "nla_get_s16");

   function nla_put_s16
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : x86_64_linux_gnu_bits_stdint_intn_h.int16_t) return int;  -- ../include/netlink/attr.h:110
   pragma Import (C, nla_put_s16, "nla_put_s16");

   function nla_get_u16 (arg1 : access constant nlattr) return x86_64_linux_gnu_bits_stdint_uintn_h.uint16_t;  -- ../include/netlink/attr.h:111
   pragma Import (C, nla_get_u16, "nla_get_u16");

   function nla_put_u16
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : x86_64_linux_gnu_bits_stdint_uintn_h.uint16_t) return int;  -- ../include/netlink/attr.h:112
   pragma Import (C, nla_put_u16, "nla_put_u16");

   function nla_get_s32 (arg1 : access constant nlattr) return x86_64_linux_gnu_bits_stdint_intn_h.int32_t;  -- ../include/netlink/attr.h:113
   pragma Import (C, nla_get_s32, "nla_get_s32");

   function nla_put_s32
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : x86_64_linux_gnu_bits_stdint_intn_h.int32_t) return int;  -- ../include/netlink/attr.h:114
   pragma Import (C, nla_put_s32, "nla_put_s32");

   function nla_get_u32 (arg1 : access constant nlattr) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/attr.h:115
   pragma Import (C, nla_get_u32, "nla_get_u32");

   function nla_put_u32
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t) return int;  -- ../include/netlink/attr.h:116
   pragma Import (C, nla_put_u32, "nla_put_u32");

   function nla_get_s64 (arg1 : access constant nlattr) return x86_64_linux_gnu_bits_stdint_intn_h.int64_t;  -- ../include/netlink/attr.h:117
   pragma Import (C, nla_get_s64, "nla_get_s64");

   function nla_put_s64
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : x86_64_linux_gnu_bits_stdint_intn_h.int64_t) return int;  -- ../include/netlink/attr.h:118
   pragma Import (C, nla_put_s64, "nla_put_s64");

   function nla_get_u64 (arg1 : access constant nlattr) return x86_64_linux_gnu_bits_stdint_uintn_h.uint64_t;  -- ../include/netlink/attr.h:119
   pragma Import (C, nla_get_u64, "nla_get_u64");

   function nla_put_u64
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : x86_64_linux_gnu_bits_stdint_uintn_h.uint64_t) return int;  -- ../include/netlink/attr.h:120
   pragma Import (C, nla_put_u64, "nla_put_u64");

   function nla_get_string (arg1 : access constant nlattr) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/attr.h:123
   pragma Import (C, nla_get_string, "nla_get_string");

   function nla_strdup (arg1 : access constant nlattr) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/attr.h:124
   pragma Import (C, nla_strdup, "nla_strdup");

   function nla_put_string
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/attr.h:125
   pragma Import (C, nla_put_string, "nla_put_string");

   function nla_get_flag (arg1 : access constant nlattr) return int;  -- ../include/netlink/attr.h:128
   pragma Import (C, nla_get_flag, "nla_get_flag");

   function nla_put_flag (arg1 : access netlink_handlers_h.nl_msg; arg2 : int) return int;  -- ../include/netlink/attr.h:129
   pragma Import (C, nla_put_flag, "nla_put_flag");

   function nla_get_msecs (arg1 : access constant nlattr) return unsigned_long;  -- ../include/netlink/attr.h:132
   pragma Import (C, nla_get_msecs, "nla_get_msecs");

   function nla_put_msecs
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : unsigned_long) return int;  -- ../include/netlink/attr.h:133
   pragma Import (C, nla_put_msecs, "nla_put_msecs");

   function nla_put_nested
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : int;
      arg3 : access constant nl_msg) return int;  -- ../include/netlink/attr.h:136
   pragma Import (C, nla_put_nested, "nla_put_nested");

   function nla_nest_start (arg1 : access netlink_handlers_h.nl_msg; arg2 : int) return access linux_netlink_h.nlattr;  -- ../include/netlink/attr.h:138
   pragma Import (C, nla_nest_start, "nla_nest_start");

   function nla_nest_end (arg1 : access netlink_handlers_h.nl_msg; arg2 : access linux_netlink_h.nlattr) return int;  -- ../include/netlink/attr.h:139
   pragma Import (C, nla_nest_end, "nla_nest_end");

   function nla_nest_end_keep_empty (arg1 : access netlink_handlers_h.nl_msg; arg2 : access linux_netlink_h.nlattr) return int;  -- ../include/netlink/attr.h:140
   pragma Import (C, nla_nest_end_keep_empty, "nla_nest_end_keep_empty");

   procedure nla_nest_cancel (arg1 : access netlink_handlers_h.nl_msg; arg2 : access constant nlattr);  -- ../include/netlink/attr.h:141
   pragma Import (C, nla_nest_cancel, "nla_nest_cancel");

   function nla_parse_nested
     (arg1 : System.Address;
      arg2 : int;
      arg3 : access linux_netlink_h.nlattr;
      arg4 : access constant nla_policy) return int;  -- ../include/netlink/attr.h:142
   pragma Import (C, nla_parse_nested, "nla_parse_nested");

   function nla_is_nested (arg1 : access constant nlattr) return int;  -- ../include/netlink/attr.h:144
   pragma Import (C, nla_is_nested, "nla_is_nested");

end netlink_attr_h;
