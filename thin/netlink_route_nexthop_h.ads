pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with x86_64_linux_gnu_bits_stdint_uintn_h;
limited with netlink_types_h;
limited with netlink_addr_h;
with Interfaces.C.Strings;
with nlada_stddef_h;

package netlink_route_nexthop_h is

   package stddef_h renames nlada_stddef_h;

   type rtnl_nexthop is null record;   -- incomplete struct

   function rtnl_route_nh_alloc return access rtnl_nexthop;  -- ../include/netlink/route/nexthop.h:25
   pragma Import (C, rtnl_route_nh_alloc, "rtnl_route_nh_alloc");

   function rtnl_route_nh_clone (arg1 : access rtnl_nexthop) return access rtnl_nexthop;  -- ../include/netlink/route/nexthop.h:26
   pragma Import (C, rtnl_route_nh_clone, "rtnl_route_nh_clone");

   procedure rtnl_route_nh_free (arg1 : access rtnl_nexthop);  -- ../include/netlink/route/nexthop.h:27
   pragma Import (C, rtnl_route_nh_free, "rtnl_route_nh_free");

   function rtnl_route_nh_compare
     (arg1 : access rtnl_nexthop;
      arg2 : access rtnl_nexthop;
      arg3 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;
      arg4 : int) return int;  -- ../include/netlink/route/nexthop.h:29
   pragma Import (C, rtnl_route_nh_compare, "rtnl_route_nh_compare");

   procedure rtnl_route_nh_dump (arg1 : access rtnl_nexthop; arg2 : access netlink_types_h.nl_dump_params);  -- ../include/netlink/route/nexthop.h:33
   pragma Import (C, rtnl_route_nh_dump, "rtnl_route_nh_dump");

   procedure rtnl_route_nh_set_weight (arg1 : access rtnl_nexthop; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t);  -- ../include/netlink/route/nexthop.h:36
   pragma Import (C, rtnl_route_nh_set_weight, "rtnl_route_nh_set_weight");

   function rtnl_route_nh_get_weight (arg1 : access rtnl_nexthop) return x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;  -- ../include/netlink/route/nexthop.h:37
   pragma Import (C, rtnl_route_nh_get_weight, "rtnl_route_nh_get_weight");

   procedure rtnl_route_nh_set_ifindex (arg1 : access rtnl_nexthop; arg2 : int);  -- ../include/netlink/route/nexthop.h:38
   pragma Import (C, rtnl_route_nh_set_ifindex, "rtnl_route_nh_set_ifindex");

   function rtnl_route_nh_get_ifindex (arg1 : access rtnl_nexthop) return int;  -- ../include/netlink/route/nexthop.h:39
   pragma Import (C, rtnl_route_nh_get_ifindex, "rtnl_route_nh_get_ifindex");

   procedure rtnl_route_nh_set_gateway (arg1 : access rtnl_nexthop; arg2 : access netlink_addr_h.nl_addr);  -- ../include/netlink/route/nexthop.h:40
   pragma Import (C, rtnl_route_nh_set_gateway, "rtnl_route_nh_set_gateway");

   function rtnl_route_nh_get_gateway (arg1 : access rtnl_nexthop) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/nexthop.h:42
   pragma Import (C, rtnl_route_nh_get_gateway, "rtnl_route_nh_get_gateway");

   procedure rtnl_route_nh_set_flags (arg1 : access rtnl_nexthop; arg2 : unsigned);  -- ../include/netlink/route/nexthop.h:43
   pragma Import (C, rtnl_route_nh_set_flags, "rtnl_route_nh_set_flags");

   procedure rtnl_route_nh_unset_flags (arg1 : access rtnl_nexthop; arg2 : unsigned);  -- ../include/netlink/route/nexthop.h:45
   pragma Import (C, rtnl_route_nh_unset_flags, "rtnl_route_nh_unset_flags");

   function rtnl_route_nh_get_flags (arg1 : access rtnl_nexthop) return unsigned;  -- ../include/netlink/route/nexthop.h:47
   pragma Import (C, rtnl_route_nh_get_flags, "rtnl_route_nh_get_flags");

   procedure rtnl_route_nh_set_realms (arg1 : access rtnl_nexthop; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/route/nexthop.h:48
   pragma Import (C, rtnl_route_nh_set_realms, "rtnl_route_nh_set_realms");

   function rtnl_route_nh_get_realms (arg1 : access rtnl_nexthop) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/nexthop.h:50
   pragma Import (C, rtnl_route_nh_get_realms, "rtnl_route_nh_get_realms");

   function rtnl_route_nh_set_newdst (arg1 : access rtnl_nexthop; arg2 : access netlink_addr_h.nl_addr) return int;  -- ../include/netlink/route/nexthop.h:52
   pragma Import (C, rtnl_route_nh_set_newdst, "rtnl_route_nh_set_newdst");

   function rtnl_route_nh_get_newdst (arg1 : access rtnl_nexthop) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/nexthop.h:54
   pragma Import (C, rtnl_route_nh_get_newdst, "rtnl_route_nh_get_newdst");

   function rtnl_route_nh_set_via (arg1 : access rtnl_nexthop; arg2 : access netlink_addr_h.nl_addr) return int;  -- ../include/netlink/route/nexthop.h:55
   pragma Import (C, rtnl_route_nh_set_via, "rtnl_route_nh_set_via");

   function rtnl_route_nh_get_via (arg1 : access rtnl_nexthop) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/nexthop.h:57
   pragma Import (C, rtnl_route_nh_get_via, "rtnl_route_nh_get_via");

   function rtnl_route_nh_flags2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/nexthop.h:58
   pragma Import (C, rtnl_route_nh_flags2str, "rtnl_route_nh_flags2str");

   function rtnl_route_nh_str2flags (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/nexthop.h:59
   pragma Import (C, rtnl_route_nh_str2flags, "rtnl_route_nh_str2flags");

   function rtnl_route_nh_encap_mpls
     (nh : access rtnl_nexthop;
      addr : access netlink_addr_h.nl_addr;
      ttl : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t) return int;  -- ../include/netlink/route/nexthop.h:64
   pragma Import (C, rtnl_route_nh_encap_mpls, "rtnl_route_nh_encap_mpls");

end netlink_route_nexthop_h;
