pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
limited with netlink_handlers_h;
with System;
limited with netlink_object_h;
with Interfaces.C.Strings;
with nlada_stddef_h;
with x86_64_linux_gnu_bits_stdint_uintn_h;
limited with netlink_addr_h;
with x86_64_linux_gnu_bits_stdint_intn_h;
limited with netlink_data_h;
with x86_64_linux_gnu_sys_types_h;

package netlink_route_link_h is

   package stddef_h renames nlada_stddef_h;

   --  unsupported macro: RTNL_LINK_STATS_MAX (__RTNL_LINK_STATS_MAX - 1)
   type rtnl_link is null record;   -- incomplete struct

   type rtnl_link_stat_id_t is 
     (RTNL_LINK_RX_PACKETS,
      RTNL_LINK_TX_PACKETS,
      RTNL_LINK_RX_BYTES,
      RTNL_LINK_TX_BYTES,
      RTNL_LINK_RX_ERRORS,
      RTNL_LINK_TX_ERRORS,
      RTNL_LINK_RX_DROPPED,
      RTNL_LINK_TX_DROPPED,
      RTNL_LINK_RX_COMPRESSED,
      RTNL_LINK_TX_COMPRESSED,
      RTNL_LINK_RX_FIFO_ERR,
      RTNL_LINK_TX_FIFO_ERR,
      RTNL_LINK_RX_LEN_ERR,
      RTNL_LINK_RX_OVER_ERR,
      RTNL_LINK_RX_CRC_ERR,
      RTNL_LINK_RX_FRAME_ERR,
      RTNL_LINK_RX_MISSED_ERR,
      RTNL_LINK_TX_ABORT_ERR,
      RTNL_LINK_TX_CARRIER_ERR,
      RTNL_LINK_TX_HBEAT_ERR,
      RTNL_LINK_TX_WIN_ERR,
      RTNL_LINK_COLLISIONS,
      RTNL_LINK_MULTICAST,
      RTNL_LINK_IP6_INPKTS,
      RTNL_LINK_IP6_INHDRERRORS,
      RTNL_LINK_IP6_INTOOBIGERRORS,
      RTNL_LINK_IP6_INNOROUTES,
      RTNL_LINK_IP6_INADDRERRORS,
      RTNL_LINK_IP6_INUNKNOWNPROTOS,
      RTNL_LINK_IP6_INTRUNCATEDPKTS,
      RTNL_LINK_IP6_INDISCARDS,
      RTNL_LINK_IP6_INDELIVERS,
      RTNL_LINK_IP6_OUTFORWDATAGRAMS,
      RTNL_LINK_IP6_OUTPKTS,
      RTNL_LINK_IP6_OUTDISCARDS,
      RTNL_LINK_IP6_OUTNOROUTES,
      RTNL_LINK_IP6_REASMTIMEOUT,
      RTNL_LINK_IP6_REASMREQDS,
      RTNL_LINK_IP6_REASMOKS,
      RTNL_LINK_IP6_REASMFAILS,
      RTNL_LINK_IP6_FRAGOKS,
      RTNL_LINK_IP6_FRAGFAILS,
      RTNL_LINK_IP6_FRAGCREATES,
      RTNL_LINK_IP6_INMCASTPKTS,
      RTNL_LINK_IP6_OUTMCASTPKTS,
      RTNL_LINK_IP6_INBCASTPKTS,
      RTNL_LINK_IP6_OUTBCASTPKTS,
      RTNL_LINK_IP6_INOCTETS,
      RTNL_LINK_IP6_OUTOCTETS,
      RTNL_LINK_IP6_INMCASTOCTETS,
      RTNL_LINK_IP6_OUTMCASTOCTETS,
      RTNL_LINK_IP6_INBCASTOCTETS,
      RTNL_LINK_IP6_OUTBCASTOCTETS,
      RTNL_LINK_ICMP6_INMSGS,
      RTNL_LINK_ICMP6_INERRORS,
      RTNL_LINK_ICMP6_OUTMSGS,
      RTNL_LINK_ICMP6_OUTERRORS,
      RTNL_LINK_ICMP6_CSUMERRORS,
      RTNL_LINK_IP6_CSUMERRORS,
      RTNL_LINK_IP6_NOECTPKTS,
      RTNL_LINK_IP6_ECT1PKTS,
      RTNL_LINK_IP6_ECT0PKTS,
      RTNL_LINK_IP6_CEPKTS,
      RTNL_LINK_RX_NOHANDLER,
      RTNL_LINK_REASM_OVERLAPS,
      uu_RTNL_LINK_STATS_MAX);
   pragma Convention (C, rtnl_link_stat_id_t);  -- ../include/netlink/route/link.h:98

   --  Too big to declare.
   --   rtln_link_policy : aliased array (size_t) of aliased netlink_attr_h.nla_policy;  -- ../include/netlink/route/link.h:102
   --   pragma Import (C, rtln_link_policy, "rtln_link_policy");

   function rtnl_link_alloc return access rtnl_link;  -- ../include/netlink/route/link.h:104
   pragma Import (C, rtnl_link_alloc, "rtnl_link_alloc");

   procedure rtnl_link_put (arg1 : access rtnl_link);  -- ../include/netlink/route/link.h:105
   pragma Import (C, rtnl_link_put, "rtnl_link_put");

   function rtnl_link_alloc_cache
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : int;
      arg3 : System.Address) return int;  -- ../include/netlink/route/link.h:107
   pragma Import (C, rtnl_link_alloc_cache, "rtnl_link_alloc_cache");

   function rtnl_link_alloc_cache_flags
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : int;
      arg3 : System.Address;
      flags : unsigned) return int;  -- ../include/netlink/route/link.h:108
   pragma Import (C, rtnl_link_alloc_cache_flags, "rtnl_link_alloc_cache_flags");

   function rtnl_link_get (arg1 : access netlink_object_h.nl_cache; arg2 : int) return access rtnl_link;  -- ../include/netlink/route/link.h:111
   pragma Import (C, rtnl_link_get, "rtnl_link_get");

   function rtnl_link_get_by_name (arg1 : access netlink_object_h.nl_cache; arg2 : Interfaces.C.Strings.chars_ptr) return access rtnl_link;  -- ../include/netlink/route/link.h:112
   pragma Import (C, rtnl_link_get_by_name, "rtnl_link_get_by_name");

   function rtnl_link_build_add_request
     (arg1 : access rtnl_link;
      arg2 : int;
      arg3 : System.Address) return int;  -- ../include/netlink/route/link.h:115
   pragma Import (C, rtnl_link_build_add_request, "rtnl_link_build_add_request");

   function rtnl_link_add
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : access rtnl_link;
      arg3 : int) return int;  -- ../include/netlink/route/link.h:117
   pragma Import (C, rtnl_link_add, "rtnl_link_add");

   function rtnl_link_build_change_request
     (arg1 : access rtnl_link;
      arg2 : access rtnl_link;
      arg3 : int;
      arg4 : System.Address) return int;  -- ../include/netlink/route/link.h:118
   pragma Import (C, rtnl_link_build_change_request, "rtnl_link_build_change_request");

   function rtnl_link_change
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : access rtnl_link;
      arg3 : access rtnl_link;
      arg4 : int) return int;  -- ../include/netlink/route/link.h:121
   pragma Import (C, rtnl_link_change, "rtnl_link_change");

   function rtnl_link_build_delete_request (arg1 : access constant rtnl_link; arg2 : System.Address) return int;  -- ../include/netlink/route/link.h:124
   pragma Import (C, rtnl_link_build_delete_request, "rtnl_link_build_delete_request");

   function rtnl_link_delete (arg1 : access netlink_handlers_h.nl_sock; arg2 : access constant rtnl_link) return int;  -- ../include/netlink/route/link.h:126
   pragma Import (C, rtnl_link_delete, "rtnl_link_delete");

   function rtnl_link_build_get_request
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : System.Address) return int;  -- ../include/netlink/route/link.h:127
   pragma Import (C, rtnl_link_build_get_request, "rtnl_link_build_get_request");

   function rtnl_link_get_kernel
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : int;
      arg3 : Interfaces.C.Strings.chars_ptr;
      arg4 : System.Address) return int;  -- ../include/netlink/route/link.h:129
   pragma Import (C, rtnl_link_get_kernel, "rtnl_link_get_kernel");

   function rtnl_link_i2name
     (arg1 : access netlink_object_h.nl_cache;
      arg2 : int;
      arg3 : Interfaces.C.Strings.chars_ptr;
      arg4 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:133
   pragma Import (C, rtnl_link_i2name, "rtnl_link_i2name");

   function rtnl_link_name2i (arg1 : access netlink_object_h.nl_cache; arg2 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/link.h:134
   pragma Import (C, rtnl_link_name2i, "rtnl_link_name2i");

   function rtnl_link_stat2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:137
   pragma Import (C, rtnl_link_stat2str, "rtnl_link_stat2str");

   function rtnl_link_str2stat (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/link.h:138
   pragma Import (C, rtnl_link_str2stat, "rtnl_link_str2stat");

   function rtnl_link_flags2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:141
   pragma Import (C, rtnl_link_flags2str, "rtnl_link_flags2str");

   function rtnl_link_str2flags (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/link.h:142
   pragma Import (C, rtnl_link_str2flags, "rtnl_link_str2flags");

   function rtnl_link_operstate2str
     (arg1 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:144
   pragma Import (C, rtnl_link_operstate2str, "rtnl_link_operstate2str");

   function rtnl_link_str2operstate (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/link.h:145
   pragma Import (C, rtnl_link_str2operstate, "rtnl_link_str2operstate");

   function rtnl_link_mode2str
     (arg1 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:147
   pragma Import (C, rtnl_link_mode2str, "rtnl_link_mode2str");

   function rtnl_link_str2mode (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/link.h:148
   pragma Import (C, rtnl_link_str2mode, "rtnl_link_str2mode");

   function rtnl_link_carrier2str
     (arg1 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:151
   pragma Import (C, rtnl_link_carrier2str, "rtnl_link_carrier2str");

   function rtnl_link_str2carrier (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/link.h:152
   pragma Import (C, rtnl_link_str2carrier, "rtnl_link_str2carrier");

   procedure rtnl_link_set_qdisc (arg1 : access rtnl_link; arg2 : Interfaces.C.Strings.chars_ptr);  -- ../include/netlink/route/link.h:155
   pragma Import (C, rtnl_link_set_qdisc, "rtnl_link_set_qdisc");

   function rtnl_link_get_qdisc (arg1 : access rtnl_link) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:156
   pragma Import (C, rtnl_link_get_qdisc, "rtnl_link_get_qdisc");

   procedure rtnl_link_set_name (arg1 : access rtnl_link; arg2 : Interfaces.C.Strings.chars_ptr);  -- ../include/netlink/route/link.h:158
   pragma Import (C, rtnl_link_set_name, "rtnl_link_set_name");

   function rtnl_link_get_name (arg1 : access rtnl_link) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:159
   pragma Import (C, rtnl_link_get_name, "rtnl_link_get_name");

   procedure rtnl_link_set_group (arg1 : access rtnl_link; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/route/link.h:161
   pragma Import (C, rtnl_link_set_group, "rtnl_link_set_group");

   function rtnl_link_get_group (arg1 : access rtnl_link) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/link.h:162
   pragma Import (C, rtnl_link_get_group, "rtnl_link_get_group");

   procedure rtnl_link_set_flags (arg1 : access rtnl_link; arg2 : unsigned);  -- ../include/netlink/route/link.h:164
   pragma Import (C, rtnl_link_set_flags, "rtnl_link_set_flags");

   procedure rtnl_link_unset_flags (arg1 : access rtnl_link; arg2 : unsigned);  -- ../include/netlink/route/link.h:165
   pragma Import (C, rtnl_link_unset_flags, "rtnl_link_unset_flags");

   function rtnl_link_get_flags (arg1 : access rtnl_link) return unsigned;  -- ../include/netlink/route/link.h:166
   pragma Import (C, rtnl_link_get_flags, "rtnl_link_get_flags");

   procedure rtnl_link_set_mtu (arg1 : access rtnl_link; arg2 : unsigned);  -- ../include/netlink/route/link.h:168
   pragma Import (C, rtnl_link_set_mtu, "rtnl_link_set_mtu");

   function rtnl_link_get_mtu (arg1 : access rtnl_link) return unsigned;  -- ../include/netlink/route/link.h:169
   pragma Import (C, rtnl_link_get_mtu, "rtnl_link_get_mtu");

   procedure rtnl_link_set_txqlen (arg1 : access rtnl_link; arg2 : unsigned);  -- ../include/netlink/route/link.h:171
   pragma Import (C, rtnl_link_set_txqlen, "rtnl_link_set_txqlen");

   function rtnl_link_get_txqlen (arg1 : access rtnl_link) return unsigned;  -- ../include/netlink/route/link.h:172
   pragma Import (C, rtnl_link_get_txqlen, "rtnl_link_get_txqlen");

   procedure rtnl_link_set_ifindex (arg1 : access rtnl_link; arg2 : int);  -- ../include/netlink/route/link.h:174
   pragma Import (C, rtnl_link_set_ifindex, "rtnl_link_set_ifindex");

   function rtnl_link_get_ifindex (arg1 : access rtnl_link) return int;  -- ../include/netlink/route/link.h:175
   pragma Import (C, rtnl_link_get_ifindex, "rtnl_link_get_ifindex");

   procedure rtnl_link_set_family (arg1 : access rtnl_link; arg2 : int);  -- ../include/netlink/route/link.h:177
   pragma Import (C, rtnl_link_set_family, "rtnl_link_set_family");

   function rtnl_link_get_family (arg1 : access rtnl_link) return int;  -- ../include/netlink/route/link.h:178
   pragma Import (C, rtnl_link_get_family, "rtnl_link_get_family");

   procedure rtnl_link_set_arptype (arg1 : access rtnl_link; arg2 : unsigned);  -- ../include/netlink/route/link.h:180
   pragma Import (C, rtnl_link_set_arptype, "rtnl_link_set_arptype");

   function rtnl_link_get_arptype (arg1 : access rtnl_link) return unsigned;  -- ../include/netlink/route/link.h:181
   pragma Import (C, rtnl_link_get_arptype, "rtnl_link_get_arptype");

   procedure rtnl_link_set_addr (arg1 : access rtnl_link; arg2 : access netlink_addr_h.nl_addr);  -- ../include/netlink/route/link.h:183
   pragma Import (C, rtnl_link_set_addr, "rtnl_link_set_addr");

   function rtnl_link_get_addr (arg1 : access rtnl_link) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/link.h:184
   pragma Import (C, rtnl_link_get_addr, "rtnl_link_get_addr");

   procedure rtnl_link_set_broadcast (arg1 : access rtnl_link; arg2 : access netlink_addr_h.nl_addr);  -- ../include/netlink/route/link.h:186
   pragma Import (C, rtnl_link_set_broadcast, "rtnl_link_set_broadcast");

   function rtnl_link_get_broadcast (arg1 : access rtnl_link) return access netlink_addr_h.nl_addr;  -- ../include/netlink/route/link.h:187
   pragma Import (C, rtnl_link_get_broadcast, "rtnl_link_get_broadcast");

   procedure rtnl_link_set_link (arg1 : access rtnl_link; arg2 : int);  -- ../include/netlink/route/link.h:189
   pragma Import (C, rtnl_link_set_link, "rtnl_link_set_link");

   function rtnl_link_get_link (arg1 : access rtnl_link) return int;  -- ../include/netlink/route/link.h:190
   pragma Import (C, rtnl_link_get_link, "rtnl_link_get_link");

   procedure rtnl_link_set_master (arg1 : access rtnl_link; arg2 : int);  -- ../include/netlink/route/link.h:192
   pragma Import (C, rtnl_link_set_master, "rtnl_link_set_master");

   function rtnl_link_get_master (arg1 : access rtnl_link) return int;  -- ../include/netlink/route/link.h:193
   pragma Import (C, rtnl_link_get_master, "rtnl_link_get_master");

   procedure rtnl_link_set_carrier (arg1 : access rtnl_link; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t);  -- ../include/netlink/route/link.h:195
   pragma Import (C, rtnl_link_set_carrier, "rtnl_link_set_carrier");

   function rtnl_link_get_carrier (arg1 : access rtnl_link) return x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;  -- ../include/netlink/route/link.h:196
   pragma Import (C, rtnl_link_get_carrier, "rtnl_link_get_carrier");

   function rtnl_link_get_carrier_changes (arg1 : access rtnl_link; arg2 : access x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t) return int;  -- ../include/netlink/route/link.h:198
   pragma Import (C, rtnl_link_get_carrier_changes, "rtnl_link_get_carrier_changes");

   procedure rtnl_link_set_operstate (arg1 : access rtnl_link; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t);  -- ../include/netlink/route/link.h:200
   pragma Import (C, rtnl_link_set_operstate, "rtnl_link_set_operstate");

   function rtnl_link_get_operstate (arg1 : access rtnl_link) return x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;  -- ../include/netlink/route/link.h:201
   pragma Import (C, rtnl_link_get_operstate, "rtnl_link_get_operstate");

   procedure rtnl_link_set_linkmode (arg1 : access rtnl_link; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t);  -- ../include/netlink/route/link.h:203
   pragma Import (C, rtnl_link_set_linkmode, "rtnl_link_set_linkmode");

   function rtnl_link_get_linkmode (arg1 : access rtnl_link) return x86_64_linux_gnu_bits_stdint_uintn_h.uint8_t;  -- ../include/netlink/route/link.h:204
   pragma Import (C, rtnl_link_get_linkmode, "rtnl_link_get_linkmode");

   function rtnl_link_set_link_netnsid (link : access rtnl_link; link_netnsid : x86_64_linux_gnu_bits_stdint_intn_h.int32_t) return int;  -- ../include/netlink/route/link.h:206
   pragma Import (C, rtnl_link_set_link_netnsid, "rtnl_link_set_link_netnsid");

   function rtnl_link_get_link_netnsid (link : access constant rtnl_link; out_link_netnsid : access x86_64_linux_gnu_bits_stdint_intn_h.int32_t) return int;  -- ../include/netlink/route/link.h:207
   pragma Import (C, rtnl_link_get_link_netnsid, "rtnl_link_get_link_netnsid");

   function rtnl_link_get_ifalias (arg1 : access rtnl_link) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:209
   pragma Import (C, rtnl_link_get_ifalias, "rtnl_link_get_ifalias");

   procedure rtnl_link_set_ifalias (arg1 : access rtnl_link; arg2 : Interfaces.C.Strings.chars_ptr);  -- ../include/netlink/route/link.h:210
   pragma Import (C, rtnl_link_set_ifalias, "rtnl_link_set_ifalias");

   function rtnl_link_get_num_vf (arg1 : access rtnl_link; arg2 : access x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t) return int;  -- ../include/netlink/route/link.h:212
   pragma Import (C, rtnl_link_get_num_vf, "rtnl_link_get_num_vf");

   function rtnl_link_get_stat (arg1 : access rtnl_link; arg2 : rtnl_link_stat_id_t) return x86_64_linux_gnu_bits_stdint_uintn_h.uint64_t;  -- ../include/netlink/route/link.h:214
   pragma Import (C, rtnl_link_get_stat, "rtnl_link_get_stat");

   function rtnl_link_set_stat
     (arg1 : access rtnl_link;
      arg2 : rtnl_link_stat_id_t;
      arg3 : x86_64_linux_gnu_bits_stdint_uintn_h.uint64_t) return int;  -- ../include/netlink/route/link.h:215
   pragma Import (C, rtnl_link_set_stat, "rtnl_link_set_stat");

   function rtnl_link_set_type (arg1 : access rtnl_link; arg2 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/link.h:218
   pragma Import (C, rtnl_link_set_type, "rtnl_link_set_type");

   function rtnl_link_get_type (arg1 : access rtnl_link) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:219
   pragma Import (C, rtnl_link_get_type, "rtnl_link_get_type");

   function rtnl_link_set_slave_type (arg1 : access rtnl_link; arg2 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/link.h:221
   pragma Import (C, rtnl_link_set_slave_type, "rtnl_link_set_slave_type");

   function rtnl_link_get_slave_type (arg1 : access constant rtnl_link) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:222
   pragma Import (C, rtnl_link_get_slave_type, "rtnl_link_get_slave_type");

   procedure rtnl_link_set_promiscuity (arg1 : access rtnl_link; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/route/link.h:224
   pragma Import (C, rtnl_link_set_promiscuity, "rtnl_link_set_promiscuity");

   function rtnl_link_get_promiscuity (arg1 : access rtnl_link) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/link.h:225
   pragma Import (C, rtnl_link_get_promiscuity, "rtnl_link_get_promiscuity");

   procedure rtnl_link_set_num_tx_queues (arg1 : access rtnl_link; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/route/link.h:227
   pragma Import (C, rtnl_link_set_num_tx_queues, "rtnl_link_set_num_tx_queues");

   function rtnl_link_get_num_tx_queues (arg1 : access rtnl_link) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/link.h:228
   pragma Import (C, rtnl_link_get_num_tx_queues, "rtnl_link_get_num_tx_queues");

   procedure rtnl_link_set_num_rx_queues (arg1 : access rtnl_link; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/route/link.h:230
   pragma Import (C, rtnl_link_set_num_rx_queues, "rtnl_link_set_num_rx_queues");

   function rtnl_link_get_num_rx_queues (arg1 : access rtnl_link) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/route/link.h:231
   pragma Import (C, rtnl_link_get_num_rx_queues, "rtnl_link_get_num_rx_queues");

   function rtnl_link_get_gso_max_segs (arg1 : access rtnl_link; arg2 : access x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t) return int;  -- ../include/netlink/route/link.h:233
   pragma Import (C, rtnl_link_get_gso_max_segs, "rtnl_link_get_gso_max_segs");

   function rtnl_link_get_gso_max_size (arg1 : access rtnl_link; arg2 : access x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t) return int;  -- ../include/netlink/route/link.h:235
   pragma Import (C, rtnl_link_get_gso_max_size, "rtnl_link_get_gso_max_size");

   function rtnl_link_get_phys_port_id (arg1 : access rtnl_link) return access netlink_data_h.nl_data;  -- ../include/netlink/route/link.h:237
   pragma Import (C, rtnl_link_get_phys_port_id, "rtnl_link_get_phys_port_id");

   function rtnl_link_get_phys_port_name (arg1 : access rtnl_link) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:239
   pragma Import (C, rtnl_link_get_phys_port_name, "rtnl_link_get_phys_port_name");

   function rtnl_link_get_phys_switch_id (arg1 : access rtnl_link) return access netlink_data_h.nl_data;  -- ../include/netlink/route/link.h:241
   pragma Import (C, rtnl_link_get_phys_switch_id, "rtnl_link_get_phys_switch_id");

   procedure rtnl_link_set_ns_fd (arg1 : access rtnl_link; arg2 : int);  -- ../include/netlink/route/link.h:243
   pragma Import (C, rtnl_link_set_ns_fd, "rtnl_link_set_ns_fd");

   function rtnl_link_get_ns_fd (arg1 : access rtnl_link) return int;  -- ../include/netlink/route/link.h:244
   pragma Import (C, rtnl_link_get_ns_fd, "rtnl_link_get_ns_fd");

   procedure rtnl_link_set_ns_pid (arg1 : access rtnl_link; arg2 : x86_64_linux_gnu_sys_types_h.pid_t);  -- ../include/netlink/route/link.h:245
   pragma Import (C, rtnl_link_set_ns_pid, "rtnl_link_set_ns_pid");

   function rtnl_link_get_ns_pid (arg1 : access rtnl_link) return x86_64_linux_gnu_sys_types_h.pid_t;  -- ../include/netlink/route/link.h:246
   pragma Import (C, rtnl_link_get_ns_pid, "rtnl_link_get_ns_pid");

   function rtnl_link_enslave_ifindex
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : int;
      arg3 : int) return int;  -- ../include/netlink/route/link.h:248
   pragma Import (C, rtnl_link_enslave_ifindex, "rtnl_link_enslave_ifindex");

   function rtnl_link_enslave
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : access rtnl_link;
      arg3 : access rtnl_link) return int;  -- ../include/netlink/route/link.h:249
   pragma Import (C, rtnl_link_enslave, "rtnl_link_enslave");

   function rtnl_link_release_ifindex (arg1 : access netlink_handlers_h.nl_sock; arg2 : int) return int;  -- ../include/netlink/route/link.h:251
   pragma Import (C, rtnl_link_release_ifindex, "rtnl_link_release_ifindex");

   function rtnl_link_release (arg1 : access netlink_handlers_h.nl_sock; arg2 : access rtnl_link) return int;  -- ../include/netlink/route/link.h:252
   pragma Import (C, rtnl_link_release, "rtnl_link_release");

   function rtnl_link_fill_info (arg1 : access netlink_handlers_h.nl_msg; arg2 : access rtnl_link) return int;  -- ../include/netlink/route/link.h:253
   pragma Import (C, rtnl_link_fill_info, "rtnl_link_fill_info");

   function rtnl_link_info_parse (arg1 : access rtnl_link; arg2 : System.Address) return int;  -- ../include/netlink/route/link.h:254
   pragma Import (C, rtnl_link_info_parse, "rtnl_link_info_parse");

   function rtnl_link_has_vf_list (arg1 : access rtnl_link) return int;  -- ../include/netlink/route/link.h:256
   pragma Import (C, rtnl_link_has_vf_list, "rtnl_link_has_vf_list");

   procedure rtnl_link_set_vf_list (arg1 : access rtnl_link);  -- ../include/netlink/route/link.h:257
   pragma Import (C, rtnl_link_set_vf_list, "rtnl_link_set_vf_list");

   procedure rtnl_link_unset_vf_list (arg1 : access rtnl_link);  -- ../include/netlink/route/link.h:258
   pragma Import (C, rtnl_link_unset_vf_list, "rtnl_link_unset_vf_list");

   function rtnl_link_set_info_type (arg1 : access rtnl_link; arg2 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/link.h:262
   pragma Import (C, rtnl_link_set_info_type, "rtnl_link_set_info_type");

   function rtnl_link_get_info_type (arg1 : access rtnl_link) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link.h:263
   pragma Import (C, rtnl_link_get_info_type, "rtnl_link_get_info_type");

   procedure rtnl_link_set_weight (arg1 : access rtnl_link; arg2 : unsigned);  -- ../include/netlink/route/link.h:264
   pragma Import (C, rtnl_link_set_weight, "rtnl_link_set_weight");

   function rtnl_link_get_weight (arg1 : access rtnl_link) return unsigned;  -- ../include/netlink/route/link.h:265
   pragma Import (C, rtnl_link_get_weight, "rtnl_link_get_weight");

end netlink_route_link_h;
