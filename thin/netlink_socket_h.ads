pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with netlink_handlers_h;
with x86_64_linux_gnu_bits_stdint_uintn_h;
with System;
with nlada_stddef_h;

package netlink_socket_h is

   package stddef_h renames nlada_stddef_h;

   function nl_socket_alloc return access netlink_handlers_h.nl_sock;  -- ../include/netlink/socket.h:16
   pragma Import (C, nl_socket_alloc, "nl_socket_alloc");

   function nl_socket_alloc_cb (arg1 : access netlink_handlers_h.nl_cb) return access netlink_handlers_h.nl_sock;  -- ../include/netlink/socket.h:17
   pragma Import (C, nl_socket_alloc_cb, "nl_socket_alloc_cb");

   procedure nl_socket_free (arg1 : access netlink_handlers_h.nl_sock);  -- ../include/netlink/socket.h:18
   pragma Import (C, nl_socket_free, "nl_socket_free");

   function nl_socket_get_local_port (arg1 : access constant netlink_handlers_h.nl_sock) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/socket.h:20
   pragma Import (C, nl_socket_get_local_port, "nl_socket_get_local_port");

   procedure nl_socket_set_local_port (arg1 : access netlink_handlers_h.nl_sock; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/socket.h:21
   pragma Import (C, nl_socket_set_local_port, "nl_socket_set_local_port");

   function nl_socket_add_memberships (arg1 : access netlink_handlers_h.nl_sock; arg2 : int  -- , ...
      ) return int;  -- ../include/netlink/socket.h:23
   pragma Import (C, nl_socket_add_memberships, "nl_socket_add_memberships");

   function nl_socket_add_membership (arg1 : access netlink_handlers_h.nl_sock; arg2 : int) return int;  -- ../include/netlink/socket.h:24
   pragma Import (C, nl_socket_add_membership, "nl_socket_add_membership");

   function nl_socket_drop_memberships (arg1 : access netlink_handlers_h.nl_sock; arg2 : int  -- , ...
      ) return int;  -- ../include/netlink/socket.h:25
   pragma Import (C, nl_socket_drop_memberships, "nl_socket_drop_memberships");

   function nl_socket_drop_membership (arg1 : access netlink_handlers_h.nl_sock; arg2 : int) return int;  -- ../include/netlink/socket.h:26
   pragma Import (C, nl_socket_drop_membership, "nl_socket_drop_membership");

   procedure nl_join_groups (arg1 : access netlink_handlers_h.nl_sock; arg2 : int);  -- ../include/netlink/socket.h:28
   pragma Import (C, nl_join_groups, "nl_join_groups");

   function nl_socket_get_peer_port (arg1 : access constant netlink_handlers_h.nl_sock) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/socket.h:31
   pragma Import (C, nl_socket_get_peer_port, "nl_socket_get_peer_port");

   procedure nl_socket_set_peer_port (arg1 : access netlink_handlers_h.nl_sock; arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/socket.h:32
   pragma Import (C, nl_socket_set_peer_port, "nl_socket_set_peer_port");

   function nl_socket_get_peer_groups (sk : access constant netlink_handlers_h.nl_sock) return x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;  -- ../include/netlink/socket.h:34
   pragma Import (C, nl_socket_get_peer_groups, "nl_socket_get_peer_groups");

   procedure nl_socket_set_peer_groups (sk : access netlink_handlers_h.nl_sock; groups : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t);  -- ../include/netlink/socket.h:35
   pragma Import (C, nl_socket_set_peer_groups, "nl_socket_set_peer_groups");

   function nl_socket_get_cb (arg1 : access constant netlink_handlers_h.nl_sock) return access netlink_handlers_h.nl_cb;  -- ../include/netlink/socket.h:36
   pragma Import (C, nl_socket_get_cb, "nl_socket_get_cb");

   procedure nl_socket_set_cb (arg1 : access netlink_handlers_h.nl_sock; arg2 : access netlink_handlers_h.nl_cb);  -- ../include/netlink/socket.h:37
   pragma Import (C, nl_socket_set_cb, "nl_socket_set_cb");

   function nl_socket_modify_cb
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : netlink_handlers_h.nl_cb_type;
      arg3 : netlink_handlers_h.nl_cb_kind;
      arg4 : netlink_handlers_h.nl_recvmsg_msg_cb_t;
      arg5 : System.Address) return int;  -- ../include/netlink/socket.h:39
   pragma Import (C, nl_socket_modify_cb, "nl_socket_modify_cb");

   function nl_socket_modify_err_cb
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : netlink_handlers_h.nl_cb_kind;
      arg3 : netlink_handlers_h.nl_recvmsg_err_cb_t;
      arg4 : System.Address) return int;  -- ../include/netlink/socket.h:42
   pragma Import (C, nl_socket_modify_err_cb, "nl_socket_modify_err_cb");

   function nl_socket_set_buffer_size
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : int;
      arg3 : int) return int;  -- ../include/netlink/socket.h:45
   pragma Import (C, nl_socket_set_buffer_size, "nl_socket_set_buffer_size");

   function nl_socket_set_msg_buf_size (arg1 : access netlink_handlers_h.nl_sock; arg2 : stddef_h.size_t) return int;  -- ../include/netlink/socket.h:46
   pragma Import (C, nl_socket_set_msg_buf_size, "nl_socket_set_msg_buf_size");

   function nl_socket_get_msg_buf_size (arg1 : access netlink_handlers_h.nl_sock) return stddef_h.size_t;  -- ../include/netlink/socket.h:47
   pragma Import (C, nl_socket_get_msg_buf_size, "nl_socket_get_msg_buf_size");

   function nl_socket_set_passcred (arg1 : access netlink_handlers_h.nl_sock; arg2 : int) return int;  -- ../include/netlink/socket.h:48
   pragma Import (C, nl_socket_set_passcred, "nl_socket_set_passcred");

   function nl_socket_recv_pktinfo (arg1 : access netlink_handlers_h.nl_sock; arg2 : int) return int;  -- ../include/netlink/socket.h:49
   pragma Import (C, nl_socket_recv_pktinfo, "nl_socket_recv_pktinfo");

   procedure nl_socket_disable_seq_check (arg1 : access netlink_handlers_h.nl_sock);  -- ../include/netlink/socket.h:51
   pragma Import (C, nl_socket_disable_seq_check, "nl_socket_disable_seq_check");

   function nl_socket_use_seq (arg1 : access netlink_handlers_h.nl_sock) return unsigned;  -- ../include/netlink/socket.h:52
   pragma Import (C, nl_socket_use_seq, "nl_socket_use_seq");

   procedure nl_socket_disable_auto_ack (arg1 : access netlink_handlers_h.nl_sock);  -- ../include/netlink/socket.h:53
   pragma Import (C, nl_socket_disable_auto_ack, "nl_socket_disable_auto_ack");

   procedure nl_socket_enable_auto_ack (arg1 : access netlink_handlers_h.nl_sock);  -- ../include/netlink/socket.h:54
   pragma Import (C, nl_socket_enable_auto_ack, "nl_socket_enable_auto_ack");

   function nl_socket_get_fd (arg1 : access constant netlink_handlers_h.nl_sock) return int;  -- ../include/netlink/socket.h:56
   pragma Import (C, nl_socket_get_fd, "nl_socket_get_fd");

   function nl_socket_set_fd
     (sk : access netlink_handlers_h.nl_sock;
      protocol : int;
      fd : int) return int;  -- ../include/netlink/socket.h:57
   pragma Import (C, nl_socket_set_fd, "nl_socket_set_fd");

   function nl_socket_set_nonblocking (arg1 : access constant netlink_handlers_h.nl_sock) return int;  -- ../include/netlink/socket.h:58
   pragma Import (C, nl_socket_set_nonblocking, "nl_socket_set_nonblocking");

   procedure nl_socket_enable_msg_peek (arg1 : access netlink_handlers_h.nl_sock);  -- ../include/netlink/socket.h:59
   pragma Import (C, nl_socket_enable_msg_peek, "nl_socket_enable_msg_peek");

   procedure nl_socket_disable_msg_peek (arg1 : access netlink_handlers_h.nl_sock);  -- ../include/netlink/socket.h:60
   pragma Import (C, nl_socket_disable_msg_peek, "nl_socket_disable_msg_peek");

end netlink_socket_h;
