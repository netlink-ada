pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with nlada_stddef_h;
with x86_64_linux_gnu_bits_stdint_uintn_h;
limited with netlink_route_link_h;

package netlink_route_link_inet_h is

   package stddef_h renames nlada_stddef_h;

   function rtnl_link_inet_devconf2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/link/inet.h:15
   pragma Import (C, rtnl_link_inet_devconf2str, "rtnl_link_inet_devconf2str");

   function rtnl_link_inet_str2devconf (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/link/inet.h:16
   pragma Import (C, rtnl_link_inet_str2devconf, "rtnl_link_inet_str2devconf");

   function rtnl_link_inet_get_conf
     (arg1 : access netlink_route_link_h.rtnl_link;
      arg2 : unsigned;
      arg3 : access x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t) return int;  -- ../include/netlink/route/link/inet.h:18
   pragma Import (C, rtnl_link_inet_get_conf, "rtnl_link_inet_get_conf");

   function rtnl_link_inet_set_conf
     (arg1 : access netlink_route_link_h.rtnl_link;
      arg2 : unsigned;
      arg3 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t) return int;  -- ../include/netlink/route/link/inet.h:20
   pragma Import (C, rtnl_link_inet_set_conf, "rtnl_link_inet_set_conf");

end netlink_route_link_inet_h;
