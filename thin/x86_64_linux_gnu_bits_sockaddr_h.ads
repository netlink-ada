pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package x86_64_linux_gnu_bits_sockaddr_h is

   subtype sa_family_t is unsigned_short;  -- /usr/include/x86_64-linux-gnu/bits/sockaddr.h:28

end x86_64_linux_gnu_bits_sockaddr_h;
