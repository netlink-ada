pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
limited with netlink_object_h;
with System;
with x86_64_linux_gnu_bits_stdint_uintn_h;
limited with netlink_netlink_h;
limited with netlink_handlers_h;
with Interfaces.C.Strings;
limited with netlink_types_h;

package netlink_cache_h is

   --  unsupported macro: NL_ACT_MAX (__NL_ACT_MAX - 1)
   NL_CACHE_AF_ITER : constant := 16#0001#;  --  ../include/netlink/cache.h:39

   NL_AUTO_PROVIDE : constant := 1;  --  ../include/netlink/cache.h:143
   NL_ALLOCATED_SOCK : constant := 2;  --  ../include/netlink/cache.h:144

   type change_func_t is access procedure
        (arg1 : access netlink_object_h.nl_cache;
         arg2 : access netlink_object_h.nl_object;
         arg3 : int;
         arg4 : System.Address);
   pragma Convention (C, change_func_t);  -- ../include/netlink/cache.h:31

   type change_func_v2_t is access procedure
        (arg1 : access netlink_object_h.nl_cache;
         arg2 : access netlink_object_h.nl_object;
         arg3 : access netlink_object_h.nl_object;
         arg4 : x86_64_linux_gnu_bits_stdint_uintn_h.uint64_t;
         arg5 : int;
         arg6 : System.Address);
   pragma Convention (C, change_func_v2_t);  -- ../include/netlink/cache.h:32

   function nl_cache_nitems (arg1 : access netlink_object_h.nl_cache) return int;  -- ../include/netlink/cache.h:42
   pragma Import (C, nl_cache_nitems, "nl_cache_nitems");

   function nl_cache_nitems_filter (arg1 : access netlink_object_h.nl_cache; arg2 : access netlink_object_h.nl_object) return int;  -- ../include/netlink/cache.h:43
   pragma Import (C, nl_cache_nitems_filter, "nl_cache_nitems_filter");

   function nl_cache_get_ops (arg1 : access netlink_object_h.nl_cache) return access netlink_netlink_h.nl_cache_ops;  -- ../include/netlink/cache.h:45
   pragma Import (C, nl_cache_get_ops, "nl_cache_get_ops");

   function nl_cache_get_first (arg1 : access netlink_object_h.nl_cache) return access netlink_object_h.nl_object;  -- ../include/netlink/cache.h:46
   pragma Import (C, nl_cache_get_first, "nl_cache_get_first");

   function nl_cache_get_last (arg1 : access netlink_object_h.nl_cache) return access netlink_object_h.nl_object;  -- ../include/netlink/cache.h:47
   pragma Import (C, nl_cache_get_last, "nl_cache_get_last");

   function nl_cache_get_next (arg1 : access netlink_object_h.nl_object) return access netlink_object_h.nl_object;  -- ../include/netlink/cache.h:48
   pragma Import (C, nl_cache_get_next, "nl_cache_get_next");

   function nl_cache_get_prev (arg1 : access netlink_object_h.nl_object) return access netlink_object_h.nl_object;  -- ../include/netlink/cache.h:49
   pragma Import (C, nl_cache_get_prev, "nl_cache_get_prev");

   function nl_cache_alloc (arg1 : access netlink_netlink_h.nl_cache_ops) return access netlink_object_h.nl_cache;  -- ../include/netlink/cache.h:51
   pragma Import (C, nl_cache_alloc, "nl_cache_alloc");

   function nl_cache_alloc_and_fill
     (arg1 : access netlink_netlink_h.nl_cache_ops;
      arg2 : access netlink_handlers_h.nl_sock;
      arg3 : System.Address) return int;  -- ../include/netlink/cache.h:52
   pragma Import (C, nl_cache_alloc_and_fill, "nl_cache_alloc_and_fill");

   function nl_cache_alloc_name (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : System.Address) return int;  -- ../include/netlink/cache.h:55
   pragma Import (C, nl_cache_alloc_name, "nl_cache_alloc_name");

   function nl_cache_subset (arg1 : access netlink_object_h.nl_cache; arg2 : access netlink_object_h.nl_object) return access netlink_object_h.nl_cache;  -- ../include/netlink/cache.h:57
   pragma Import (C, nl_cache_subset, "nl_cache_subset");

   function nl_cache_clone (arg1 : access netlink_object_h.nl_cache) return access netlink_object_h.nl_cache;  -- ../include/netlink/cache.h:59
   pragma Import (C, nl_cache_clone, "nl_cache_clone");

   procedure nl_cache_clear (arg1 : access netlink_object_h.nl_cache);  -- ../include/netlink/cache.h:60
   pragma Import (C, nl_cache_clear, "nl_cache_clear");

   procedure nl_cache_get (arg1 : access netlink_object_h.nl_cache);  -- ../include/netlink/cache.h:61
   pragma Import (C, nl_cache_get, "nl_cache_get");

   procedure nl_cache_free (arg1 : access netlink_object_h.nl_cache);  -- ../include/netlink/cache.h:62
   pragma Import (C, nl_cache_free, "nl_cache_free");

   procedure nl_cache_put (cache : access netlink_object_h.nl_cache);  -- ../include/netlink/cache.h:63
   pragma Import (C, nl_cache_put, "nl_cache_put");

   function nl_cache_add (arg1 : access netlink_object_h.nl_cache; arg2 : access netlink_object_h.nl_object) return int;  -- ../include/netlink/cache.h:66
   pragma Import (C, nl_cache_add, "nl_cache_add");

   function nl_cache_parse_and_add (arg1 : access netlink_object_h.nl_cache; arg2 : access netlink_handlers_h.nl_msg) return int;  -- ../include/netlink/cache.h:68
   pragma Import (C, nl_cache_parse_and_add, "nl_cache_parse_and_add");

   function nl_cache_move (arg1 : access netlink_object_h.nl_cache; arg2 : access netlink_object_h.nl_object) return int;  -- ../include/netlink/cache.h:70
   pragma Import (C, nl_cache_move, "nl_cache_move");

   procedure nl_cache_remove (arg1 : access netlink_object_h.nl_object);  -- ../include/netlink/cache.h:72
   pragma Import (C, nl_cache_remove, "nl_cache_remove");

   function nl_cache_refill (arg1 : access netlink_handlers_h.nl_sock; arg2 : access netlink_object_h.nl_cache) return int;  -- ../include/netlink/cache.h:73
   pragma Import (C, nl_cache_refill, "nl_cache_refill");

   function nl_cache_pickup (arg1 : access netlink_handlers_h.nl_sock; arg2 : access netlink_object_h.nl_cache) return int;  -- ../include/netlink/cache.h:75
   pragma Import (C, nl_cache_pickup, "nl_cache_pickup");

   function nl_cache_pickup_checkdup (arg1 : access netlink_handlers_h.nl_sock; arg2 : access netlink_object_h.nl_cache) return int;  -- ../include/netlink/cache.h:77
   pragma Import (C, nl_cache_pickup_checkdup, "nl_cache_pickup_checkdup");

   function nl_cache_resync
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : access netlink_object_h.nl_cache;
      arg3 : change_func_t;
      arg4 : System.Address) return int;  -- ../include/netlink/cache.h:79
   pragma Import (C, nl_cache_resync, "nl_cache_resync");

   function nl_cache_include
     (arg1 : access netlink_object_h.nl_cache;
      arg2 : access netlink_object_h.nl_object;
      arg3 : change_func_t;
      arg4 : System.Address) return int;  -- ../include/netlink/cache.h:83
   pragma Import (C, nl_cache_include, "nl_cache_include");

   function nl_cache_include_v2
     (arg1 : access netlink_object_h.nl_cache;
      arg2 : access netlink_object_h.nl_object;
      arg3 : change_func_v2_t;
      arg4 : System.Address) return int;  -- ../include/netlink/cache.h:87
   pragma Import (C, nl_cache_include_v2, "nl_cache_include_v2");

   procedure nl_cache_set_arg1 (arg1 : access netlink_object_h.nl_cache; arg2 : int);  -- ../include/netlink/cache.h:91
   pragma Import (C, nl_cache_set_arg1, "nl_cache_set_arg1");

   procedure nl_cache_set_arg2 (arg1 : access netlink_object_h.nl_cache; arg2 : int);  -- ../include/netlink/cache.h:92
   pragma Import (C, nl_cache_set_arg2, "nl_cache_set_arg2");

   procedure nl_cache_set_flags (arg1 : access netlink_object_h.nl_cache; arg2 : unsigned);  -- ../include/netlink/cache.h:93
   pragma Import (C, nl_cache_set_flags, "nl_cache_set_flags");

   function nl_cache_is_empty (arg1 : access netlink_object_h.nl_cache) return int;  -- ../include/netlink/cache.h:96
   pragma Import (C, nl_cache_is_empty, "nl_cache_is_empty");

   function nl_cache_search (arg1 : access netlink_object_h.nl_cache; arg2 : access netlink_object_h.nl_object) return access netlink_object_h.nl_object;  -- ../include/netlink/cache.h:97
   pragma Import (C, nl_cache_search, "nl_cache_search");

   function nl_cache_find (arg1 : access netlink_object_h.nl_cache; arg2 : access netlink_object_h.nl_object) return access netlink_object_h.nl_object;  -- ../include/netlink/cache.h:99
   pragma Import (C, nl_cache_find, "nl_cache_find");

   procedure nl_cache_mark_all (arg1 : access netlink_object_h.nl_cache);  -- ../include/netlink/cache.h:101
   pragma Import (C, nl_cache_mark_all, "nl_cache_mark_all");

   procedure nl_cache_dump (arg1 : access netlink_object_h.nl_cache; arg2 : access netlink_types_h.nl_dump_params);  -- ../include/netlink/cache.h:104
   pragma Import (C, nl_cache_dump, "nl_cache_dump");

   procedure nl_cache_dump_filter
     (arg1 : access netlink_object_h.nl_cache;
      arg2 : access netlink_types_h.nl_dump_params;
      arg3 : access netlink_object_h.nl_object);  -- ../include/netlink/cache.h:106
   pragma Import (C, nl_cache_dump_filter, "nl_cache_dump_filter");

   procedure nl_cache_foreach
     (arg1 : access netlink_object_h.nl_cache;
      cb : access procedure (arg1 : access netlink_object_h.nl_object; arg2 : System.Address);
      arg : System.Address);  -- ../include/netlink/cache.h:111
   pragma Import (C, nl_cache_foreach, "nl_cache_foreach");

   procedure nl_cache_foreach_filter
     (arg1 : access netlink_object_h.nl_cache;
      arg2 : access netlink_object_h.nl_object;
      cb : access procedure (arg1 : access netlink_object_h.nl_object; arg2 : System.Address);
      arg : System.Address);  -- ../include/netlink/cache.h:115
   pragma Import (C, nl_cache_foreach_filter, "nl_cache_foreach_filter");

   function nl_cache_ops_lookup (arg1 : Interfaces.C.Strings.chars_ptr) return access netlink_netlink_h.nl_cache_ops;  -- ../include/netlink/cache.h:125
   pragma Import (C, nl_cache_ops_lookup, "nl_cache_ops_lookup");

   function nl_cache_ops_lookup_safe (arg1 : Interfaces.C.Strings.chars_ptr) return access netlink_netlink_h.nl_cache_ops;  -- ../include/netlink/cache.h:126
   pragma Import (C, nl_cache_ops_lookup_safe, "nl_cache_ops_lookup_safe");

   function nl_cache_ops_associate (arg1 : int; arg2 : int) return access netlink_netlink_h.nl_cache_ops;  -- ../include/netlink/cache.h:127
   pragma Import (C, nl_cache_ops_associate, "nl_cache_ops_associate");

   function nl_cache_ops_associate_safe (arg1 : int; arg2 : int) return access netlink_netlink_h.nl_cache_ops;  -- ../include/netlink/cache.h:128
   pragma Import (C, nl_cache_ops_associate_safe, "nl_cache_ops_associate_safe");

   type nl_msgtype is null record;   -- incomplete struct

   function nl_msgtype_lookup (arg1 : access netlink_netlink_h.nl_cache_ops; arg2 : int) return access nl_msgtype;  -- ../include/netlink/cache.h:129
   pragma Import (C, nl_msgtype_lookup, "nl_msgtype_lookup");

   procedure nl_cache_ops_foreach (cb : access procedure (arg1 : access netlink_netlink_h.nl_cache_ops; arg2 : System.Address); arg2 : System.Address);  -- ../include/netlink/cache.h:130
   pragma Import (C, nl_cache_ops_foreach, "nl_cache_ops_foreach");

   function nl_cache_mngt_register (arg1 : access netlink_netlink_h.nl_cache_ops) return int;  -- ../include/netlink/cache.h:131
   pragma Import (C, nl_cache_mngt_register, "nl_cache_mngt_register");

   function nl_cache_mngt_unregister (arg1 : access netlink_netlink_h.nl_cache_ops) return int;  -- ../include/netlink/cache.h:132
   pragma Import (C, nl_cache_mngt_unregister, "nl_cache_mngt_unregister");

   procedure nl_cache_mngt_provide (arg1 : access netlink_object_h.nl_cache);  -- ../include/netlink/cache.h:135
   pragma Import (C, nl_cache_mngt_provide, "nl_cache_mngt_provide");

   procedure nl_cache_mngt_unprovide (arg1 : access netlink_object_h.nl_cache);  -- ../include/netlink/cache.h:136
   pragma Import (C, nl_cache_mngt_unprovide, "nl_cache_mngt_unprovide");

   function nl_cache_mngt_require (arg1 : Interfaces.C.Strings.chars_ptr) return access netlink_object_h.nl_cache;  -- ../include/netlink/cache.h:137
   pragma Import (C, nl_cache_mngt_require, "nl_cache_mngt_require");

   function nl_cache_mngt_require_safe (arg1 : Interfaces.C.Strings.chars_ptr) return access netlink_object_h.nl_cache;  -- ../include/netlink/cache.h:138
   pragma Import (C, nl_cache_mngt_require_safe, "nl_cache_mngt_require_safe");

   --  skipped func __nl_cache_mngt_require

   type nl_cache_mngr is null record;   -- incomplete struct

   function nl_cache_mngr_alloc
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : int;
      arg3 : int;
      arg4 : System.Address) return int;  -- ../include/netlink/cache.h:146
   pragma Import (C, nl_cache_mngr_alloc, "nl_cache_mngr_alloc");

   function nl_cache_mngr_add
     (arg1 : access nl_cache_mngr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : change_func_t;
      arg4 : System.Address;
      arg5 : System.Address) return int;  -- ../include/netlink/cache.h:149
   pragma Import (C, nl_cache_mngr_add, "nl_cache_mngr_add");

   function nl_cache_mngr_add_cache
     (mngr : access nl_cache_mngr;
      cache : access netlink_object_h.nl_cache;
      cb : change_func_t;
      data : System.Address) return int;  -- ../include/netlink/cache.h:154
   pragma Import (C, nl_cache_mngr_add_cache, "nl_cache_mngr_add_cache");

   function nl_cache_mngr_add_cache_v2
     (mngr : access nl_cache_mngr;
      cache : access netlink_object_h.nl_cache;
      cb : change_func_v2_t;
      data : System.Address) return int;  -- ../include/netlink/cache.h:157
   pragma Import (C, nl_cache_mngr_add_cache_v2, "nl_cache_mngr_add_cache_v2");

   function nl_cache_mngr_get_fd (arg1 : access nl_cache_mngr) return int;  -- ../include/netlink/cache.h:160
   pragma Import (C, nl_cache_mngr_get_fd, "nl_cache_mngr_get_fd");

   function nl_cache_mngr_poll (arg1 : access nl_cache_mngr; arg2 : int) return int;  -- ../include/netlink/cache.h:161
   pragma Import (C, nl_cache_mngr_poll, "nl_cache_mngr_poll");

   function nl_cache_mngr_data_ready (arg1 : access nl_cache_mngr) return int;  -- ../include/netlink/cache.h:163
   pragma Import (C, nl_cache_mngr_data_ready, "nl_cache_mngr_data_ready");

   procedure nl_cache_mngr_info (arg1 : access nl_cache_mngr; arg2 : access netlink_types_h.nl_dump_params);  -- ../include/netlink/cache.h:164
   pragma Import (C, nl_cache_mngr_info, "nl_cache_mngr_info");

   procedure nl_cache_mngr_free (arg1 : access nl_cache_mngr);  -- ../include/netlink/cache.h:166
   pragma Import (C, nl_cache_mngr_free, "nl_cache_mngr_free");

   procedure nl_cache_ops_get (arg1 : access netlink_netlink_h.nl_cache_ops);  -- ../include/netlink/cache.h:168
   pragma Import (C, nl_cache_ops_get, "nl_cache_ops_get");

   procedure nl_cache_ops_put (arg1 : access netlink_netlink_h.nl_cache_ops);  -- ../include/netlink/cache.h:169
   pragma Import (C, nl_cache_ops_put, "nl_cache_ops_put");

   procedure nl_cache_ops_set_flags (arg1 : access netlink_netlink_h.nl_cache_ops; arg2 : unsigned);  -- ../include/netlink/cache.h:170
   pragma Import (C, nl_cache_ops_set_flags, "nl_cache_ops_set_flags");

end netlink_cache_h;
