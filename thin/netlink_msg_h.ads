pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with System;
limited with linux_netlink_h;
limited with netlink_handlers_h;
with nlada_stddef_h;
with x86_64_linux_gnu_bits_stdint_uintn_h;
with Interfaces.C.Strings;
limited with netlink_object_h;
with x86_64_linux_gnu_bits_types_FILE_h;
limited with netlink_attr_h;

package netlink_msg_h is

   package stddef_h renames nlada_stddef_h;

   NL_DONTPAD : constant := 0;  --  ../include/netlink/msg.h:19

   NL_AUTO_PORT : constant := 0;  --  ../include/netlink/msg.h:29
   --  unsupported macro: NL_AUTO_PID NL_AUTO_PORT

   NL_AUTO_SEQ : constant := 0;  --  ../include/netlink/msg.h:40
   --  arg-macro: procedure nlmsg_for_each_attr (pos, nlh, hdrlen, rem)
   --    nla_for_each_attr(pos, nlmsg_attrdata(nlh, hdrlen), nlmsg_attrlen(nlh, hdrlen), rem)
   --  arg-macro: procedure nlmsg_for_each (pos, head, len)
   --    for (int rem := len, pos := head; nlmsg_ok(pos, rem); pos := nlmsg_next(pos, andrem))
   --  arg-macro: procedure nlmsg_for_each_msg (pos, head, len, rem)
   --    nlmsg_for_each(pos, head, len)

   type nl_tree is null record;   -- incomplete struct

   function nlmsg_size (arg1 : int) return int;  -- ../include/netlink/msg.h:46
   pragma Import (C, nlmsg_size, "nlmsg_size");

   function nlmsg_total_size (arg1 : int) return int;  -- ../include/netlink/msg.h:47
   pragma Import (C, nlmsg_total_size, "nlmsg_total_size");

   function nlmsg_padlen (arg1 : int) return int;  -- ../include/netlink/msg.h:48
   pragma Import (C, nlmsg_padlen, "nlmsg_padlen");

   function nlmsg_data (arg1 : access constant linux_netlink_h.nlmsghdr) return System.Address;  -- ../include/netlink/msg.h:50
   pragma Import (C, nlmsg_data, "nlmsg_data");

   function nlmsg_datalen (arg1 : access constant linux_netlink_h.nlmsghdr) return int;  -- ../include/netlink/msg.h:51
   pragma Import (C, nlmsg_datalen, "nlmsg_datalen");

   function nlmsg_tail (arg1 : access constant linux_netlink_h.nlmsghdr) return System.Address;  -- ../include/netlink/msg.h:52
   pragma Import (C, nlmsg_tail, "nlmsg_tail");

   function nlmsg_attrdata (arg1 : access constant linux_netlink_h.nlmsghdr; arg2 : int) return access linux_netlink_h.nlattr;  -- ../include/netlink/msg.h:55
   pragma Import (C, nlmsg_attrdata, "nlmsg_attrdata");

   function nlmsg_attrlen (arg1 : access constant linux_netlink_h.nlmsghdr; arg2 : int) return int;  -- ../include/netlink/msg.h:56
   pragma Import (C, nlmsg_attrlen, "nlmsg_attrlen");

   function nlmsg_valid_hdr (arg1 : access constant linux_netlink_h.nlmsghdr; arg2 : int) return int;  -- ../include/netlink/msg.h:59
   pragma Import (C, nlmsg_valid_hdr, "nlmsg_valid_hdr");

   function nlmsg_ok (arg1 : access constant linux_netlink_h.nlmsghdr; arg2 : int) return int;  -- ../include/netlink/msg.h:60
   pragma Import (C, nlmsg_ok, "nlmsg_ok");

   function nlmsg_next (arg1 : access linux_netlink_h.nlmsghdr; arg2 : access int) return access linux_netlink_h.nlmsghdr;  -- ../include/netlink/msg.h:61
   pragma Import (C, nlmsg_next, "nlmsg_next");

   function nlmsg_parse
     (arg1 : access linux_netlink_h.nlmsghdr;
      arg2 : int;
      arg3 : System.Address;
      arg4 : int;
      arg5 : access constant netlink_attr_h.nla_policy) return int;  -- ../include/netlink/msg.h:62
   pragma Import (C, nlmsg_parse, "nlmsg_parse");

   function nlmsg_find_attr
     (arg1 : access linux_netlink_h.nlmsghdr;
      arg2 : int;
      arg3 : int) return access linux_netlink_h.nlattr;  -- ../include/netlink/msg.h:64
   pragma Import (C, nlmsg_find_attr, "nlmsg_find_attr");

   function nlmsg_validate
     (arg1 : access linux_netlink_h.nlmsghdr;
      arg2 : int;
      arg3 : int;
      arg4 : access constant netlink_attr_h.nla_policy) return int;  -- ../include/netlink/msg.h:65
   pragma Import (C, nlmsg_validate, "nlmsg_validate");

   function nlmsg_alloc return access netlink_handlers_h.nl_msg;  -- ../include/netlink/msg.h:68
   pragma Import (C, nlmsg_alloc, "nlmsg_alloc");

   function nlmsg_alloc_size (arg1 : stddef_h.size_t) return access netlink_handlers_h.nl_msg;  -- ../include/netlink/msg.h:69
   pragma Import (C, nlmsg_alloc_size, "nlmsg_alloc_size");

   function nlmsg_alloc_simple (arg1 : int; arg2 : int) return access netlink_handlers_h.nl_msg;  -- ../include/netlink/msg.h:70
   pragma Import (C, nlmsg_alloc_simple, "nlmsg_alloc_simple");

   procedure nlmsg_set_default_size (arg1 : stddef_h.size_t);  -- ../include/netlink/msg.h:71
   pragma Import (C, nlmsg_set_default_size, "nlmsg_set_default_size");

   function nlmsg_inherit (arg1 : access linux_netlink_h.nlmsghdr) return access netlink_handlers_h.nl_msg;  -- ../include/netlink/msg.h:72
   pragma Import (C, nlmsg_inherit, "nlmsg_inherit");

   function nlmsg_convert (arg1 : access linux_netlink_h.nlmsghdr) return access netlink_handlers_h.nl_msg;  -- ../include/netlink/msg.h:73
   pragma Import (C, nlmsg_convert, "nlmsg_convert");

   function nlmsg_reserve
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : stddef_h.size_t;
      arg3 : int) return System.Address;  -- ../include/netlink/msg.h:74
   pragma Import (C, nlmsg_reserve, "nlmsg_reserve");

   function nlmsg_append
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : System.Address;
      arg3 : stddef_h.size_t;
      arg4 : int) return int;  -- ../include/netlink/msg.h:75
   pragma Import (C, nlmsg_append, "nlmsg_append");

   function nlmsg_expand (arg1 : access netlink_handlers_h.nl_msg; arg2 : stddef_h.size_t) return int;  -- ../include/netlink/msg.h:76
   pragma Import (C, nlmsg_expand, "nlmsg_expand");

   function nlmsg_put
     (arg1 : access netlink_handlers_h.nl_msg;
      arg2 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;
      arg3 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;
      arg4 : int;
      arg5 : int;
      arg6 : int) return access linux_netlink_h.nlmsghdr;  -- ../include/netlink/msg.h:78
   pragma Import (C, nlmsg_put, "nlmsg_put");

   function nlmsg_hdr (arg1 : access netlink_handlers_h.nl_msg) return access linux_netlink_h.nlmsghdr;  -- ../include/netlink/msg.h:80
   pragma Import (C, nlmsg_hdr, "nlmsg_hdr");

   procedure nlmsg_get (arg1 : access netlink_handlers_h.nl_msg);  -- ../include/netlink/msg.h:81
   pragma Import (C, nlmsg_get, "nlmsg_get");

   procedure nlmsg_free (arg1 : access netlink_handlers_h.nl_msg);  -- ../include/netlink/msg.h:82
   pragma Import (C, nlmsg_free, "nlmsg_free");

   procedure nlmsg_set_proto (arg1 : access netlink_handlers_h.nl_msg; arg2 : int);  -- ../include/netlink/msg.h:85
   pragma Import (C, nlmsg_set_proto, "nlmsg_set_proto");

   function nlmsg_get_proto (arg1 : access netlink_handlers_h.nl_msg) return int;  -- ../include/netlink/msg.h:86
   pragma Import (C, nlmsg_get_proto, "nlmsg_get_proto");

   function nlmsg_get_max_size (arg1 : access netlink_handlers_h.nl_msg) return stddef_h.size_t;  -- ../include/netlink/msg.h:87
   pragma Import (C, nlmsg_get_max_size, "nlmsg_get_max_size");

   procedure nlmsg_set_src (arg1 : access netlink_handlers_h.nl_msg; arg2 : access linux_netlink_h.sockaddr_nl);  -- ../include/netlink/msg.h:88
   pragma Import (C, nlmsg_set_src, "nlmsg_set_src");

   function nlmsg_get_src (arg1 : access netlink_handlers_h.nl_msg) return access linux_netlink_h.sockaddr_nl;  -- ../include/netlink/msg.h:89
   pragma Import (C, nlmsg_get_src, "nlmsg_get_src");

   procedure nlmsg_set_dst (arg1 : access netlink_handlers_h.nl_msg; arg2 : access linux_netlink_h.sockaddr_nl);  -- ../include/netlink/msg.h:90
   pragma Import (C, nlmsg_set_dst, "nlmsg_set_dst");

   function nlmsg_get_dst (arg1 : access netlink_handlers_h.nl_msg) return access linux_netlink_h.sockaddr_nl;  -- ../include/netlink/msg.h:91
   pragma Import (C, nlmsg_get_dst, "nlmsg_get_dst");

   procedure nlmsg_set_creds (arg1 : access netlink_handlers_h.nl_msg; arg2 : access netlink_handlers_h.ucred);  -- ../include/netlink/msg.h:92
   pragma Import (C, nlmsg_set_creds, "nlmsg_set_creds");

   function nlmsg_get_creds (arg1 : access netlink_handlers_h.nl_msg) return access netlink_handlers_h.ucred;  -- ../include/netlink/msg.h:93
   pragma Import (C, nlmsg_get_creds, "nlmsg_get_creds");

   function nl_nlmsgtype2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/msg.h:95
   pragma Import (C, nl_nlmsgtype2str, "nl_nlmsgtype2str");

   function nl_str2nlmsgtype (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/msg.h:96
   pragma Import (C, nl_str2nlmsgtype, "nl_str2nlmsgtype");

   function nl_nlmsg_flags2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/msg.h:98
   pragma Import (C, nl_nlmsg_flags2str, "nl_nlmsg_flags2str");

   function nl_msg_parse
     (arg1 : access netlink_handlers_h.nl_msg;
      cb : access procedure (arg1 : access netlink_object_h.nl_object; arg2 : System.Address);
      arg3 : System.Address) return int;  -- ../include/netlink/msg.h:100
   pragma Import (C, nl_msg_parse, "nl_msg_parse");

   procedure nl_msg_dump (arg1 : access netlink_handlers_h.nl_msg; arg2 : access x86_64_linux_gnu_bits_types_FILE_h.FILE);  -- ../include/netlink/msg.h:104
   pragma Import (C, nl_msg_dump, "nl_msg_dump");

end netlink_msg_h;
