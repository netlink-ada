pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with netlink_types_h;
limited with netlink_handlers_h;
with System;
with nlada_stddef_h;
limited with x86_64_linux_gnu_bits_socket_h;
limited with x86_64_linux_gnu_bits_types_struct_iovec_h;
limited with linux_netlink_h;
with Interfaces.C.Strings;

package netlink_netlink_h is

   package stddef_h renames nlada_stddef_h;

   type nl_cache_ops is null record;   -- incomplete struct

   type nl_parser_param is null record;   -- incomplete struct

   nl_debug : aliased int;  -- ../include/netlink/netlink.h:42
   pragma Import (C, nl_debug, "nl_debug");

   nl_debug_dp : aliased netlink_types_h.nl_dump_params;  -- ../include/netlink/netlink.h:43
   pragma Import (C, nl_debug_dp, "nl_debug_dp");

   function nl_connect (arg1 : access netlink_handlers_h.nl_sock; arg2 : int) return int;  -- ../include/netlink/netlink.h:46
   pragma Import (C, nl_connect, "nl_connect");

   procedure nl_close (arg1 : access netlink_handlers_h.nl_sock);  -- ../include/netlink/netlink.h:47
   pragma Import (C, nl_close, "nl_close");

   function nl_sendto
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : System.Address;
      arg3 : stddef_h.size_t) return int;  -- ../include/netlink/netlink.h:50
   pragma Import (C, nl_sendto, "nl_sendto");

   function nl_sendmsg
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : access netlink_handlers_h.nl_msg;
      arg3 : access x86_64_linux_gnu_bits_socket_h.msghdr) return int;  -- ../include/netlink/netlink.h:51
   pragma Import (C, nl_sendmsg, "nl_sendmsg");

   function nl_send (arg1 : access netlink_handlers_h.nl_sock; arg2 : access netlink_handlers_h.nl_msg) return int;  -- ../include/netlink/netlink.h:53
   pragma Import (C, nl_send, "nl_send");

   function nl_send_iovec
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : access netlink_handlers_h.nl_msg;
      arg3 : access x86_64_linux_gnu_bits_types_struct_iovec_h.iovec;
      arg4 : unsigned) return int;  -- ../include/netlink/netlink.h:54
   pragma Import (C, nl_send_iovec, "nl_send_iovec");

   procedure nl_complete_msg (arg1 : access netlink_handlers_h.nl_sock; arg2 : access netlink_handlers_h.nl_msg);  -- ../include/netlink/netlink.h:56
   pragma Import (C, nl_complete_msg, "nl_complete_msg");

   procedure nl_auto_complete (arg1 : access netlink_handlers_h.nl_sock; arg2 : access netlink_handlers_h.nl_msg);  -- ../include/netlink/netlink.h:58
   pragma Import (C, nl_auto_complete, "nl_auto_complete");

   function nl_send_auto (arg1 : access netlink_handlers_h.nl_sock; arg2 : access netlink_handlers_h.nl_msg) return int;  -- ../include/netlink/netlink.h:60
   pragma Import (C, nl_send_auto, "nl_send_auto");

   function nl_send_auto_complete (arg1 : access netlink_handlers_h.nl_sock; arg2 : access netlink_handlers_h.nl_msg) return int;  -- ../include/netlink/netlink.h:61
   pragma Import (C, nl_send_auto_complete, "nl_send_auto_complete");

   function nl_send_sync (arg1 : access netlink_handlers_h.nl_sock; arg2 : access netlink_handlers_h.nl_msg) return int;  -- ../include/netlink/netlink.h:63
   pragma Import (C, nl_send_sync, "nl_send_sync");

   function nl_send_simple
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : int;
      arg3 : int;
      arg4 : System.Address;
      arg5 : stddef_h.size_t) return int;  -- ../include/netlink/netlink.h:64
   pragma Import (C, nl_send_simple, "nl_send_simple");

   function nl_recv
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : access linux_netlink_h.sockaddr_nl;
      arg3 : System.Address;
      arg4 : System.Address) return int;  -- ../include/netlink/netlink.h:68
   pragma Import (C, nl_recv, "nl_recv");

   function nl_recvmsgs (arg1 : access netlink_handlers_h.nl_sock; arg2 : access netlink_handlers_h.nl_cb) return int;  -- ../include/netlink/netlink.h:72
   pragma Import (C, nl_recvmsgs, "nl_recvmsgs");

   function nl_recvmsgs_report (arg1 : access netlink_handlers_h.nl_sock; arg2 : access netlink_handlers_h.nl_cb) return int;  -- ../include/netlink/netlink.h:73
   pragma Import (C, nl_recvmsgs_report, "nl_recvmsgs_report");

   function nl_recvmsgs_default (arg1 : access netlink_handlers_h.nl_sock) return int;  -- ../include/netlink/netlink.h:75
   pragma Import (C, nl_recvmsgs_default, "nl_recvmsgs_default");

   function nl_wait_for_ack (arg1 : access netlink_handlers_h.nl_sock) return int;  -- ../include/netlink/netlink.h:77
   pragma Import (C, nl_wait_for_ack, "nl_wait_for_ack");

   function nl_pickup
     (arg1 : access netlink_handlers_h.nl_sock;
      parser : access function
        (arg1 : access nl_cache_ops;
         arg2 : access linux_netlink_h.sockaddr_nl;
         arg3 : access linux_netlink_h.nlmsghdr;
         arg4 : access nl_parser_param) return int;
      arg3 : System.Address) return int;  -- ../include/netlink/netlink.h:79
   pragma Import (C, nl_pickup, "nl_pickup");

   function nl_pickup_keep_syserr
     (sk : access netlink_handlers_h.nl_sock;
      parser : access function
        (arg1 : access nl_cache_ops;
         arg2 : access linux_netlink_h.sockaddr_nl;
         arg3 : access linux_netlink_h.nlmsghdr;
         arg4 : access nl_parser_param) return int;
      result : System.Address;
      syserror : access int) return int;  -- ../include/netlink/netlink.h:85
   pragma Import (C, nl_pickup_keep_syserr, "nl_pickup_keep_syserr");

   function nl_nlfamily2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/netlink.h:91
   pragma Import (C, nl_nlfamily2str, "nl_nlfamily2str");

   function nl_str2nlfamily (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/netlink.h:92
   pragma Import (C, nl_str2nlfamily, "nl_str2nlfamily");

end netlink_netlink_h;
