pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
limited with netlink_handlers_h;
with Interfaces.C.Strings;
with nlada_stddef_h;
with x86_64_linux_gnu_bits_stdint_uintn_h;

package netlink_route_rtnl_h is

   package stddef_h renames nlada_stddef_h;

   RTNL_REALM_MASK : constant := (16#FFFF#);  --  ../include/netlink/route/rtnl.h:24
   --  arg-macro: function RTNL_REALM_FROM (realm)
   --    return (realm) >> 16;
   --  arg-macro: function RTNL_REALM_TO (realm)
   --    return (realm) and RTNL_REALM_MASK;
   --  arg-macro: function RTNL_MAKE_REALM (from, to)
   --    return (RTNL_REALM_TO(from) << 16) and RTNL_REALM_TO(to);

   function nl_rtgen_request
     (arg1 : access netlink_handlers_h.nl_sock;
      arg2 : int;
      arg3 : int;
      arg4 : int) return int;  -- ../include/netlink/route/rtnl.h:46
   pragma Import (C, nl_rtgen_request, "nl_rtgen_request");

   function nl_rtntype2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/rtnl.h:49
   pragma Import (C, nl_rtntype2str, "nl_rtntype2str");

   function nl_str2rtntype (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/rtnl.h:50
   pragma Import (C, nl_str2rtntype, "nl_str2rtntype");

   function rtnl_scope2str
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/rtnl.h:53
   pragma Import (C, rtnl_scope2str, "rtnl_scope2str");

   function rtnl_str2scope (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- ../include/netlink/route/rtnl.h:54
   pragma Import (C, rtnl_str2scope, "rtnl_str2scope");

   function rtnl_realms2str
     (arg1 : x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- ../include/netlink/route/rtnl.h:57
   pragma Import (C, rtnl_realms2str, "rtnl_realms2str");

end netlink_route_rtnl_h;
