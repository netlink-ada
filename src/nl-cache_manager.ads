--
--  Copyright (c) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This file is part of Netlink/Ada.
--
--  Netlink/Ada is free software; you can redistribute it and/or modify
--  it under the terms of the GNU Lesser General Public License as published
--  by the Free Software Foundation; either version 2.1 of the License, or
--  (at your option) any later version.
--
--  Netlink/Ada is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public License
--  along with Netlink/Ada.  If not, see <https://www.gnu.org/licenses/>.
--

with Ada.Finalization;
private with Ada.Containers.Doubly_Linked_Lists;

with netlink_route_link_h;
with netlink_route_addr_h;
with netlink_route_route_h;
with netlink_object_h;

private with netlink_cache_h;

package NL.Cache_Manager
is

   --  Handler interfaces for different update messages.

   type Handler_Type is interface;

   type Handler_Access is not null access all Handler_Type'Class;

   type Link_Handler_Type is interface and Handler_Type;

   --  Dispatch procedure to handle incoming link update.
   procedure Link_Update
     (H      : in out Link_Handler_Type;
      Action :        Cache_Actions_Type;
      Msg    : access netlink_route_link_h.rtnl_link)
   is abstract;

   type Addr_Handler_Type is interface and Handler_Type;

   --  Dispatch procedure to handle incoming address update.
   procedure Address_Update
     (H      : in out Addr_Handler_Type;
      Action :        Cache_Actions_Type;
      Msg    : access netlink_route_addr_h.rtnl_addr)
   is abstract;

   type Route_Handler_Type is interface and Handler_Type;

   --  Dispatch procedure to handle incoming route update.
   procedure Route_Update
     (H      : in out Route_Handler_Type;
      Action :        Cache_Actions_Type;
      Msg    : access netlink_route_route_h.rtnl_route)
   is abstract;

   type Cache_Mngr_Type is tagged limited private;

   --  Returns True if cache manager is successfully initialized.
   function Is_Initialized (Cache_Mngr : Cache_Mngr_Type) return Boolean;

   --  Initialize cache manager with given socket.
   procedure Initialize
     (Cache_Mngr : in out Cache_Mngr_Type;
      Socket     :        Socket_Type)
     with
       Post => Is_Initialized (Cache_Mngr);

   --  Peek for a new message to dispatch.
   procedure Peek
     (Cache_Mngr   : Cache_Mngr_Type'Class;
      Timeout_Msec : Integer := 100)
     with
       Pre => Is_Initialized (Cache_Mngr);

   --  Register handler for link updates.
   procedure Register_Link_Handler
     (Cache_Mngr : in out Cache_Mngr_Type;
      Handler    :        Handler_Access);

   --  Register handler for address updates.
   procedure Register_Addr_Handler
     (Cache_Mngr : in out Cache_Mngr_Type;
      Handler    :        Handler_Access);

   --  Register handler for route updates.
   procedure Register_Route_Handler
     (Cache_Mngr : in out Cache_Mngr_Type;
      Handler    :        Handler_Access);

   --  Return access to link cache;
   function Get_Link_Cache
     (Cache_Mngr : Cache_Mngr_Type)
      return access netlink_object_h.nl_cache
     with
       Pre => Is_Initialized (Cache_Mngr);

   --  Return access to address cache;
   function Get_Addr_Cache
     (Cache_Mngr : Cache_Mngr_Type)
      return access netlink_object_h.nl_cache
     with
       Pre => Is_Initialized (Cache_Mngr);

   --  Return access to route cache;
   function Get_Route_Cache
     (Cache_Mngr : Cache_Mngr_Type)
      return access netlink_object_h.nl_cache
     with
       Pre => Is_Initialized (Cache_Mngr);

private

   package List_Of_Handlers_Pkg is new
     Ada.Containers.Doubly_Linked_Lists
       (Element_Type => Handler_Access);

   subtype Handler_List is List_Of_Handlers_Pkg.List;

   type Cache_Mngr_Type is new Ada.Finalization.Limited_Controlled with record
      nl_mngr        : access netlink_cache_h.nl_cache_mngr := null;
      nl_link_cache  : access netlink_object_h.nl_cache     := null;
      nl_addr_cache  : access netlink_object_h.nl_cache     := null;
      nl_route_cache : access netlink_object_h.nl_cache     := null;

      Link_Handlers  : Handler_List;
      Addr_Handlers  : Handler_List;
      Route_Handlers : Handler_List;
   end record;

   overriding
   procedure Finalize (Cache_Mngr : in out Cache_Mngr_Type);

   function Is_Initialized (Cache_Mngr : Cache_Mngr_Type) return Boolean
   is (Cache_Mngr.nl_mngr /= null
       and then Cache_Mngr.nl_link_cache /= null
       and then Cache_Mngr.nl_addr_cache /= null
       and then Cache_Mngr.nl_route_cache /= null);

   function Get_Link_Cache
     (Cache_Mngr : Cache_Mngr_Type)
      return access netlink_object_h.nl_cache
   is (Cache_Mngr.nl_link_cache);

   function Get_Addr_Cache
     (Cache_Mngr : Cache_Mngr_Type)
      return access netlink_object_h.nl_cache
   is (Cache_Mngr.nl_addr_cache);

   function Get_Route_Cache
     (Cache_Mngr : Cache_Mngr_Type)
      return access netlink_object_h.nl_cache
   is (Cache_Mngr.nl_route_cache);

end NL.Cache_Manager;
