--
--  Copyright (c) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This file is part of Netlink/Ada.
--
--  Netlink/Ada is free software; you can redistribute it and/or modify
--  it under the terms of the GNU Lesser General Public License as published
--  by the Free Software Foundation; either version 2.1 of the License, or
--  (at your option) any later version.
--
--  Netlink/Ada is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public License
--  along with Netlink/Ada.  If not, see <https://www.gnu.org/licenses/>.
--

with System;

with Interfaces.C.Strings;

with linux_netlink_h;

package body NL.Cache_Manager
is

   procedure Link_Update
     (arg1 : access netlink_object_h.nl_cache;
      arg2 : access netlink_object_h.nl_object;
      arg3 : Interfaces.C.int;
      arg4 : System.Address)
     with
       Convention => C;

   procedure Address_Update
     (arg1 : access netlink_object_h.nl_cache;
      arg2 : access netlink_object_h.nl_object;
      arg3 : Interfaces.C.int;
      arg4 : System.Address)
     with
       Convention => C;

   procedure Route_Update
     (arg1 : access netlink_object_h.nl_cache;
      arg2 : access netlink_object_h.nl_object;
      arg3 : Interfaces.C.int;
      arg4 : System.Address)
     with
       Convention => C;

   -------------------------------------------------------------------------

   procedure Address_Update
     (arg1 : access netlink_object_h.nl_cache;
      arg2 : access netlink_object_h.nl_object;
      arg3 : Interfaces.C.int;
      arg4 : System.Address)
   is
      pragma Unreferenced (arg1);

      Addr : access netlink_route_addr_h.rtnl_addr
        with
          Import,
          Address => arg2'Address;
      Cache_Mngr : Cache_Mngr_Type
        with
          Import,
          Address => arg4;
      Action : constant Cache_Actions_Type := Cache_Actions_Type'Val (arg3);
   begin
      for H of Cache_Mngr.Addr_Handlers loop
         Addr_Handler_Type'Class (H.all).Address_Update
           (Action => Action,
            Msg    => Addr);
      end loop;
   end Address_Update;

   -------------------------------------------------------------------------

   procedure Finalize (Cache_Mngr : in out Cache_Mngr_Type)
   is
   begin

      --  Associated caches are freed by nl_cache_mngr_free.

      if Cache_Mngr.nl_mngr /= null then
         netlink_cache_h.nl_cache_mngr_free (arg1 => Cache_Mngr.nl_mngr);
      end if;

      Cache_Mngr.Link_Handlers.Clear;
      Cache_Mngr.Addr_Handlers.Clear;
      Cache_Mngr.Route_Handlers.Clear;
   end Finalize;

   -------------------------------------------------------------------------

   procedure Initialize
     (Cache_Mngr : in out Cache_Mngr_Type;
      Socket     :        Socket_Type)
   is
      use type Interfaces.C.int;

      Res : Interfaces.C.int;
   begin
      Res := netlink_cache_h.nl_cache_mngr_alloc
        (arg1 => Socket.nl_socket,
         arg2 => linux_netlink_h.NETLINK_ROUTE,
         arg3 => netlink_cache_h.NL_AUTO_PROVIDE,
         arg4 => Cache_Mngr.nl_mngr'Address);
      if Res < 0 then
         raise NL_Error with "Error creating cache manager";
      end if;

      declare
         C_Link  : Interfaces.C.Strings.chars_ptr
           := Interfaces.C.Strings.New_String (Str => "route/link");
         C_Addr  : Interfaces.C.Strings.chars_ptr
           := Interfaces.C.Strings.New_String (Str => "route/addr");
         C_Route : Interfaces.C.Strings.chars_ptr
           := Interfaces.C.Strings.New_String (Str => "route/route");
      begin
         Res := netlink_cache_h.nl_cache_mngr_add
           (arg1 => Cache_Mngr.nl_mngr,
            arg2 => C_Link,
            arg3 => Link_Update'Access,
            arg4 => Cache_Mngr'Address,
            arg5 => Cache_Mngr.nl_link_cache'Address);
         Interfaces.C.Strings.Free (Item => C_Link);
         if Res < 0 then
            raise NL_Error with "Error adding link cache";
         end if;

         Res := netlink_cache_h.nl_cache_mngr_add
           (arg1 => Cache_Mngr.nl_mngr,
            arg2 => C_Addr,
            arg3 => Address_Update'Access,
            arg4 => Cache_Mngr'Address,
            arg5 => Cache_Mngr.nl_addr_cache'Address);
         Interfaces.C.Strings.Free (Item => C_Addr);
         if Res < 0 then
            raise NL_Error with "Error adding address cache";
         end if;

         Res := netlink_cache_h.nl_cache_mngr_add
           (arg1 => Cache_Mngr.nl_mngr,
            arg2 => C_Route,
            arg3 => Route_Update'Access,
            arg4 => Cache_Mngr'Address,
            arg5 => Cache_Mngr.nl_route_cache'Address);
         Interfaces.C.Strings.Free (Item => C_Route);
         if Res < 0 then
            raise NL_Error with "Error adding route cache";
         end if;
      end;
   end Initialize;

   -------------------------------------------------------------------------

   procedure Link_Update
     (arg1 : access netlink_object_h.nl_cache;
      arg2 : access netlink_object_h.nl_object;
      arg3 : Interfaces.C.int;
      arg4 : System.Address)
   is
      pragma Unreferenced (arg1);

      Link : access netlink_route_link_h.rtnl_link
        with
          Import,
          Address => arg2'Address;
      Cache_Mngr : Cache_Mngr_Type
        with
          Import,
          Address => arg4;
      Action : constant Cache_Actions_Type := Cache_Actions_Type'Val (arg3);
   begin
      for H of Cache_Mngr.Link_Handlers loop
         Link_Handler_Type'Class (H.all).Link_Update
           (Action => Action,
            Msg    => Link);
      end loop;
   end Link_Update;

   -------------------------------------------------------------------------

   procedure Peek
     (Cache_Mngr   : Cache_Mngr_Type'Class;
      Timeout_Msec : Integer := 100)
   is
      use type Interfaces.C.int;

      Res : Interfaces.C.int;
   begin
      Res := netlink_cache_h.nl_cache_mngr_poll
        (arg1 => Cache_Mngr.nl_mngr,
         arg2 => Interfaces.C.int (Timeout_Msec));
      if Res < 0 then
         raise NL_Error with "Error polling the cache manager";
      end if;
   end Peek;

   -------------------------------------------------------------------------

   procedure Register_Addr_Handler
     (Cache_Mngr : in out Cache_Mngr_Type;
      Handler    :        Handler_Access)
   is
   begin
      Cache_Mngr.Addr_Handlers.Append (New_Item => Handler);
   end Register_Addr_Handler;

   -------------------------------------------------------------------------

   procedure Register_Link_Handler
     (Cache_Mngr : in out Cache_Mngr_Type;
      Handler    :        Handler_Access)
   is
   begin
      Cache_Mngr.Link_Handlers.Append (New_Item => Handler);
   end Register_Link_Handler;

   -------------------------------------------------------------------------

   procedure Register_Route_Handler
     (Cache_Mngr : in out Cache_Mngr_Type;
      Handler    :        Handler_Access)
   is
   begin
      Cache_Mngr.Route_Handlers.Append (New_Item => Handler);
   end Register_Route_Handler;

   -------------------------------------------------------------------------

   procedure Route_Update
     (arg1 : access netlink_object_h.nl_cache;
      arg2 : access netlink_object_h.nl_object;
      arg3 : Interfaces.C.int;
      arg4 : System.Address)
   is
      pragma Unreferenced (arg1);

      Route : access netlink_route_route_h.rtnl_route
        with
          Import,
          Address => arg2'Address;
      Cache_Mngr : Cache_Mngr_Type
        with
          Import,
          Address => arg4;
      Action : constant Cache_Actions_Type := Cache_Actions_Type'Val (arg3);
   begin
      for H of Cache_Mngr.Route_Handlers loop
         Route_Handler_Type'Class (H.all).Route_Update
           (Action => Action,
            Msg    => Route);
      end loop;
   end Route_Update;

end NL.Cache_Manager;
