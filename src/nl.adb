--
--  Copyright (c) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This file is part of Netlink/Ada.
--
--  Netlink/Ada is free software; you can redistribute it and/or modify
--  it under the terms of the GNU Lesser General Public License as published
--  by the Free Software Foundation; either version 2.1 of the License, or
--  (at your option) any later version.
--
--  Netlink/Ada is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public License
--  along with Netlink/Ada.  If not, see <https://www.gnu.org/licenses/>.
--

with netlink_socket_h;

package body NL
is

   -------------------------------------------------------------------------

   procedure Finalize (Socket : in out Socket_Type)
   is
   begin
      if Socket.nl_socket /= null then
         netlink_socket_h.nl_socket_free (arg1 => Socket.nl_socket);
      end if;
   end Finalize;

   -------------------------------------------------------------------------

   procedure Initialize (Socket : in out Socket_Type)
   is
   begin
      Socket.nl_socket := netlink_socket_h.nl_socket_alloc;
      if Socket.nl_socket = null then
         raise NL_Error with "Error creating Netlink socket";
      end if;
   end Initialize;

end NL;
