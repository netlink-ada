--
--  Copyright (c) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This file is part of Netlink/Ada.
--
--  Netlink/Ada is free software; you can redistribute it and/or modify
--  it under the terms of the GNU Lesser General Public License as published
--  by the Free Software Foundation; either version 2.1 of the License, or
--  (at your option) any later version.
--
--  Netlink/Ada is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public License
--  along with Netlink/Ada.  If not, see <https://www.gnu.org/licenses/>.
--

with Interfaces.C.Strings;

package body NL.Utils
is

   -------------------------------------------------------------------------

   function Nladdr_To_String
     (Addr : access netlink_addr_h.nl_addr)
      return String
   is
      use type Interfaces.C.Strings.chars_ptr;
      use type Interfaces.C.size_t;

      --  Length of IPv6 address + / + 3 bytes prefix + 0.
      Addrbuf : aliased Interfaces.C.char_array
        := (1 .. NL.INET6_ADDRSTRLEN + 5 => Interfaces.C.nul);
      Res     : Interfaces.C.Strings.chars_ptr;
   begin
      Res := netlink_addr_h.nl_addr2str
        (arg1 => Addr,
         arg2 => Interfaces.C.Strings.To_Chars_Ptr
           (Addrbuf'Unchecked_Access),
         arg3 => Addrbuf'Length);

      if Res = Interfaces.C.Strings.Null_Ptr then
         raise Conversion_Error with "Unable to convert nl_addr to string";
      end if;

      return Interfaces.C.To_Ada (Addrbuf);
   end Nladdr_To_String;

end NL.Utils;
