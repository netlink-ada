--
--  Copyright (c) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This file is part of Netlink/Ada.
--
--  Netlink/Ada is free software; you can redistribute it and/or modify
--  it under the terms of the GNU Lesser General Public License as published
--  by the Free Software Foundation; either version 2.1 of the License, or
--  (at your option) any later version.
--
--  Netlink/Ada is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public License
--  along with Netlink/Ada.  If not, see <https://www.gnu.org/licenses/>.
--

with Ada.Finalization;

with netlink_handlers_h;
private with linux_rtnetlink_h;

package NL
is

   INET_ADDRSTRLEN  : constant := 16;
   INET6_ADDRSTRLEN : constant := 46;

   IF_RA_MANAGED   : constant := 16#40#;
   IF_RA_OTHERCONF : constant := 16#80#;

   type Cache_Actions_Type is
     (NL_Act_Unspec,
      NL_Act_New,
      NL_Act_Del,
      NL_Act_Get,
      NL_Act_Set,
      NL_Act_Change);

   type Scope_Type is
     (RT_Scope_Universe,
      RT_Scope_Site,
      RT_Scope_Link,
      RT_Scope_Host,
      RT_Scope_Nowwhere);

   --  RFC 2863 operational status
   type Link_Operstatus_Type is
     (IF_Oper_Unknown,
      IF_Oper_Notpresent,
      IF_Oper_Down,
      IF_Oper_Lowerlayerdown,
      IF_Oper_Testing,
      IF_Oper_Dormant,
      IF_Oper_Up);

   type Socket_Type is new Ada.Finalization.Limited_Controlled with record
      nl_socket : access netlink_handlers_h.nl_sock;
   end record;

   NL_Error : exception;

private

   for Cache_Actions_Type use
     (NL_Act_Unspec => 0,
      NL_Act_New    => 1,
      NL_Act_Del    => 2,
      NL_Act_Get    => 3,
      NL_Act_Set    => 4,
      NL_Act_Change => 5);

   for Scope_Type use
     (RT_Scope_Universe => linux_rtnetlink_h.RT_SCOPE_UNIVERSE,
      RT_Scope_Site     => linux_rtnetlink_h.RT_SCOPE_SITE,
      RT_Scope_Link     => linux_rtnetlink_h.RT_SCOPE_LINK,
      RT_Scope_Host     => linux_rtnetlink_h.RT_SCOPE_HOST,
      RT_Scope_Nowwhere => linux_rtnetlink_h.RT_SCOPE_NOWHERE);

   overriding
   procedure Initialize (Socket : in out Socket_Type);

   overriding
   procedure Finalize (Socket : in out Socket_Type);

end NL;
