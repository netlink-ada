PREFIX   ?= $(HOME)/libraries
OBJDIR    = obj
LIBDIR    = lib
SRCDIR    = src
THINDIR   = thin
GPR_FILES = gnat/*.gpr

MAJOR    = 0
MINOR    = 1
REVISION = 0
VERSION  = $(MAJOR).$(MINOR).$(REVISION)

SO_LIBRARY   = libnlada.so.$(VERSION)
LIBRARY_KIND = dynamic

# Command variables
INSTALL         = install
INSTALL_PROGRAM = $(INSTALL)
INSTALL_DATA    = $(INSTALL) --mode=644 --preserve-timestamps
INSTALL_ALI     = $(INSTALL) --mode=444

NUM_CPUS ?= $(shell nproc)

# GNAT_BUILDER_FLAGS may be overridden in the
# environment or on the command line.
GNAT_BUILDER_FLAGS ?= -R -j$(NUM_CPUS)
# GMAKE_OPTS should not be overridden because -p is essential.
GMAKE_OPTS = -p ${GNAT_BUILDER_FLAGS} \
  $(foreach v,ADAFLAGS LDFLAGS VERSION,'-X$(v)=$($(v))')

# GNU-style directory variables
prefix      = ${PREFIX}
exec_prefix = ${prefix}
includedir  = ${prefix}/include
libdir      = ${exec_prefix}/lib
bindir      = ${exec_prefix}/libexec
gprdir      = ${prefix}/lib/gnat

all: build_lib

build_lib:
	gprbuild $(GMAKE_OPTS) nl_ada_lib.gpr -XLIBRARY_KIND=$(LIBRARY_KIND)

examples:
	gprbuild $(GMAKE_OPTS) nl_ada_examples.gpr -XLIBRARY_KIND=static

install: install_lib install_$(LIBRARY_KIND)

install_lib: build_lib
	$(INSTALL) -d $(DESTDIR)$(gprdir)
	$(INSTALL) -d $(DESTDIR)$(libdir)/nl-ada
	$(INSTALL) -d $(DESTDIR)$(includedir)/nl-ada
	$(INSTALL_DATA) $(SRCDIR)/*.ad[bs] $(DESTDIR)$(includedir)/nl-ada
	$(INSTALL_DATA) $(THINDIR)/*.ad[bs] $(DESTDIR)$(includedir)/nl-ada
	$(INSTALL_ALI) $(LIBDIR)/$(LIBRARY_KIND)/*.ali $(DESTDIR)$(libdir)/nl-ada
	$(INSTALL_DATA) $(GPR_FILES) $(DESTDIR)$(gprdir)

install_examples: examples
	$(INSTALL) -d $(DESTDIR)$(bindir)/nl-ada
	$(INSTALL_PROGRAM) $(OBJDIR)/examples/nlmonitor/nlmonitor $(DESTDIR)$(bindir)/nl-ada
	$(INSTALL_PROGRAM) $(OBJDIR)/examples/slaac/slaac $(DESTDIR)$(bindir)/nl-ada

install_static:
	$(INSTALL_DATA) $(LIBDIR)/$(LIBRARY_KIND)/libnlada.a $(DESTDIR)$(libdir)

install_dynamic:
	$(INSTALL_PROGRAM) $(LIBDIR)/$(LIBRARY_KIND)/$(SO_LIBRARY) $(DESTDIR)$(libdir)
	cd $(DESTDIR)$(libdir) && ln -sf $(SO_LIBRARY) libnlada.so

clean:
	rm -rf $(OBJDIR)
	rm -rf $(LIBDIR)

.PHONY: examples
